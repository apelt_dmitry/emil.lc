<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_deal_status entity
 */

/**
 * View for /p1_deal_status/<id> page.
 */
function p1_deal_status_view_entity($deal_id) {

  $uid = isset($_GET['uid']) ? (int)$_GET['uid'] : NULL;
  $type = isset($_GET['type']) ? (int)$_GET['type'] : NULL;
  $comment = isset($_GET['comment']) ? $_GET['comment'] : NULL;
  $from = isset($_GET['from']) ? strtotime($_GET['from']) : strtotime('-60 days');
  $to = isset($_GET['to']) ? strtotime($_GET['to'])+24*60*60-1 : strtotime(date('m/d/Y',time()+24*60*60-1));

  $deal = p1_deal_load($deal_id);
  $phases = p1_helper_get_phase();
  $phase = $deal->status;
  $conditions = p1_helper_get_docs($deal, $phase);

  $deal_status_list_for_graphic = p1_deal_status_load_multiple(False, ['did' => $deal_id], TRUE);

  $deal_ids = new EntityFieldQuery();
  $deal_ids->entityCondition('entity_type', 'p1_deal_status');
  $deal_ids->propertyCondition('did', $deal_id);
  $deal_ids->propertyCondition('created', [$from, $to], 'BETWEEN');
  if (isset($uid)) {
    $deal_ids->propertyCondition('uid', $uid);
  }
  if (isset($type)) {
    $deal_ids->propertyCondition('type', $type);
  }
  if ($comment) {
    $deal_ids->propertyCondition('comment', '%' . $comment . '%', 'LIKE');
  }
  $deal_ids = $deal_ids->propertyOrderBy('id', 'DESC');
  $deal_ids = $deal_ids->addTag('distinct');
  $deal_ids = $deal_ids->execute();
  $deal_ids = array_pop($deal_ids);
  $tmp = $deal_ids;
  $deal_ids = [];
  if ($tmp) {
    foreach ($tmp as $id) {
      array_push($deal_ids, $id->id);
    }
  }

  $deal_status_list = p1_deal_status_load_multiple($deal_ids, [], TRUE);
  $types = p1_helper_get_deal_status_types();
  $header = [
    'type' => ['data' => t('Type')],
    'user' => ['data' => t('User')],
    'comment' => ['data' => t('Comment')],
    'created' => ['data' => t('Created')],
  ];
  $rows = [];
  $uids = [];
  foreach ($deal_status_list_for_graphic as $deal_status) {
    if (!in_array($deal_status->uid, $uids)) {
      array_push($uids, $deal_status->uid);
    }
  }
  if ($uids) {
    $users = db_select('users')
      ->condition('uid', $uids, 'IN')
      ->fields(NULL, ['uid', 'name'])
      ->execute()
      ->fetchAll();
    $uids = [];
    foreach ($users as $user) {
      $uids[$user->uid] = $user->name;
    }
  }
  foreach ($deal_status_list as $deal_status) {
    $rows[] = [
      'type' => $types[$deal_status->type],
      'user' => $uids[$deal_status->uid],
      'comment' => $deal_status->comment,
      'created' => format_date($deal_status->created, 'short'),
    ];
  }

  $form = drupal_get_form('p1_deal_status_message_form', $deal_id);
  $filters = drupal_get_form('p1_deal_status_filter_form', $uids, $uid, $type, $from, $to, $comment);

  $variables = array(
    'image'        => path_to_theme() . '/images/phase-'.$deal->status.'.png',
    'phase'        => $phases[$deal->status],
    'next'         => $deal->status + 1,
    'timestamp'    => $deal->created,
    'conditions'   => theme('item_list', array('items' => $conditions)),
  );

  if (p1_deal_status_access_callback('view messages', $deal)) {
    $variables['filters_form'] = drupal_render($filters);
    $variables['record_table'] = theme('table', ['header' => $header, 'rows' => $rows]);
  }
  if (p1_deal_status_access_callback('reply', $deal)) {
    $variables['message_form'] = drupal_render($form);
  }

  return theme('status', $variables);
}

function p1_deal_status_filter_form($form, &$form_state, $users, $uid, $type, $from, $to, $comment) {
  $form['comment'] = array(
    '#type' => 'textfield',
    '#default_value' => $comment,
    '#title' => t('Search by Comment'),
  );

  $form['from'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'm/d/Y',
    '#attributes' => array('autocomplete' =>'off','readonly' => 'readonly'),
    '#default_value' => date('Y-m-d', $from),
  );

  $form['to'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'm/d/Y',
    '#attributes' => array('autocomplete' =>'off','readonly' => 'readonly'),
    '#default_value' => date('Y-m-d', $to),
  );

  $form['user'] = array(
    '#type' => 'select',
    '#title' => t('Filter by User'),
    '#options' => $users,
    '#default_value' => $uid,
    '#empty_option' => t('All Users')
  );

  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Filter by Type'),
    '#options' => p1_helper_get_deal_status_types(),
    '#default_value' => $type,
    '#empty_option' => t('All Types'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  return $form;
}

function p1_deal_status_filter_form_submit($form, &$form_state) {
  $values = $form_state['input'];

  $options = array();
  if (strlen($values['user'])) {
    $options['uid'] = $values['user'];
  }
  if (strlen($values['type'])) {
    $options['type'] = $values['type'];
  }
  if (strlen($values['comment'])) {
    $options['comment'] = $values['comment'];
  }
  if (strlen($values['from']['date'])) {
    $options['from'] = $values['from']['date'];
  }
  if (strlen($values['to']['date'])) {
    $options['to'] = $values['to']['date'];
  }
  if (!empty($options)) {
    $options = array('query' => $options);
  }
  drupal_goto(current_path(), $options);
}

function p1_deal_status_message_form($form, &$form_state, $deal_id) {
  if (p1_deal_status_access_callback('reply', $deal_id)) {
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Send Message')
    ];
  }
  $form_state['deal_id'] = $deal_id;
  return $form;
}

function p1_deal_status_message_form_submit($form, &$form_state) {
  global $user;
  $deal_id = $form_state['deal_id'];
  entity_create('p1_deal_status', array(
    'uid' => $user->uid,
    'did' => $deal_id,
    'comment' =>$form_state['input']['message'],
    'type' => 5,
    'access' => 1,
    'created' => REQUEST_TIME,
  ))->save();
}

/**
 * View for /p1_deal_status/<id> page.
 */
function p1_deal_status_review_entity($deal_id, $deal_file_id) {
  $doc_fid = db_select('p1_deal_files')
    ->condition('id', $deal_file_id)
    ->fields(NULL, ['fid'])
    ->execute()
    ->fetchField();
  $doc_file = file_load($doc_fid);
  $file_url = file_create_url($doc_file->uri);

  $deal_status_list = p1_deal_status_load_multiple(FALSE, array('did' => $deal_id, 'fid' => $deal_file_id), TRUE);
  $deal_status = array_pop($deal_status_list);

  $form = drupal_get_form('p1_deal_status_review_form', $deal_status);

  $variables = array(
    'file_url' => $file_url,
    'review_form' => drupal_render($form),
  );

  return theme('review', $variables);
}

/**
 * Implements hook_form().
 */
function p1_deal_status_review_form($form, &$form_state, $deal_status) {
  $form['operation'] = [
    '#type' => 'select',
    '#options' => p1_helper_get_deal_file_statuses(),
    '#empty_option' => t('-- Chose Status --'),
    '#required' => TRUE,
  ];
  $form['comment'] = [
    '#type' => 'textarea',
    '#title' => t('Comment'),
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save Condition Review'),
    '#attributes' => array('class' => array('btn btn-danger')),
  ];
  $form_state['deal_status'] = $deal_status;
  return $form;
}

function p1_deal_status_review_form_submit($form, &$form_state) {
  global $user;

  $op = $form_state['values']['operation'];
  $comment = $form_state['values']['comment'];
  $deal_status = $form_state['deal_status'];
  $deal_status->file_status = $op;
  $deal_status->comment = $comment;
  $deal_status->save();

  if (module_exists('p1_deal_status')) {
    entity_create('p1_deal_status', array(
      'uid' => $user->uid,
      'did' => arg(1),
      'fid' => '',
      'comment' => 'Document was reviewed by ' . $user->mail . ($comment ? ' with comment: "' . $comment . '"' : ''),
      'type' => 6,
      'access' => 1,
      'created' => REQUEST_TIME,
    ))->save();
  }

  drupal_goto('/deal/' . $deal_status->did . '/upload');
}

/**
 * View for /p1_deal_status/<id> page.
 */
function p1_deal_status_email_entity_form($form, &$form_state, $deal_id, $doc_id) {
  $form['email'] = [
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
  ];
  $form['message'] = [
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#required' => TRUE,
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Send Message with Document attached'),
    '#attributes' => array('class' => array('btn btn-danger')),

  ];
  $form_state['deal_file_id'] = $doc_id;
  return $form;
}

function p1_deal_status_email_entity_form_submit($form, &$form_state) {
  $mailto = $form_state['values']['email'];
  $fid = db_select('p1_deal_files', 'df')
  ->condition('id', $form_state['deal_file_id'])
  ->fields('df', ['fid'])
  ->execute()
    ->fetchField();
  $attachment = file_load($fid)->uri;
  $mail = drupal_mail('p1_deal_status', 'p1_deal_status', $mailto, language_default(), ['message' => $form_state['values']['message'], 'attachment' => $attachment]);
  if ($mail['result']) {
    drupal_set_message(t('Message sent'));
  }
  else {
    drupal_set_message(t("Unfortunately, message wasn't sent"));
  }
}

/**
 * Implements hook_mail()
 */
function p1_deal_status_mail($key, &$message, $params) {
  if ($key == 'p1_deal_status') {
    global $user;
    $message['subject'] = 'New activity for you deal';
    $person = "\nFrom " . $user->mail;
    $message['body'][] = $params['message'] . $person;
    $message['params']['attachments'][] = ['filepath' => $params['attachment']];
  }
}
