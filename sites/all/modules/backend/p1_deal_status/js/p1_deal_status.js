(function ($) {
  Drupal.behaviors.myBehavior = {
    attach: function (context, settings) {
        console.log(settings.p1_deal_status);
        var max = settings.p1_deal_status['points'].length
        var series = [];
        for (i = 0; i < max;  i++) {
            series[i] = settings.p1_deal_status['points'][max-i-1]
        }
        $('#graphic').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Deal time analize'
            },
            xAxis: {
                categories: ['Days']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Deal opend at ' + settings.p1_deal_status['created']
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: series
        });
      }
    };
}(jQuery));
