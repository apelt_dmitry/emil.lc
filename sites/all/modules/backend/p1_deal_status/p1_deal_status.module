<?php

// 50 items per page.
define('P1_DEAL_STATUS_TOTAL_ITEMS_PER_PAGE', 50);
// Admin uri links.
define('P1_DEAL_STATUS_MANAGE_URI', 'admin/backend/deal_status/manage/');
define('P1_DEAL_STATUS_URI', 'admin/backend/deal_status');
define('P1_DEAL_STATUS_VIEW_URI', 'admin/backend/deal_status/');

/**
 * Implements hook_entity_info().
 */
function p1_deal_status_entity_info() {
  $deal_status_entity_info['p1_deal_status'] = array(
    'label' => t('Deal Status'),
    'label callback' => 'p1_deal_status_label_callback',
    'entity class' => 'DealStatus',
    'controller class' => 'DealStatusController',
    'base table' => 'p1_deal_status',
    'uri callback' => 'p1_deal_status_uri',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'id',
    ),
    'load hook' => 'p1_deal_status_load',
    'static cache' => TRUE,
    'admin ui' => array(
      'path' => P1_DEAL_STATUS_URI,
      'controller class' => 'DealStatusUIController',
      'file' => 'includes/p1_deal_status.admin.inc',
      'access arguments' => array('administer deal status entities'),
    ),
    'module' => 'p1_deal_status',
    'access callback' => 'p1_deal_status_access_callback',
    'bundles' => array(
      'p1_deal_status' => array(
        'label' => 'DEAL_STATUS',
        'admin' => array(
          'path' => P1_DEAL_STATUS_URI,
          'access arguments' => array('administer deal_status entities'),
        ),
      ),
    ),
    'views controller class' => 'EntityDefaultViewsController',
  );

  return $deal_status_entity_info;
}


/**
 * Implements hook_menu_alter().
 */
function p1_deal_status_menu_alter(&$items) {
  $items['deal/%/status'] = array(
    'title' => 'Deal Status',
    'page callback' => 'p1_deal_status_view_entity',
    'page arguments' => array(1),
    'access callback' => 'p1_deal_status_access_callback',
    'access arguments' => array('view', 1),
    'file' => drupal_get_path('module', 'p1_deal_status') . '/includes/p1_deal_status.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );

  $items['deal/%/doc/%/review'] = array(
    'title' => 'Deal Review',
    'page callback' => 'p1_deal_status_review_entity',
    'page arguments' => array(1, 3),
    'access callback' => 'p1_deal_status_access_callback',
    'access arguments' => array('review', 1),
    'file' => drupal_get_path('module', 'p1_deal_status') . '/includes/p1_deal_status.inc',
    'type' => MENU_CALLBACK,
  );

  $items['deal/%/doc/%/email'] = array(
    'title' => 'Deal Email',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('p1_deal_status_email_entity_form', 1, 3),
    'access callback' => 'p1_deal_status_access_callback',
    'access arguments' => array('email', 1),
    'file' => drupal_get_path('module', 'p1_deal_status') . '/includes/p1_deal_status.inc',
    'type' => MENU_CALLBACK,
  );
}

/**
 * Implements hook_permission().
 */
function p1_deal_status_permission() {
  return array(
    'administer deal_status entities' => array(
      'title' => t('Administer DEAL_STATUS Entities'),
      'description' => t('Allows a user to administer DEAL_STATUS entities'),
    ),
    'view deal_status entities' => array(
      'title' => t('View DEAL_STATUS Entity'),
      'description' => t('Allows a user to view the DEAL_STATUS entities.'),
    ),
    'review deal_status entities' => array(
      'title' => t('Review DEAL_STATUS Entity'),
      'description' => t('Allows a user to view the DEAL_STATUS entities.'),
    ),
    'email deal_status entities' => array(
      'title' => t('E-mail DEAL_STATUS Entity'),
      'description' => t('Allows a user to view the DEAL_STATUS entities.'),
    ),
  );
}


/**
 * p1_deal_status access callback.
 */
function p1_deal_status_access_callback($op, $deal = NULL, $account = NULL) {
  if ($deal) {
    if (is_string($deal)) {
      $deal = p1_deal_load($deal);
    }
    global $user;
    $permissions = p1_helper_role_access($deal);
    switch ($op) {
      case 'view':
        return ($deal->uid == $user->uid
          || in_array('administrator user', $user->roles)
          || in_array('supervisor', $user->roles)
          || in_array('processor', $user->roles)
          || in_array('view', $permissions));
      case 'review':
        return (in_array('administrator user', $user->roles)
          || in_array('supervisor', $user->roles)
          || in_array('processor', $user->roles));
      case 'reply':
        return ($deal->uid == $user->uid
          || in_array('administrator user', $user->roles)
          || in_array('supervisor', $user->roles)
          || in_array('processor', $user->roles)
          || in_array('follow_up_reply', $permissions));
      case 'email':
        return ($deal->uid == $user->uid
          || in_array('administrator user', $user->roles)
          || in_array('supervisor', $user->roles));
      case 'view messages':
        return ($deal->uid == $user->uid
          || in_array('administrator user', $user->roles)
          || in_array('supervisor', $user->roles)
          || in_array('follow_up_view_messages', $permissions));
    }
  }
  return FALSE;
}


/**
 * Label callback for DEAL_STATUS Entities, for menu router, etc.
 */
function p1_deal_status_label_callback($deal_status, $type) {
  return 'Deal status';
}

/**
 * Saves DEAL_STATUS to database.
 */
function p1_deal_status_save(p1_deal_status $deal_status) {
  return $deal_status->save();
}


/**
 * p1_deal_status p1 entity class.
 */
class DealStatus extends Entity {
  /**
   * Override defaultUri().
   */
  protected function defaultUri() {
    return array('path' => 'p1_deal_status/' . $this->identifier());
  }
}

/**
 * Menu autoloader for /p1_deal_status.
 */
function p1_deal_status_load($deal_status_id, $reset = FALSE) {
  $deal_status = p1_deal_status_load_multiple(array($deal_status_id), array(), $reset);
  return reset($deal_status);
}

/**
 * Load multiple p1_deal_status based on certain conditions.
 */
function p1_deal_status_load_multiple($deal_status_ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('p1_deal_status', $deal_status_ids, $conditions, $reset);
}

function p1_deal_status_theme($existing, $type, $theme, $path) {
    return array(
        'status' => array(
            'template' => 'status',
            'path' => drupal_get_path('theme', 'p1') . '/templates/deal',
        ),
        'review' => array(
            'template' => 'review',
            'path' => drupal_get_path('theme', 'p1') . '/templates/deal',
        )
    );
}

/**
 * Custom controller for the p1_deal_status entity.
 */
class DealStatusController extends AuditTrailController {

  /**
   * Override the save method.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    if (isset($entity->is_new)) {
      $entity->created = REQUEST_TIME;
    }
    return parent::save($entity, $transaction);
  }

  /**
   * Override the delete method.
   */
  public function delete($entity) {
    return FALSE;
  }
}

/**
 * Custom controller for the administrator UI.
 */
class DealStatusUIController extends EntityDefaultUIController {

  /**
   * Override the menu hook for default ui controller.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $wildcard =  '%deal';
    $items[$this->path]['title'] = t('Deal status');
    $items[$this->path]['access callback'] = 'p1_deal_status_access_callback';
    $items[$this->path]['access arguments'] = array('administer deal_status entities');
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    unset($items[$this->path . '/add']);
    unset($items[$this->path . '/manage/' . $wildcard . '/%']);

    return $items;
  }

  /**
   * Admin form for searching and doing bulk operations.
   */
  public function overviewForm($form, &$form_state) {

    $form = array();
    return $form;
  }

  /**
   * Form Submit method.
   */
  public function overviewFormSubmit($form, &$form_state) {
    $values = $form_state['input'];

    $options = array();
    if (strlen($values['city'])) {
      $options['city'] = $values['city'];
    }
    if (strlen($values['state'])) {
      $options['state'] = $values['state'];
    }
    if (!empty($options)) {
      $options = array('query' => $options);
    }
    drupal_goto(P1_DEAL_STATUS_URI, $options);
  }
}


/**
 * Implements hook_entity_update().
 */
function p1_deal_status_entity_update($entity, $type) {
  global $user;
  switch ($type) {
    case 'p1_deal':
      entity_create('p1_deal_status', array(
        'uid' => $user->uid,
        'did' => $entity->id,
        'comment' => 'Deal updated by user ' . $user->mail,
        'type' => 1,
        'access' => 1,
        'created' => REQUEST_TIME,
      ))->save();
      break;
    case 'p1_term_sheet':
      entity_create('p1_deal_status', array(
        'uid' => $user->uid,
        'did' => $entity->id,
        'comment' => 'Term sheet updated by user ' . $user->mail,
        'type' => 2,
        'access' => 1,
        'created' => REQUEST_TIME,
      ))->save();
      break;
    case 'p1_deal_status':
      $fid = $entity->fid;
      $phase = db_select('p1_deal_files', 'pdf');
      $phase->leftJoin('p1_docs', 'pd', 'pd.id = pdf.doc_id');
      $phase = $phase->fields('pd', ['phase'])
        ->condition('pdf.id', $fid)
        ->execute()
        ->fetchField();

      $query = new EntityFieldQuery();

      $query->entityCondition('entity_type', 'p1_deal_status')
        ->propertyCondition('did', $entity->did)
        ->propertyCondition('fid', 'NULL', '!=');
      $ids = $query->execute();
      $ids = array_pop($ids);

      $conditions = [];

      foreach ($ids as $id) {
        $conditions[] = $id->id;
      }

      $deal_status_list = p1_deal_status_load_multiple($conditions);
      $flag = FALSE;
      $deal = p1_deal_load($entity->did);
      $docs = p1_helper_get_docs($deal, $deal->status);
      $doc_types = [];
      foreach ($docs as $key => $doc) {
        $doc_types[] = $key;
      }
      $doc_files = db_select('p1_deal_files')
        ->condition('did', (int)$deal->id)
        ->condition('doc_id', $doc_types, 'IN')
        ->fields(NULL, ['id'])
        ->execute()
        ->fetchAll();
      $real_statuses = [];
      $expect_statuses = [];
      foreach ($doc_files as $doc) {
        $expect_statuses[] = $doc->id;
      }
      foreach ($deal_status_list as $value) {
        if (isset($value->file_status)) {
          $real_statuses[] = $value->fid;
        }
      }
      sort($expect_statuses);
      sort($real_statuses);
      if ($real_statuses == $expect_statuses) {
        $flag = TRUE;
      }
      if ($flag) {
        entity_create('p1_deal_status', array(
          'uid' => $user->uid,
          'did' => $entity->did,
          'type' => 4,
          'comment' => $phase,
          'access' => 1,
          'created' => REQUEST_TIME,
        ));
        $deal = p1_deal_load($entity->did);
        $deal->status = $phase;
        $deal->save();
      }
  }
}

/**
 * Implements hook_entity_insert().
 */
function p1_deal_status_entity_insert($entity, $type) {
  global $user;
  switch ($type) {
    case 'p1_deal':
      entity_create('p1_deal_status', array(
        'uid' => $user->uid,
        'did' => $entity->id,
        'comment' => 'Deal created by user ' . $user->mail,
        'type' => 0,
        'access' => 1,
        'created' => REQUEST_TIME,
      ))->save();
      break;
    case 'p1_term_sheet':
      entity_create('p1_deal_status', array(
        'uid' => $user->uid,
        'did' => $entity->id,
        'comment' => 'Term sheet updated by user ' . $user->mail,
        'type' => 2,
        'access' => 1,
        'created' => REQUEST_TIME,
      ))->save();
      break;
    case 'p1_deal_files':
      global $user;
      entity_create('p1_deal_status', array(
        'uid' => $user->uid,
        'did' => $entity->id,
        'fid' => $entity->fid,
        'comment' => 'Uploaded by user ' . $user->mail,
        'type' => 3,
        'access' => 1,
        'created' => REQUEST_TIME,
      ))->save();
      break;
    case 'p1_access':
      entity_create('p1_deal_status', array(
        'uid' => $user->uid,
        'did' => $entity->id,
        'comment' => 'Add role' . $entity->role . ' to ' . $entity->email . ' by ' . $user->mail,
        'type' => 3,
        'access' => 1,
        'created' => REQUEST_TIME,
      ))->save();
      break;
  }
}
