<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_docs entity
 */

/**
 * Implements hook_form().
 */
function p1_docs_form($form, &$form_state, $docs = NULL) {
	$form = array();
  $checboxes = array();
  if (isset($docs->id)) {
    $docs_states = db_select('p1_docs_states')
      ->fields(NULL, array('state_code', 'is_use'))
      ->condition('docs_id', $docs->id)
      ->execute()
      ->fetchAllAssoc('state_code');

    $checboxes = array();
    foreach ($docs_states as $key => $value) {
      if ($value->is_use) {
        $checboxes[] = $key;
      }
    }
  }

	// Get list of Docs category
	$category = p1_helper_get_docs_category();

	// Get list of who should provide document
	$provided = p1_helper_get_docs_provided_by();

	$form['name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#default_value' => isset($docs->name) ? $docs->name : '',
		'#required' => TRUE,
		'#maxlength' => 255,
	);

	$form['active'] = array(
		'#title' => t('Active'),
		'#type' => 'select',
		'#options' => array(
			0 => 'No',
			1 => 'Yes',
		),
		'#default_value' => isset($docs->active) ? $docs->active : 0,
		'#required' => TRUE,
	);

	$form['required'] = array(
		'#title' => t('Required'),
		'#type' => 'select',
		'#options' => array(
			0 => 'No',
			1 => 'Yes',
		),
		'#default_value' => isset($docs->required) ? $docs->required : 0,
	);

	$form['category'] = array(
		'#title' => t('Category'),
		'#type' => 'select',
		'#options' => $category,
		'#default_value' => isset($docs->category) ? $docs->category : NULL,
		'#required' => TRUE,
	);

	$form['provided_by'] = array(
		'#title' => t('Provided by'),
		'#type' => 'select',
		'#options' => $provided,
		'#default_value' => isset($docs->provided_by) ? $docs->provided_by : NULL,
	);

  $form['property_type'] = array(
    '#title' => t('Property Type'),
    '#type' => 'select',
    '#options' => p1_helper_get_property_types(),
    '#default_value' => isset($docs->property_type) ? $docs->property_type : NULL,
    '#required' => TRUE,
  );

  $form['loan_type'] = array(
    '#title' => t('Loan Type'),
    '#type' => 'select',
    '#empty_option' => '- Select -',
    '#options' => p1_helper_get_features_type_by_type('Loan type'),
    '#default_value' => isset($docs->loan_type) ? $docs->loan_type : NULL,
  );
  $form['phase'] = array(
    '#title' => t('Phase'),
    '#type' => 'select',
    '#empty_option' => '- Select -',
    '#options' => p1_helper_get_phase(),
    '#default_value' => isset($docs->phase) ? $docs->phase : NULL,
  );

  $form['states'] = array(
    '#title' => t('States'),
    '#type' => 'checkboxes',
    '#options' => p1_helper_get_states(),
    '#default_value' => $checboxes,
  );

	$form['description'] = array(
		'#title' => 'Why do you need this document?',
		'#type' => 'textarea',
		'#default_value' => isset($docs->description) ? $docs->description : NULL,
		'#rows' => 1,
	);

	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
			'#type' => 'submit',
			'#value' => isset($docs->id) ? t('Update Docs') : t('Save Docs'),
		),
		'delete_link' => array(
			'#markup' => isset($docs->id) ? l(t('Delete'), P1_DOCS_MANAGE_URI . $docs->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''));
	return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_docs_form_validate($form, &$form_state) {
	$element = $form['name'];
	$entity = entity_load('p1_docs', FALSE, array('name' => $element['#value']), FALSE);
		if (count($entity)) {
			if ($form_state['op'] == 'add' || $form_state['p1_docs']->id != array_shift($entity)->id) {
			form_error($element, t('Docs with inputted name is exist. Please choose another name.'));
		}
	}
}

/**
 * Implements hook_form_submit().
 */
function p1_docs_form_submit($form, &$form_state) {
	$docs = entity_ui_form_submit_build_entity($form, $form_state);
	$docs->save();
	drupal_set_message(t('@name Docs has been saved.', array('@name' => $docs->name)));
	$form_state['redirect'] = P1_DOCS_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_docs.
 */
function p1_docs_bulk_delete($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$docss = p1_docs_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($docss as $docs) {
		$variables['items'][] = $docs->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Delete all Docs?'), P1_DOCS_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_docs.
 */
function p1_docs_bulk_activate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$docss = p1_docs_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($docss as $docs) {
		$variables['items'][] = $docs->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Activate all Docs?'), P1_DOCS_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_docs.
 */
function p1_docs_bulk_deactivate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$docss = p1_docs_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($docss as $docs) {
		$variables['items'][] = $docs->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Deactivate all Docs?'), P1_DOCS_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_docs_bulk_delete_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_docs_delete_multiple($ids);

	drupal_set_message(t('Docs deleted'));
	drupal_goto(P1_DOCS_URI);
}

/**
 * Implements hook_submit().
 */
function p1_docs_bulk_activate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_docs_activate_multiple($ids);

	drupal_set_message(t('Docs actived'));
	drupal_goto(P1_DOCS_URI);
}

/**
 * Implements hook_submit().
 */
function p1_docs_bulk_deactivate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_docs_deactivate_multiple($ids);

	drupal_set_message(t('Docs actived'));
	drupal_goto(P1_DOCS_URI);
}
