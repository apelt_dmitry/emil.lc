<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_features entity
 */

/**
 * Implements hook_form().
 */
function p1_features_type_form($form, &$form_state, $features_type = NULL) {
  $form = array();

  $form['type'] = array(
    '#title' => t('Features Type'),
    '#type' => 'select',
    '#options' => array(
      'Loan type' => 'Loan type',
      'Term' => 'Term',
      'Amortization' => 'Amortization',
      'Occupancy' => 'Occupancy',
      'Loan' => 'Loan',
      'LTV'=> 'LTV',
      'Pre-Payment' => 'Pre-Payment',
      'Streamline' => 'Streamline',
      'Rebate' => 'Rebate',
      'Units' => 'Units',
    ),
    '#empty_option' => t('- Select Feature Type -'),
    '#default_value' => isset($features_type->type) ? $features_type->type: NULL,
    '#required' => TRUE,
  );

  $form['name'] = array(
    '#title' => t('Feature Name'),
    '#type' => 'textfield',
    '#default_value' => isset($features_type->name) ? $features_type->name : '',
    '#required' => TRUE,
    '#maxlength' => 255,
  );

  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'select',
    '#options' => array(
      0 => 'No',
      1 => 'Yes',
    ),
    '#default_value' => isset($features_type->active) ? $features_type->active : 0,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($features_type->id) ? t('Update Features Type') : t('Save Features Type'),
    ),
    'delete_link' => array(
      '#markup' => isset($features_type->id) ? l(t('Delete'), P1_FEATURES_TYPE_MANAGE_URI . $features_type->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''));
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_features_type_form_validate($form, &$form_state) {
  $element = $form['name'];
  $entity = entity_load('p1_features_type', FALSE, array('name' => $element['#value']), FALSE);
  if (count($entity)) {
    if ($form_state['op'] == 'add' || $form_state['p1_features_type']->id != array_shift($entity)->id) {
      form_error($element, t('Features with inputted name is exist. Please choose another name.'));
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function p1_features_type_form_submit($form, &$form_state) {
  $features_type = entity_ui_form_submit_build_entity($form, $form_state);
  $features_type->save();
  drupal_set_message(t('@name Features has been saved.', array('@name' => $features_type->name)));
  $form_state['redirect'] = P1_FEATURES_TYPE_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_features.
 */
function p1_features_type_bulk_delete($form, &$form_state, $ids) {
  $ids = explode('|', $ids);
  $features_type = p1_features_load_multiple($ids);

  $form = array();
  $form_state['ids'] = $ids;

  $variables = array(
    'type' => 'ul',
    'items' => array(),
    'title' => '',
    'attributes' => array(),
  );

  foreach ($features_type as $type) {
    $variables['items'][] = $type->name;
  }

  $form['summary'] = array(
    '#markup' => theme_item_list($variables),
  );

  return confirm_form($form, t('Delete all Features?'), P1_FEATURES_TYPE_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_features.
 */
function p1_features_type_bulk_activate($form, &$form_state, $ids) {
  $ids = explode('|', $ids);
  $features_type = p1_features_type_load_multiple($ids);

  $form = array();
  $form_state['ids'] = $ids;

  $variables = array(
    'type' => 'ul',
    'items' => array(),
    'title' => '',
    'attributes' => array(),
  );

  foreach ($features_type as $type) {
    $variables['items'][] = $type->name;
  }

  $form['summary'] = array(
    '#markup' => theme_item_list($variables),
  );

  return confirm_form($form, t('Activate all Features Types?'), P1_FEATURES_TYPE_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_features.
 */
function p1_features_type_bulk_deactivate($form, &$form_state, $ids) {
  $ids = explode('|', $ids);
  $features_type = p1_features_type_load_multiple($ids);

  $form = array();
  $form_state['ids'] = $ids;

  $variables = array(
    'type' => 'ul',
    'items' => array(),
    'title' => '',
    'attributes' => array(),
  );

  foreach ($features_type as $type) {
    $variables['items'][] = $type->name;
  }

  $form['summary'] = array(
    '#markup' => theme_item_list($variables),
  );

  return confirm_form($form, t('Deactivate all Features?'), P1_FEATURES_TYPE_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_features_type_bulk_delete_submit($form, &$form_state) {
  $ids = $form_state['ids'];
  p1_features_type_delete_multiple($ids);

  drupal_set_message(t('Features deleted'));
  drupal_goto(P1_FEATURES_TYPE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_features_type_bulk_activate_submit($form, &$form_state) {
  $ids = $form_state['ids'];
  p1_features_type_activate_multiple($ids);

  drupal_set_message(t('Features actived'));
  drupal_goto(P1_FEATURES_TYPE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_features_type_bulk_deactivate_submit($form, &$form_state) {
  $ids = $form_state['ids'];
  p1_features_type_deactivate_multiple($ids);

  drupal_set_message(t('Features actived'));
  drupal_goto(P1_FEATURES_TYPE_URI);
}
