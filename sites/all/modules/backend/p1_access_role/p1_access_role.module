<?php

// 50 items per page.
define('P1_ACCESS_ROLE_TOTAL_ITEMS_PER_PAGE', 50);
// Admin uri links.
define('P1_ACCESS_ROLE_MANAGE_URI', 'admin/backend/access-role/manage/');
define('P1_ACCESS_ROLE_URI', 'admin/backend/access-role');
define('P1_ACCESS_ROLE_VIEW_URI', 'admin/backend/access-role/');



/**
 * Implements hook_entity_info().
 */
function p1_access_role_entity_info() {
    $access_role_entity_info['p1_access_role'] = array(
        'label' => t('Access Role'),
        'label callback' => 'p1_access_role_label_callback',
        'entity class' => 'AccessRole',
        'controller class' => 'AccessRoleController',
        'base table' => 'p1_access_role',
        'uri callback' => 'p1_access_role_uri',
        'fieldable' => FALSE,
        'entity keys' => array(
            'id' => 'id',
        ),
        'load hook' => 'p1_access_role_load',
        'static cache' => TRUE,
        'admin ui' => array(
            'path' => P1_ACCESS_ROLE_URI,
            'controller class' => 'AccessRoleUIController',
            'file' => 'includes/p1_access_role.admin.inc',
            'access arguments' => array('administer access role entities'),
        ),
        'module' => 'p1_access_role',
        'access callback' => 'p1_access_role_access_callback',
        'bundles' => array(
            'p1_access_role' => array(
                'label' => 'Access roles',
                'admin' => array(
                    'path' => P1_ACCESS_ROLE_URI,
                    'access arguments' => array('administer access role entities'),
                ),
            ),
        ),
        'views controller class' => 'EntityDefaultViewsController',
    );

    $access_role_entity_info['p1_access_role_permissions'] = array(
        'label' => t('Access Role Permissions'),
        'label callback' => 'p1_access_role_label_callback',
        'entity class' => 'AccessRolePermissions',
        'controller class' => 'AccessRolePermissionsController',
        'base table' => 'p1_access_role_permissions',
        'uri callback' => 'p1_access_role_permissions_uri',
        'fieldable' => FALSE,
        'entity keys' => array(
            'id' => 'id',
        ),
        'load hook' => 'p1_access_role_permissions_load',
        'static cache' => TRUE,
        'admin ui' => array(
            'path' => P1_ACCESS_ROLE_URI,
            'controller class' => 'AccessRolePermissionsUIController',
            'file' => 'includes/p1_access_role_permissions.admin.inc',
            'access arguments' => array('administer access role entities'),
        ),
        'module' => 'p1_access_role',
        'access callback' => 'p1_access_role_access_callback',
        'bundles' => array(
            'p1_access_role' => array(
                'label' => 'Access role permissions',
                'admin' => array(
                    'path' => P1_ACCESS_ROLE_URI,
                    'access arguments' => array('administer access role entities'),
                ),
            ),
        ),
        'views controller class' => 'EntityDefaultViewsController',
    );

    return $access_role_entity_info;
}

/**
 * Implements hook_menu().
 */
function p1_access_role_menu() {
    $items = array();

    $items[P1_ACCESS_ROLE_VIEW_URI. '%p1_access_role'] = array(
        'title' => 'Access Role',
        'page callback' => 'p1_access_role_view_entity',
        'page arguments' => array(3),
        'access callback' => 'p1_access_role_access_menu_callback',
        'access arguments' => array('view', 3),
    );

    $items[P1_ACCESS_ROLE_URI . '/bulk/delete/%'] = array(
        'title' => 'Bulk Delete Access Role',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('p1_access_role_bulk_delete', 5),
        'access arguments' => array('administer access role entities'),
        'file' => 'includes/p1_access_role.admin.inc',
    );

    $items[P1_ACCESS_ROLE_URI . '/bulk/activate/%'] = array(
        'title' => 'Bulk Activate Access Role',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('p1_access_role_bulk_activate', 5),
        'access arguments' => array('administer access role entities'),
        'file' => 'includes/p1_access_role.admin.inc',
    );

    $items[P1_ACCESS_ROLE_URI . '/bulk/deactivate/%'] = array(
        'title' => 'Bulk Deactivate Access Role',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('p1_access_role_bulk_deactivate', 5),
        'access arguments' => array('administer access role entities'),
        'file' => 'includes/p1_access_role.admin.inc',
    );

    return $items;
}

/**
 * Implements hook_permission().
 */
function p1_access_role_permission() {
    return array(
        'administer access role entities' => array(
            'title' => t('Administer Access Role Entities'),
            'description' => t('Allows a user to administer Access Role entities'),
        ),
        'view access role entities' => array(
            'title' => t('View Access Role Entity'),
            'description' => t('Allows a user to view the Access Role entities.'),
        ),
    );
}

/**
 * Check access permission for p1_access_role Entity UI.
 */
function p1_access_role_access_menu_callback($op, $access_role = NULL, $account = NULL) {
    switch ($op) {
        case 'view':
            return user_access('view access role entities', $account);

        case 'create':
            return user_access('administer access role entities', $account);

        case 'update':
            return user_access('administer access role entities', $account);

        case 'delete':
            return user_access('administer access role entities', $account);
    }

    return FALSE;
}

/**
 * p1_access_role access callback.
 */
function p1_access_role_access_callback() {
    if (user_is_anonymous() || !user_access('administer access role entities')) {
        return FALSE;
    }
    else {
        return TRUE;
    }
}


/**
 * Label callback for Access Role Entities, for menu router, etc.
 */
function p1_access_role_label_callback($access_role, $type) {
    return empty($access_role->name) ? 'Unnamed Access Role' : $access_role->name;
}

/**
 * Saves Access Role to database.
 */
function p1_access_role_save(p1_access_role $access_role) {
    return $access_role->save();
}

/**
 * View for /p1_access_role/<id> page.
 */
function p1_access_role_view_entity($access_role) {
    drupal_set_title($access_role->name);
    // Path not entity.
    $access_role_output = theme('p1_access_role_full', array('p1_access_role' => $access_role));
    return $access_role_output;
}

/**
 * p1_access_role p1 entity class.
 */
class AccessRole extends Entity {
    /**
     * Override defaultUri().
     */
    protected function defaultUri() {
        return array('path' => 'p1_access_role/' . $this->identifier());
    }
}

/**
 * p1_access_role_permissions p1 entity class.
 */
class AccessRolePermissions extends Entity {
    /**
     * Override defaultUri().
     */
    protected function defaultUri() {
        return array('path' => 'p1_access_role_permissions/' . $this->identifier());
    }
}

/**
 * Menu autoloader for /p1_access_role.
 */
function p1_access_role_load($access_role_id, $reset = FALSE) {
    $access_role = p1_access_role_load_multiple(array($access_role_id), array(), $reset);
    return reset($access_role);
}

/**
 * Load multiple p1_access_role based on certain conditions.
 */
function p1_access_role_load_multiple($access_role_ids = FALSE, $conditions = array(), $reset = FALSE) {
    return entity_load('p1_access_role', $access_role_ids, $conditions, $reset);
}

/**
 * Load multiple p1_access_role_permissions based on certain conditions.
 */
function p1_access_role_permissions_load_multiple($access_role_permissions_ids = FALSE, $conditions = array(), $reset = FALSE) {
    return entity_load('p1_access_role_permissions', $access_role_permissions_ids, $conditions, $reset);
}

/**
 * Deletes a Access Role.
 */
function p1_access_role_delete(p1_access_role $access_role) {
    $access_role->delete($access_role);
}

/**
 * Delete multiple p1_access_role.
 */
function p1_access_role_delete_multiple(array $access_role_ids) {
    entity_get_controller('p1_access_role')->remove($access_role_ids, 'p1_access_role');
}

/**
 * Delete multiple p1_access_role_permissions.
 */
function p1_access_role_permissions_delete_multiple(array $access_role_permissions_ids) {
    entity_get_controller('p1_access_role_permissions')->remove($access_role_permissions_ids, 'p1_access_role_permissions');
}

/**
 * Activate multiple p1_access_role.
 */
function p1_access_role_activate_multiple(array $access_role_ids) {
    entity_get_controller('p1_access_role')->activate('p1_access_role', $access_role_ids);
}

/**
 * Deactivate multiple p1_access_role.
 */
function p1_access_role_deactivate_multiple(array $access_role_ids) {
    entity_get_controller('p1_access_role')->deactivate('p1_access_role', $access_role_ids);
}

/**
 * Custom controller for the p1_access_role entity.
 */
class AccessRoleController extends AuditTrailController {
    /**
     * Override the save method.
     */
    public function save($entity, DatabaseTransaction $transaction = NULL) {
        return parent::save($entity, $transaction);
    }

    /**
     * Override the delete method.
     */
    public function delete($entity) {
        return parent::remove($entity, 'p1_access_role');
    }
}

/**
 * Custom controller for the p1_access_role_permissions entity.
 */
class AccessRolePermissionsController extends AuditTrailController {

    /**
     * Override the save method.
     */
    public function save($entity, DatabaseTransaction $transaction = NULL) {
        return parent::save($entity, $transaction);
    }

    /**
     * Override the delete method.
     */
    public function delete($entity) {
        return parent::remove($entity, 'p1_access_role');
    }
}

/**
 * Custom controller for the administrator UI.
 */
class AccessRoleUIController extends EntityDefaultUIController {

    /**
     * Override the menu hook for default ui controller.
     */
    public function hook_menu() {
        $items = parent::hook_menu();
        $items[$this->path]['title'] = t('Access Role');
        $items[$this->path]['access callback'] = 'p1_access_role_access_callback';
        $items[$this->path]['access arguments'] = array('administer access role entities');
        $items[$this->path]['type'] = MENU_LOCAL_TASK;
        return $items;
    }

    /**
     * Admin form for searching and doing bulk operations.
     */
    public function overviewForm($form, &$form_state) {

        $form['pager'] = array('#theme' => 'pager');

        $header = array(
            'name' => array('data' => t('Role'), 'field' => 'name'),
            'deal' => array('data' => t('Deal'), 'field' => 'deal'),
            'document' => array('data' => t('Document Upload'), 'field' => 'document_view_name'),
            'client' => array('data' => t('Client Data'), 'field' => 'client_view'),
            'follow_up' => array('data' => t('Follow Up'), 'field' => 'follow_up_view_messages'),
            'active' => array('data' => t('Status'), 'field' => 'active'),
            'operations' => array('data' => t('Operations')),
        );

        $rows = array();

        $search_term = !empty($_GET['search']) ? $_GET['search'] : NULL;
        $status = isset($_GET['active']) ? (int)$_GET['active'] : NULL;

        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'p1_access_role');
        if (isset($status)) {
            $query->propertyCondition('active', $status);
        }
        if (!empty($search_term)) {
            $query->propertyCondition('name', '%' . $search_term . '%', 'like');
        }
        $query->pager(50);

        $result = $query->execute();
        $access_role_ids = array();
        $access_role_permissions = array();
        if($result) {
            foreach ($result['p1_access_role'] as $value) {
                $access_role_ids[] = $value->id;
            }
        }

        $roles = p1_access_role_load_multiple($access_role_ids);
        $role_permissions = array();
        if ($access_role_ids) {
            $role_permissions = p1_access_role_permissions_load_multiple(FALSE, array('role_id' => $access_role_ids));
        }
        $permissions = array();

        foreach ($roles as $access_role) {
            $rows['id-' . $access_role->id] = array(
                'name' => $access_role->name,
                'active' => $access_role->active ? 'Active' : 'Inactive',
                'operations' =>
                    l(t('Edit'), P1_ACCESS_ROLE_MANAGE_URI . $access_role->id) . ' ' .
                    l(t('Delete'), P1_ACCESS_ROLE_MANAGE_URI . $access_role->id . '/delete'),
            );
            $permissions['id-' . $access_role->id]= array(
                'deal' => 'N/A',
                'document_view_name' => 'N/A',
                'document_upload' => 'N/A',
                'document_download' => 'N/A',
                'client_view'=> 'N/A',
                'client_change'=> 'N/A',
                'client_sign'=> 'N/A',
                'follow_up_view_messages'=> 'N/A',
                'follow_up_reply'=> 'N/A',
            );
        }

        foreach ($role_permissions as $permission) {
            $permissions['id-' . $permission->role_id][$permission->permission] = p1_helper_get_access_role_status($permission->permission, 1);
        }

        foreach ($permissions as $key => $permission) {
            $rows[$key]['deal'] = $permission['deal'];
            $rows[$key]['document'] =  $permission['document_view_name'] . ' - ' .
                $permission['document_upload'] . ' - ' .
                $permission['document_download'];
            $rows[$key]['client'] = $permission['client_view'] . ' - ' .
                $permission['client_change'] . ' - ' .
                $permission['client_sign'];
            $rows[$key]['follow_up'] =$permission['follow_up_view_messages'] .' - ' .
                $permission['follow_up_reply'];
        }

        $form['search'] = array(
            '#type' => 'container',
            '#title' => t('Basic Search'),
            '#attributes' => array('class' => array('container-inline')),
        );

        $form['search']['search_name'] = array(
            '#type' => 'textfield',
            '#title' => t('Search by Name'),
            '#default_value' => !empty($search_term) ? $search_term : '',
        );

        $form['search']['filter'] = array(
            '#type' => 'select',
            '#title' => t('Filter by status'),
            '#options' => array(
                1 => 'Active only',
                0 => 'Inactive only',
            ),
            '#empty_option' => 'All types',
            '#default_value' => $status,
        );

        $form['search']['search_submit'] = array(
            '#type' => 'submit',
            '#value' => t('Search'),
        );

        $form['bulk_operations'] = array(
            '#type' => 'fieldset',
            '#title' => t('Bulk Operations'),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
        );

        $form['bulk_operations']['operations'] = array(
            '#type' => 'select',
            '#options' => array(
                0 => t('Select a bulk operation'),
                'activate'=> t('Activate selected Lender Type'),
                'deactivate'=> t('Deactivate selected Lender Type'),
                'delete' => t('Delete selected Lender Type'),
            ),
        );

        $form['bulk_operations']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );

        $form['entities'] = array(
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $rows,
            '#attributes' => array('class' => array('entity-sort-table')),
            '#empty' => t('There are no Access Roles.'),
        );

        return $form;
    }

    /**
     * Form Submit method.
     */
    public function overviewFormSubmit($form, &$form_state) {
        $values = $form_state['input'];
        $access_role_ids = array();

        if (!empty($values['entities'])) {
            foreach ($values['entities'] as $index => $value) {
                if (!empty($value)) {
                    $access_role_ids[] = str_replace('id-', '', $value);
                }
            }

            switch ($values['operations']) {
                case 'delete':
                    drupal_goto(P1_ACCESS_ROLE_URI . '/bulk/delete/' . implode('|', $access_role_ids));
                    break;
                case 'activate':
                    drupal_goto(P1_ACCESS_ROLE_URI . '/bulk/activate/' . implode('|', $access_role_ids));
                    break;
                case 'deactivate':
                    drupal_goto(P1_ACCESS_ROLE_URI . '/bulk/deactivate/' . implode('|', $access_role_ids));
                    break;
            }
        }

        $options = array();
        if (strlen($values['filter'])) {
            $options['active'] = $values['filter'];
        }
        if (strlen($values['search_name'])) {
            $options['search'] = $values['search_name'];
        }
        if (!empty($options)) {
            $options = array('query' => $options);
        }
        drupal_goto(P1_ACCESS_ROLE_URI, $options);
    }
}

/**
 * Custom controller for the administrator UI.
 */
class AccessRolePermissionsUIController extends EntityDefaultUIController {

    /**
     * Override the menu hook for default ui controller.
     */
    public function hook_menu() {
        $items = parent::hook_menu();
        $items[$this->path]['title'] = t('Access Role');
        $items[$this->path]['access callback'] = 'p1_access_role_access_callback';
        $items[$this->path]['access arguments'] = array('administer access role entities');
        $items[$this->path]['type'] = MENU_LOCAL_TASK;
        return $items;
    }

    /**
     * Admin form for searching and doing bulk operations.
     */
    public function overviewForm($form, &$form_state) {
        $form['pager'] = array('#theme' => 'pager');

        $header = array(
            'name' => array('data' => t('Name'), 'field' => 'name'),
            'active' => array('data' => t('Active'), 'field' => 'active'),
            'operations' => array('data' => t('Operations'), 'field' => 'operations'),
        );

        $options = array();

        $search_term = !empty($_GET['search']) ? $_GET['search'] : NULL;
        $active_status = isset($_GET['active']) ? (int) $_GET['active'] : NULL;

        $query = new EntityFieldQuery();
        $query
            ->entityCondition('entity_type', 'p1_access_role');

        if (!empty($search_term)) {
            $query->propertyCondition('name', '%' . $search_term . '%', 'like');
        }
        if (isset($active_status)) {
            $query->propertyCondition('active', $active_status);
        }
        // Check for sort order and sort key.
        if (!empty($_GET['sort']) && !empty($_GET['order'])) {
            $sort = strtoupper($_GET['sort']);
            $order = strtolower($_GET['order']);
            $order = str_replace(' ', '_', $order);
            if ($order != 'operations') {
                $query->propertyOrderBy($order, $sort);
            }
        }

        $query->pager(P1_ACCESS_ROLE_TOTAL_ITEMS_PER_PAGE);

        $result = $query->execute();
        $access_role_results = !empty($result['p1_access_role']) ? $result['p1_access_role'] : array();
        $access_role_array = !empty($access_role_results) ? p1_access_role_load_multiple(array_keys($access_role_results)) : array();
        foreach ($access_role_array as $access_role_id => $access_role) {
            $options['id-' . $access_role_id] = array(
                'name' => $access_role->name,
                'active' => $access_role->active ? t('Yes') : t('No'),
                'operations' =>
                    l(t('Edit'), P1_ACCESS_ROLE_MANAGE_URI . $access_role_id) . ' ' .
                    l(t('Delete'), P1_ACCESS_ROLE_MANAGE_URI . $access_role_id . '/delete'),
            );
        }

        $form['search'] = array(
            '#type' => 'container',
            '#title' => t('Basic Search'),
            '#attributes' => array('class' => array('container-inline')),
        );

        $form['search']['search_text'] = array(
            '#type' => 'textfield',
            '#title' => t('Search by Name'),
            '#default_value' => !empty($search_term) ? $search_term : '',
        );

        $form['search']['filter'] = array(
            '#type' => 'select',
            '#title' => t('Filter by status'),
            '#options' => array(
                1 => 'Active only',
                0 => 'Inactive only',
            ),
            '#empty_option' => 'All types',
            '#default_value' => $active_status,
        );

        $form['search']['search_submit'] = array(
            '#type' => 'submit',
            '#value' => t('Search'),
        );

        $form['bulk_operations'] = array(
            '#type' => 'fieldset',
            '#title' => t('Bulk Operations'),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
        );

        $form['bulk_operations']['operations'] = array(
            '#type' => 'select',
            '#options' => array(
                0 => t('Select a bulk operation'),
                'activate'=> t('Activate selected Access Role'),
                'deactivate'=> t('Deactivate selected Access Role'),
                'delete' => t('Delete selected Access Role'),
            ),
        );

        $form['bulk_operations']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );

        $form['entities'] = array(
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $options,
            '#attributes' => array('class' => array('entity-sort-table')),
            '#empty' => t('There are no Access Role.'),
        );

        return $form;
    }

    /**
     * Form Submit method.
     */
    public function overviewFormSubmit($form, &$form_state) {
        $values = $form_state['input'];
        $access_role_ids = array();

        if (!empty($values['entities'])) {
            foreach ($values['entities'] as $index => $value) {
                if (!empty($value)) {
                    $access_role_ids[] = str_replace('id-', '', $value);
                }
            }

            switch ($values['operations']) {
                case 'delete':
                    drupal_goto(P1_ACCESS_ROLE_URI . '/bulk/delete/' . implode('|', $access_role_ids));
                    break;
                case 'activate':
                    drupal_goto(P1_ACCESS_ROLE_URI . '/bulk/activate/' . implode('|', $access_role_ids));
                    break;
                case 'deactivate':
                    drupal_goto(P1_ACCESS_ROLE_URI . '/bulk/deactivate/' . implode('|', $access_role_ids));
                    break;
            }
        }

        $options = array();
        if (strlen($values['filter'])) {
            $options['active'] = $values['filter'];
        }
        if (strlen($values['search_text'])) {
            $options['search'] = $values['search_text'];
        }
        if (!empty($options)) {
            $options = array('query' => $options);
        }
        drupal_goto(P1_ACCESS_ROLE_URI, $options);
    }
}
