<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_access_role entity
 */

/**
 * Implements hook_form().
 */
function p1_access_role_form($form, &$form_state, $access_role = NULL) {

    $permissions_array = array(
        'deal' => 0,
        'document_view_name' => 0,
        'document_upload' => 0,
        'document_download' => 0,
        'client_view' => 0,
        'client_change' => 0,
        'client_sign' => 0,
        'follow_up_view_messages' => 0,
        'follow_up_reply' => 0,
    );
    if (!isset($access_role->is_new)) {
        $permissions = p1_access_role_permissions_load_multiple(FALSE, array('role_id' => $access_role->id));
        foreach ($permissions as $permission) {
            $permissions_array[$permission->permission] = 1;
        }
    }

    $form = array();

    $form['name'] = array(
        '#title' => 'Role Name',
        '#type' => 'textfield',
        '#default_value' => isset($access_role->name) ? $access_role->name : NULL,
    );
    $form['active'] = array(
        '#type' => 'select',
        '#title' => t('Active'),
        '#default_value' => isset($access_role->active) ? $access_role->active : 0,
        '#options' => array(
            1 => 'Yes',
            0 => 'No',
        )
    );
    $form['permissions'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
    );
    $form['permissions']['deal'] = array(
        '#type' => 'checkbox',
        '#title' => t('View Deal'),
        '#default_value' => $permissions_array['deal'],
    );
    $form['permissions']['document_view_name'] = array(
        '#type' => 'checkbox',
        '#title' => t('View Document Name'),
        '#default_value' => $permissions_array['document_view_name'],
    );
    $form['permissions']['document_upload'] = array(
        '#type' => 'checkbox',
        '#title' => t('Upload Document'),
        '#default_value' => $permissions_array['document_upload'],
    );
    $form['permissions']['document_download'] = array(
        '#type' => 'checkbox',
        '#title' => t('Download Document'),
        '#default_value' => $permissions_array['document_download'],
    );
    $form['permissions']['client_view'] = array(
        '#type' => 'checkbox',
        '#title' => t('View Term Sheet'),
        '#default_value' => $permissions_array['client_view'],
    );
    $form['permissions']['client_change'] = array(
        '#type' => 'checkbox',
        '#title' => t('Change Term Sheet'),
        '#default_value' => $permissions_array['client_change'],
    );
    $form['permissions']['client_sign'] = array(
        '#type' => 'checkbox',
        '#title' => t('Sign Term Sheet'),
        '#default_value' => $permissions_array['client_sign'],
    );
    $form['permissions']['follow_up_view_messages'] = array(
        '#type' => 'checkbox',
        '#title' => t('View Status Messages'),
        '#default_value' => $permissions_array['follow_up_view_messages'],
    );
    $form['permissions']['follow_up_reply'] = array(
        '#type' => 'checkbox',
        '#title' => t('Reply To Status Messages'),
        '#default_value' => $permissions_array['follow_up_reply'],
    );

    $form['actions'] = array(
        '#type' => 'actions',
    );

    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Save',
    );

    return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_access_role_form_validate($form, &$form_state) {
    $element = $form['name'];
    $entity = entity_load('p1_access_role', FALSE, array('name' => $element['#value']), FALSE);
    if (count($entity)) {
        if ($form_state['op'] == 'add' || $form_state['p1_access_role']->id != array_shift($entity)->id) {
            form_error($element, t('Access role with inputted name is exist. Please choose another name.'));
        }
    }
}

/**
 * Implements hook_form_submit().
 */
function p1_access_role_form_submit($form, &$form_state) {
    $access_role = entity_ui_form_submit_build_entity($form, $form_state);
    $id = $access_role->save();
    $current_permissions = p1_access_role_permissions_load_multiple(FALSE, array('role_id' => $access_role->id));
    $current_permissions_array = array();
    $to_delete = array();
    foreach ($current_permissions as $current_permission) {
        $current_permissions_array[$current_permission->permission] = $current_permission->id;
    }
    foreach ($form_state['values']['permissions'] as $permission => $status) {
        if ($status) {
            if (!isset($current_permissions_array[$permission])) {
                $entity_property_value_array = array();
                $entity_property_value_array['role_id'] = $access_role->identifier();
                $entity_property_value_array['permission'] = $permission;
                $entity = entity_create('p1_access_role_permissions', $entity_property_value_array);
                $entity->save();
            }
        }
        else {
            if (isset($current_permissions_array[$permission])) {
                $to_delete[] = $current_permissions_array[$permission];
            }
        }
    }
    p1_access_role_permissions_delete_multiple($to_delete);
    drupal_set_message(t('@name Access Role has been saved.', array('@name' => $access_role->name)));
    $form_state['redirect'] = P1_ACCESS_ROLE_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_access_role.
 */
function p1_access_role_bulk_delete($form, &$form_state, $ids) {
    $ids = explode('|', $ids);
    $access_role = p1_access_role_load_multiple($ids);

    $form = array();
    $form_state['ids'] = $ids;

    $variables = array(
        'type' => 'ul',
        'items' => array(),
        'title' => '',
        'attributes' => array(),
    );

    foreach ($access_role as $access_role) {
        $variables['items'][] = $access_role->name;
    }

    $form['summary'] = array(
        '#markup' => theme_item_list($variables),
    );

    return confirm_form($form, t('Delete all Access Roles?'), P1_ACCESS_ROLE_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_access_role.
 */
function p1_access_role_bulk_activate($form, &$form_state, $ids) {
    $ids = explode('|', $ids);
    $access_role = p1_access_role_load_multiple($ids);

    $form = array();
    $form_state['ids'] = $ids;

    $variables = array(
        'type' => 'ul',
        'items' => array(),
        'title' => '',
        'attributes' => array(),
    );

    foreach ($access_role as $access_role) {
        $variables['items'][] = $access_role->name;
    }

    $form['summary'] = array(
        '#markup' => theme_item_list($variables),
    );

    return confirm_form($form, t('Activate all Access Roles?'), P1_ACCESS_ROLE_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_access_role.
 */
function p1_access_role_bulk_deactivate($form, &$form_state, $ids) {
    $ids = explode('|', $ids);
    $access_role = p1_access_role_load_multiple($ids);

    $form = array();
    $form_state['ids'] = $ids;

    $variables = array(
        'type' => 'ul',
        'items' => array(),
        'title' => '',
        'attributes' => array(),
    );

    foreach ($access_role as $access_role) {
        $variables['items'][] = $access_role->name;
    }

    $form['summary'] = array(
        '#markup' => theme_item_list($variables),
    );

    return confirm_form($form, t('Deactivate all Access Roles?'), P1_ACCESS_ROLE_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_access_role_bulk_delete_submit($form, &$form_state) {
    $ids = $form_state['ids'];
    p1_access_role_delete_multiple($ids);

    drupal_set_message(t('Access Roles deleted'));
    drupal_goto(P1_ACCESS_ROLE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_access_role_bulk_activate_submit($form, &$form_state) {
    $ids = $form_state['ids'];
    p1_access_role_activate_multiple($ids);

    drupal_set_message(t('Access Roles actived'));
    drupal_goto(P1_ACCESS_ROLE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_access_role_bulk_deactivate_submit($form, &$form_state) {
    $ids = $form_state['ids'];
    p1_access_role_deactivate_multiple($ids);

    drupal_set_message(t('Access Roles actived'));
    drupal_goto(P1_ACCESS_ROLE_URI);
}
