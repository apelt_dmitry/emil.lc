<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_property_type entity
 */

/**
 * Implements hook_form().
 */
function p1_property_type_form($form, &$form_state, $property_type = NULL) {
	$form = array();

	$form['name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#default_value' => isset($property_type->name) ? $property_type->name : '',
		'#required' => TRUE,
		'#maxlength' => 255,
	);

	$form['active'] = array(
		'#title' => t('Active'),
		'#type' => 'select',
		'#options' => array(
			0 => 'No',
			1 => 'Yes',
		),
		'#default_value' => isset($property_type->active) ? $property_type->active : 0,
	);

	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
			'#type' => 'submit',
			'#value' => isset($property_type->id) ? t('Update Property Type') : t('Save Property Type'),
		),
		'delete_link' => array(
			'#markup' => isset($property_type->id) ? l(t('Delete'), P1_PROPERTY_TYPE_MANAGE_URI . $property_type->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''));
	return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_property_type_form_validate($form, &$form_state) {
	$element = $form['name'];
	$entity = entity_load('p1_property_type', FALSE, array('name' => $element['#value']), FALSE);
		if (count($entity)) {
			if ($form_state['op'] == 'add' || $form_state['p1_property_type']->id != array_shift($entity)->id) {
			form_error($element, t('Property type with inputted name is exist. Please choose another name.'));
		}
	}
}

/**
 * Implements hook_form_submit().
 */
function p1_property_type_form_submit($form, &$form_state) {
	$property_type = entity_ui_form_submit_build_entity($form, $form_state);
	$property_type->save();
	drupal_set_message(t('@name Property Type has been saved.', array('@name' => $property_type->name)));
	$form_state['redirect'] = P1_PROPERTY_TYPE_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_property_type.
 */
function p1_property_type_bulk_delete($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$property_types = p1_property_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($property_types as $property_type) {
		$variables['items'][] = $property_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Delete all Property Types?'), P1_PROPERTY_TYPE_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_property_type.
 */
function p1_property_type_bulk_activate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$property_types = p1_property_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($property_types as $property_type) {
		$variables['items'][] = $property_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Activate all Property Types?'), P1_PROPERTY_TYPE_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_property_type.
 */
function p1_property_type_bulk_deactivate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$property_types = p1_property_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($property_types as $property_type) {
		$variables['items'][] = $property_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Deactivate all Property Types?'), P1_PROPERTY_TYPE_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_property_type_bulk_delete_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_property_type_delete_multiple($ids);

	drupal_set_message(t('Property Types deleted'));
	drupal_goto(P1_PROPERTY_TYPE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_property_type_bulk_activate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_property_type_activate_multiple($ids);

	drupal_set_message(t('Property Types actived'));
	drupal_goto(P1_PROPERTY_TYPE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_property_type_bulk_deactivate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_property_type_deactivate_multiple($ids);

	drupal_set_message(t('Property Types actived'));
	drupal_goto(P1_PROPERTY_TYPE_URI);
}
