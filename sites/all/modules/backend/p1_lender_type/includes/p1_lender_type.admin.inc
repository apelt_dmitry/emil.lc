<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_lender_type entity
 */

/**
 * Implements hook_form().
 */
function p1_lender_type_form($form, &$form_state, $lender_type = NULL) {
	$form = array();

	$form['name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#default_value' => isset($lender_type->name) ? $lender_type->name : '',
		'#required' => TRUE,
		'#maxlength' => 255,
	);

	$form['active'] = array(
		'#title' => t('Active'),
		'#type' => 'select',
		'#options' => array(
			0 => 'No',
			1 => 'Yes',
		),
		'#default_value' => isset($lender_type->active) ? $lender_type->active : 0,
	);

	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
			'#type' => 'submit',
			'#value' => isset($lender_type->id) ? t('Update Lender Type') : t('Save Lender Type'),
		),
		'delete_link' => array(
			'#markup' => isset($lender_type->id) ? l(t('Delete'), P1_LENDER_TYPE_MANAGE_URI . $lender_type->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''));
	return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_lender_type_form_validate($form, &$form_state) {
	$element = $form['name'];
	$entity = entity_load('p1_lender_type', FALSE, array('name' => $element['#value']), FALSE);
		if (count($entity)) {
			if ($form_state['op'] == 'add' || $form_state['p1_lender_type']->id != array_shift($entity)->id) {
			form_error($element, t('Lender type with inputted name is exist. Please choose another name.'));
		}
	}
}

/**
 * Implements hook_form_submit().
 */
function p1_lender_type_form_submit($form, &$form_state) {
	$lender_type = entity_ui_form_submit_build_entity($form, $form_state);
	$lender_type->save();
	drupal_set_message(t('@name Lender Type has been saved.', array('@name' => $lender_type->name)));
	$form_state['redirect'] = P1_LENDER_TYPE_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_lender_type.
 */
function p1_lender_type_bulk_delete($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lender_types = p1_lender_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lender_types as $lender_type) {
		$variables['items'][] = $lender_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Delete all Lender Types?'), P1_LENDER_TYPE_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_lender_type.
 */
function p1_lender_type_bulk_activate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lender_types = p1_lender_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lender_types as $lender_type) {
		$variables['items'][] = $lender_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Activate all Lender Types?'), P1_LENDER_TYPE_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_lender_type.
 */
function p1_lender_type_bulk_deactivate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lender_types = p1_lender_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lender_types as $lender_type) {
		$variables['items'][] = $lender_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Deactivate all Lender Types?'), P1_LENDER_TYPE_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_lender_type_bulk_delete_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lender_type_delete_multiple($ids);

	drupal_set_message(t('Lender Types deleted'));
	drupal_goto(P1_LENDER_TYPE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_lender_type_bulk_activate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lender_type_activate_multiple($ids);

	drupal_set_message(t('Lender Types actived'));
	drupal_goto(P1_LENDER_TYPE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_lender_type_bulk_deactivate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lender_type_deactivate_multiple($ids);

	drupal_set_message(t('Lender Types actived'));
	drupal_goto(P1_LENDER_TYPE_URI);
}
