<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_features entity
 */

/**
 * Implements hook_form().
 */
function p1_features_form($form, &$form_state, $features = NULL) {
  $form = array();

  $form['definition'] = array(
    '#type' => 'container',
    '#prefix' => '<div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a>Definition</a>
      </h4>
    </div>
    <div id="collapse1" class="panel">
      <div class="panel-body">',
    '#suffix' => '</div></div></div></div>',
  );

  $form['definition']['lending_program_id'] = array(
    '#title' => t('Lending Program'),
    '#type' => 'select',
    '#options' => p1_helper_get_lending_programs(),
    '#empty_option' => t('- Select lending program -'),
    '#default_value' => isset($features->lending_program_id) ? $features->lending_program_id: NULL,
    '#required' => TRUE,
    '#prefix' => '<div class="col-md-6">',
    '#suffix' => '</div>',
  );

  $form['definition']['type'] = array(
    '#title' => t('Features Type'),
    '#type' => 'select',
    '#options' => p1_helper_get_features_type(),
    '#empty_option' => t('- Select Feature Type -'),
    '#default_value' => isset($features->type) ? $features->type: NULL,
    '#required' => TRUE,
    '#prefix' => '<div class="col-md-6">',
    '#suffix' => '</div>',
  );

  $form['definition']['years'] = array(
    '#title' => t('Years'),
    '#type' => 'select',
    '#options' => p1_helper_get_100_years_array(),
    '#default_value' => isset($features->years) ? $features->years : NULL,
    '#prefix' => '<div class="col-md-6">',
    '#suffix' => '</div>',
  );

  $form['definition']['interest'] = array(
    '#title' => t('Interest'),
    '#type' => 'select',
    '#options' => array(
      'N/A' => 'N/A',
      'Fixed' => 'Fixed',
      'Variable' => 'Variable',
    ),
    '#default_value' => isset($features->interest) ? $features->interest : NULL,
    '#prefix' => '<div class="col-md-6">',
    '#suffix' => '</div>',
  );

  $form['definition']['state'] = array(
    '#title' => t('State'),
    '#type' => 'select',
    '#options' => p1_helper_get_states(),
    '#empty_option' => t('- Select State -'),
    '#default_value' => isset($features->state) ? $features->state : NULL,
    '#prefix' => '<div class="col-md-6">',
    '#suffix' => '</div>',
  );

  $form['calculations'] = array(
    '#type' => 'container',
    '#prefix' => '<div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a>Calculations</a>
      </h4>
    </div>
    <div id="collapse1" class="panel">
      <div class="panel-body">',
    '#suffix' => '</div></div></div></div>',
  );

  $form['calculations']['comparison'] = array(
    '#title' => t('Comparison'),
    '#type' => 'select',
    '#options' => array(
      'equal' => 'equal',
      'not equal' => 'not equal',
      'less than' => 'less than',
      'greater than' => 'greater than',
      'less than or equal' => 'less than or equal to',
      'greater than or equal' => 'greater than or equal to',
    ),
    '#empty_option' => t('- Select Comparison -'),
    '#default_value' => isset($features->comparison) ? $features->comparison : NULL,
    '#prefix' => '<div class="col-md-4">',
    '#suffix' => '</div>',
  );

  $form['calculations']['features_limit'] = array(
    '#title' => t('Limit'),
    '#type' => 'textfield',
    '#default_value' => isset($features->features_limit) ? $features->features_limit : '0',
    '#prefix' => '<div class="col-md-4">',
    '#suffix' => '</div>',
  );

  $form['calculations']['operation'] = array(
    '#title' => t('Operation'),
    '#type' => 'select',
    '#options' => array(
      'increase' => 'increase',
      'decrease' => 'decrease'
    ),
    '#default_value' => isset($features->operation) ? $features->operation : NULL,
    '#prefix' => '<div class="col-md-4">',
    '#suffix' => '</div>',
  );

  $form['calculations']['adjusted_by'] = array(
    '#title' => t('Adjusted by'),
    '#type' => 'textfield',
    '#default_value' => isset($features->adjusted_by) ? $features->adjusted_by : '0.000',
    '#prefix' => '<div class="col-md-4">',
    '#suffix' => '</div>',
  );

  $form['calculations']['unit'] = array(
    '#title' => t('Unit'),
    '#type' => 'select',
    '#options' => array(
      'interest' => 'interest',
      'point' => 'point'
    ),
    '#default_value' => isset($features->point) ? $features->point : NULL,
    '#prefix' => '<div class="col-md-4">',
    '#suffix' => '</div>',
  );

  $form['calculations']['active'] = array(
    '#title' => t('Active'),
    '#type' => 'select',
    '#options' => array(
      0 => 'No',
      1 => 'Yes',
    ),
    '#default_value' => isset($features->active) ? $features->active : 0,
    '#prefix' => '<div class="col-md-4">',
    '#suffix' => '</div>',
  );

  $form['actions'] = array(
    '#type' => 'actions',
    '#prefix' => '<div class="col-md-12">',
    '#suffix' => '</div>',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($features->id) ? t('Update Features') : t('Save Features'),
    ),
    'delete_link' => array(
      '#markup' => isset($features->id) ? l(t('Delete'), P1_FEATURES_MANAGE_URI . $features->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''));
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_features_form_validate($form, &$form_state) {

}

/**
 * Implements hook_form_submit().
 */
function p1_features_form_submit($form, &$form_state) {
  $features = entity_ui_form_submit_build_entity($form, $form_state);
  $features->save();
  drupal_set_message(t('Features has been saved.'));
  $form_state['redirect'] = P1_FEATURES_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_features.
 */
function p1_features_bulk_delete($form, &$form_state, $ids) {
  $ids = explode('|', $ids);
  $features = p1_features_load_multiple($ids);

  $form = array();
  $form_state['ids'] = $ids;

  $variables = array(
    'type' => 'ul',
    'items' => array(),
    'title' => '',
    'attributes' => array(),
  );

  $form['summary'] = array(
    '#markup' => theme_item_list($variables),
  );

  return confirm_form($form, t('Delete all Features?'), P1_FEATURES_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_features.
 */
function p1_features_bulk_activate($form, &$form_state, $ids) {
  $ids = explode('|', $ids);
  $features = p1_features_load_multiple($ids);

  $form = array();
  $form_state['ids'] = $ids;

  $variables = array(
    'type' => 'ul',
    'items' => array(),
    'title' => '',
    'attributes' => array(),
  );

  $form['summary'] = array(
    '#markup' => theme_item_list($variables),
  );

  return confirm_form($form, t('Activate all Features?'), P1_FEATURES_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_features.
 */
function p1_features_bulk_deactivate($form, &$form_state, $ids) {
  $ids = explode('|', $ids);
  $features = p1_features_load_multiple($ids);

  $form = array();
  $form_state['ids'] = $ids;

  $variables = array(
    'type' => 'ul',
    'items' => array(),
    'title' => '',
    'attributes' => array(),
  );

  $form['summary'] = array(
    '#markup' => theme_item_list($variables),
  );

  return confirm_form($form, t('Deactivate all Features?'), P1_FEATURES_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_features_bulk_delete_submit($form, &$form_state) {
  $ids = $form_state['ids'];
  p1_features_delete_multiple($ids);

  drupal_set_message(t('Features deleted'));
  drupal_goto(P1_FEATURES_URI);
}

/**
 * Implements hook_submit().
 */
function p1_features_bulk_activate_submit($form, &$form_state) {
  $ids = $form_state['ids'];
  p1_features_activate_multiple($ids);

  drupal_set_message(t('Features actived'));
  drupal_goto(P1_FEATURES_URI);
}

/**
 * Implements hook_submit().
 */
function p1_features_bulk_deactivate_submit($form, &$form_state) {
  $ids = $form_state['ids'];
  p1_features_deactivate_multiple($ids);

  drupal_set_message(t('Features actived'));
  drupal_goto(P1_FEATURES_URI);
}
