<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_program_type entity
 */

/**
 * Implements hook_form().
 */
function p1_program_type_form($form, &$form_state, $program_type = NULL) {
	$form = array();

	$form['name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#default_value' => isset($program_type->name) ? $program_type->name : '',
		'#required' => TRUE,
		'#maxlength' => 255,
	);

	$form['active'] = array(
		'#title' => t('Active'),
		'#type' => 'select',
		'#options' => array(
			0 => 'No',
			1 => 'Yes',
		),
		'#default_value' => isset($program_type->active) ? $program_type->active : 0,
	);

	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
			'#type' => 'submit',
			'#value' => isset($program_type->id) ? t('Update Program Type') : t('Save Program Type'),
		),
		'delete_link' => array(
			'#markup' => isset($program_type->id) ? l(t('Delete'), P1_PROGRAM_TYPE_MANAGE_URI . $program_type->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''));
	return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_program_type_form_validate($form, &$form_state) {
	$element = $form['name'];
	$entity = entity_load('p1_program_type', FALSE, array('name' => $element['#value']), FALSE);
		if (count($entity)) {
			if ($form_state['op'] == 'add' || $form_state['p1_program_type']->id != array_shift($entity)->id) {
			form_error($element, t('Program type with inputted name is exist. Please choose another name.'));
		}
	}
}

/**
 * Implements hook_form_submit().
 */
function p1_program_type_form_submit($form, &$form_state) {
	$program_type = entity_ui_form_submit_build_entity($form, $form_state);
	$program_type->save();
	drupal_set_message(t('@name Program Type has been saved.', array('@name' => $program_type->name)));
	$form_state['redirect'] = P1_PROGRAM_TYPE_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_program_type.
 */
function p1_program_type_bulk_delete($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$program_types = p1_program_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($program_types as $program_type) {
		$variables['items'][] = $program_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Delete all Program Types?'), P1_PROGRAM_TYPE_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_program_type.
 */
function p1_program_type_bulk_activate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$program_types = p1_program_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($program_types as $program_type) {
		$variables['items'][] = $program_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Activate all Program Types?'), P1_PROGRAM_TYPE_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_program_type.
 */
function p1_program_type_bulk_deactivate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$program_types = p1_program_type_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($program_types as $program_type) {
		$variables['items'][] = $program_type->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Deactivate all Program Types?'), P1_PROGRAM_TYPE_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_program_type_bulk_delete_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_program_type_delete_multiple($ids);

	drupal_set_message(t('Program Types deleted'));
	drupal_goto(P1_PROGRAM_TYPE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_program_type_bulk_activate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_program_type_activate_multiple($ids);

	drupal_set_message(t('Program Types actived'));
	drupal_goto(P1_PROGRAM_TYPE_URI);
}

/**
 * Implements hook_submit().
 */
function p1_program_type_bulk_deactivate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_program_type_deactivate_multiple($ids);

	drupal_set_message(t('Program Types actived'));
	drupal_goto(P1_PROGRAM_TYPE_URI);
}
