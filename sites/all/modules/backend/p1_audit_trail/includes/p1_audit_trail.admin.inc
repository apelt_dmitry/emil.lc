<?php

function _p1_audit_trail_get_dependent_modules() {
	$module_data = system_rebuild_module_data();
	$dependents = array_keys($module_data['p1_audit_trail']->required_by);
	foreach ($dependents as $key => $module) {
		if (!module_exists($module)) {
			unset($dependents[$key]);
		}
	}
	return $dependents;
}

function p1_audit_trail_form($form, &$form_state) {

	$depent = _p1_audit_trail_get_dependent_modules();
	$modules = array();

	foreach ($depent as $item) {
		if ($item == 'p1_helper') continue;
		$modules[$item] = ucwords(str_replace(array('p1', '_'), array('',' '), $item));
	}

	$module = isset($_GET['mod']) && $_GET['mod'] ? $_GET['mod'] : NULL;
	$operation = isset($_GET['op']) && $_GET['op'] ? $_GET['op'] : NULL;
	$username = isset($_GET['user']) && $_GET['user'] ? $_GET['user'] : NULL;
	$from = isset($_GET['from']) && $_GET['from'] ? strtotime($_GET['from']) : REQUEST_TIME;
	$to = isset($_GET['to']) && $_GET['to'] ? strtotime($_GET['to']) : REQUEST_TIME;

	list($header, $rows) = p1_audit_trail_table($module, $operation, $username, $from, $to);

	$form['search'] = array(
		'#type' => 'container',
		'#title' => t('Basic Search'),
		'#attributes' => array('class' => array('container-inline')),
	);

	$form['search']['username'] = array(
		'#title' => t('Username'),
		'#type' => 'textfield',
		'#size' => 30,
		'#maxlength' => 60,
		'#autocomplete_path' => 'user/autocomplete',
		'#default_value' => $username,
	);

	$form['search']['module'] = array(
		'#title' => t('Module'),
		'#type' => 'select',
		'#options' => $modules,
		'#empty_option' => 'All types',
		'#default_value' => $module,
	);

	$form['search']['operation'] = array(
		'#title' => t('Operation'),
		'#type' => 'select',
		'#options' => array(
			'save' => 'Save',
			'delete' => 'Delete',
			'edit' => 'Edit',
		),
		'#empty_option' => 'All operations',
		'#default_value' => $operation,
	);

	$form['search']['from'] = array(
		'#type' => 'date_popup',
		'#required' => TRUE,
		'#date_format' => 'm/d/Y',
		'#attributes' => array('autocomplete' =>'off','readonly' => 'readonly'),
		'#default_value' => date('Y-m-d', $from),
	);

	$form['search']['to'] = array(
		'#type' => 'date_popup',
		'#required' => TRUE,
		'#date_format' => 'm/d/Y',
		'#attributes' => array('autocomplete' =>'off','readonly' => 'readonly'),
		'#default_value' => date('Y-m-d', $to),
	);

	$form['search']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Search'),
	);

	$form['table'] = array(
		'#theme' => 'table',
		'#header' => $header,
		'#rows' => $rows,
		'#empty' => t('Table has no row!'),
	);

	$form['pager'] = array('#markup' => theme('pager'));

	return $form;
}

function p1_audit_trail_table($module = NULL, $op = NULL, $user = NULL, $from, $to) {

	$header = array(
		array('data' => 'ID',           'field' => 'audit_id'),
		array('data' => 'Username',     'field' => 'name'),
		array('data' => 'Date',         'field' => 'audit_timestamp', 'sort' => 'desc'),
		array('data' => 'Module',       'field' => 'audit_module'),
		array('data' => 'Entity ID',    'field' => 'audit_eid'),
		array('data' => 'Operation',    'field' => 'audit_op'),
		array('data' => 'IP',           'field' => 'audit_ip'),
		array('data' => 'Data before',  'field' => 'audit_data_before'),
		array('data' => 'Data after',   'field' => 'audit_data_after'),
	);

	$query = db_select('p1_audit_trail', 'at');
	$query->leftJoin('users', 'u', 'u.uid = at.audit_uid');
	$query = $query->fields('at', array(
		'audit_id',
		'audit_eid',
		'audit_op',
		'audit_module',
		'audit_data_before',
		'audit_data_after',
		'audit_ip',
		'audit_timestamp',
	))
		->fields('u', array('name'))
		->condition('at.audit_timestamp', array($from, $to + 60 * 60 * 24 -1), 'BETWEEN');

	if (isset($user)) {
		$query->condition('u.name', $user);
	}
	if (isset($module)) {
		$query->condition('at.audit_module', $module);
	}
	if (isset($op)) {
		$query->condition('at.audit_op', $op);
	}

	$query = $query->extend('PagerDefault')
		->limit(20)
		->extend('TableSort')
		->orderByHeader($header)
		->execute();

	$rows = array();
	foreach ($query as $row) {
		$rows[] = array(
			$row->audit_id,
			$row->name,
			format_date($row->audit_timestamp, 'short'),
			ucwords(str_replace(array('p1', '_'), array('',' '), $row->audit_module)),
			$row->audit_eid,
			$row->audit_op,
			$row->audit_ip,
			'<pre>'.print_r(unserialize($row->audit_data_before), TRUE).'</pre>',
			'<pre>'.print_r(unserialize($row->audit_data_after), TRUE).'</pre>',
		);
	}
	return array($header, $rows);
}

function p1_audit_trail_form_submit($form, &$form_state) {
	$mod = $form_state['values']['module'];
	$op = $form_state['values']['operation'];
	$user = $form_state['values']['username'];
	$from = $form_state['values']['from'];
	$to = $form_state['values']['to'];

	$query = array();

	if (strlen($mod)) {
		$query['mod'] = $mod;
	}
	if (strlen($op)) {
		$query['op'] = $op;
	}
	if (strlen($user)) {
		$query['user'] = $user;
	}
	$query['from'] = $from;
	$query['to'] = $to;

	drupal_goto(current_path(), array('query' => $query));
}
