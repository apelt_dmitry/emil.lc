<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_lending_program entity
 */

/**
 * Implements hook_form().
 */
function p1_lending_program_form($form, &$form_state, $lending_program = NULL) {
	$form = array();
	$checboxes = array();
	if (isset($lending_program->id)) {

		$lending_program_states = db_select('p1_lending_program_states')
			->fields(NULL, array('state_code', 'is_use'))
			->condition('lending_program_id', $lending_program->id)
			->execute()
			->fetchAllAssoc('state_code');

		$checboxes = array();
		foreach ($lending_program_states as $key => $value) {
			if ($value->is_use) {
				$checboxes[] = $key;
			}
		}
	}

	$form['name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#default_value' => isset($lending_program->name) ? $lending_program->name : '',
		'#required' => TRUE,
		'#maxlength' => 255,
		'#prefix' => '<div class="col-md-6">',
		'#suffix' => '</div>',
	);

	$form['active'] = array(
		'#title' => t('Active'),
		'#type' => 'select',
		'#options' => array(
			1 => 'Yes',
			0 => 'No',
		),
		'#default_value' => isset($lending_program->active) ? $lending_program->active : 0,
		'#prefix' => '<div class="col-md-2">',
		'#suffix' => '</div>',
	);

	$form['lender_id'] = array(
		'#title' => t('Lender'),
		'#type' => 'select',
		'#options' => p1_helper_get_lenders(),
		'#default_value' => isset($lending_program->lender_id) ? $lending_program->lender_id : 0,
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['program_type_id'] = array(
		'#title' => t('Program type'),
		'#type' => 'select',
		'#options' => p1_helper_get_program_types(),
		'#default_value' => isset($lending_program->program_type_id) ? $lending_program->program_type_id : 0,
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['property_type_id'] = array(
		'#title' => t('Property type'),
		'#type' => 'select',
		'#options' => p1_helper_get_property_types(),
		'#default_value' => isset($lending_program->property_type_id) ? $lending_program->property_type_id : 0,
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['min_loan'] = array(
		'#title' => t('Min Loan'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->min_loan) ? $lending_program->min_loan  : '',
		'#attributes' => array('class' => array('currency')),
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['max_loan'] = array(
		'#title' => t('Max Loan'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->max_loan) ? $lending_program->max_loan  : '',
		'#attributes' => array('class' => array('currency')),
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['lp_index'] = array(
		'#title' => t('Index'),
		'#type' => 'select',
		'#options' => array(
			'libor' => 'LIBOR',
			'wsj' => 'WSJ Prime Rate',
		),
		'#default_value' => isset($lending_program->pl_index) ? $lending_program->pl_index : 'wsj',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['margin'] = array(
		'#title' => t('margin'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->margin) ? $lending_program->margin : '0',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['is_recourse'] = array(
		'#title' => t('Is Recourse'),
		'#type' => 'select',
		'#options' => array(
			1 => 'On',
			0 => 'Off',
		),
		'#default_value' => isset($lending_program->is_recourse) ? $lending_program->is_recourse : 1,
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['lender_fee'] = array(
		'#title' => t('Lender fee'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->lender_fee) ? $lending_program->lender_fee : '0.00',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['underwriting_fees'] = array(
		'#title' => t('Underwriting Fees'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->underwriting_fees) ? $lending_program->underwriting_fees  : '0.00',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['processing_fee'] = array(
		'#title' => t('Processing Fee'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->processing_fee) ? $lending_program->processing_fee  : '0.00',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['appraisal_fee'] = array(
		'#title' => t('Appraisal Fee'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->appraisal_fee) ? $lending_program->appraisal_fee  : '0.00',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['application_deposit'] = array(
		'#title' => t('Application Deposit'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->application_deposit) ? $lending_program->application_deposit  : '0.00',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['doc_type'] = array(
		'#title' => t('Doc type'),
		'#type' => 'select',
		'#options' => p1_helper_get_docs_category(),
		'#default_value' => isset($lending_program->doc_type) ? $lending_program->doc_type : NULL,
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['min_dscr'] = array(
		'#title' => t('Min. DSCR'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->min_dscr) ? $lending_program->min_dscr  : '0.00',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['min_vacancy_ratio'] = array(
		'#title' => t('Min. Vacancy Ratio'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->min_vacancy_ratio) ? $lending_program->min_vacancy_ratio : '0',
		'#prefix' => '<div class="col-md-4">',
		'#suffix' => '</div>',
	);

	$form['max_life_adjustment'] = array(
		'#title' => t('Max Life Adjustment'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->max_life_adjustment) ? $lending_program->max_life_adjustment : '0',
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);

	$form['max_first_adjustment'] = array(
		'#title' => t('Max First Adjustment'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->max_first_adjustment) ? $lending_program->max_first_adjustment : '0',
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);

	$form['max_subsequent_adjustment'] = array(
		'#title' => t('Max Subsequent Adjustment'),
		'#type' => 'textfield',
		'#default_value' => !empty($lending_program->max_subsequent_adjustment) ? $lending_program->max_subsequent_adjustment : '0',
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);

	$form['escrow_required'] = array(
		'#title' => t('Escrow Required'),
		'#type' => 'select',
		'#options' => array(
			1 => 'On',
			0 => 'Off',
		),
		'#default_value' => isset($lending_program->escrow_required) ? $lending_program->escrow_required : 1,
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);

	$form['accept_bankruptcy'] = array(
		'#title' => t('Accept Bankruptcy'),
		'#type' => 'select',
		'#options' => p1_helper_get_years_array(),
		'#default_value' => isset($lending_program->accept_bankruptcy) ? $lending_program->accept_bankruptcy : 0,
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);

	$form['accept_foreclosure'] = array(
		'#title' => t('Accept Foreclosure'),
		'#type' => 'select',
		'#options' => p1_helper_get_years_array(),
		'#default_value' => isset($lending_program->accept_foreclosure) ? $lending_program->accept_foreclosure : 0,
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);

	$form['msa'] = array(
		'#title' => t('MSA'),
		'#type' => 'textfield',
		'#size' => 5,
		'#max_length' => 5,
		'#default_value' => isset($lending_program->accept_foreclosure) ? $lending_program->accept_foreclosure : 0,
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);

	$form['offered'] = array(
		'#title' => t('Offered'),
		'#type' => 'select',
		'#options' => array(
			'all states' => t('All states'),
			'specific states' => t('Specific states'),
		),
		'#default_value' => isset($lending_program->accept_foreclosure) ? $lending_program->accept_foreclosure : 0,
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);

	$form['states'] = array(
		'#title' => t('States'),
		'#type' => 'checkboxes',
		'#options' => p1_helper_get_states(),
	    '#default_value' => $checboxes,
		'#prefix' => '<div class="col-md-12">',
		'#suffix' => '</div>',
	);

	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
			'#type' => 'submit',
			'#value' => isset($lending_program->id) ? t('Update Lending Program') : t('Save Lending Program'),
		),
		'delete_link' => array(
			'#markup' => isset($lending_program->id) ? l(t('Delete'), P1_LENDING_PROGRAM_MANAGE_URI . $lending_program->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''),
		'#prefix' => '<div class="col-md-3">',
		'#suffix' => '</div>',
	);
	return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_lending_program_form_validate($form, &$form_state) {
	$element = $form['name'];
	$entity = entity_load('p1_lending_program', FALSE, array('name' => $element['#value']), FALSE);
		if (count($entity)) {
			if ($form_state['op'] == 'add' || $form_state['p1_lending_program']->id != array_shift($entity)->id) {
			form_error($element, t('Lending program with inputted name is exist. Please choose another name.'));
		}
	}
}

/**
 * Implements hook_form_submit().
 */
function p1_lending_program_form_submit($form, &$form_state) {
	$lending_program = entity_ui_form_submit_build_entity($form, $form_state);
	$lending_program->min_loan = str_replace(array('$',','), '', $lending_program->min_loan);
	$lending_program->max_loan = str_replace(array('$',','), '', $lending_program->max_loan);
	$lending_program->lender_fee = str_replace(array('$',','), '', $lending_program->lender_fee);
	$lending_program->underwriting_fees = str_replace(array('$',','), '', $lending_program->underwriting_fees);
	$lending_program->processing_fee = str_replace(array('$',','), '', $lending_program->processing_fee);
	$lending_program->application_deposit = str_replace(array('$',','), '', $lending_program->application_deposit);
	$lending_program->min_dscr = str_replace(array('$',','), '', $lending_program->min_dscr);
	$lending_program->margin = str_replace('%', '', $lending_program->margin);
	$lending_program->min_vacancy_ratio = str_replace('%', '', $lending_program->min_vacancy_ratio);
	$lending_program->max_life_adjustment = str_replace('%', '', $lending_program->max_life_adjustment);
	$lending_program->max_first_adjustment = str_replace('%', '', $lending_program->max_first_adjustment);
	$lending_program->max_subsequent_adjustment = str_replace('%', '', $lending_program->max_subsequent_adjustment);
	$lending_program->save();



	drupal_set_message(t('@name Lending Program has been saved.', array('@name' => $lending_program->name)));
	$form_state['redirect'] = P1_LENDING_PROGRAM_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_lending_program.
 */
function p1_lending_program_bulk_delete($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lending_programs = p1_lending_program_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lending_programs as $lending_program) {
		$variables['items'][] = $lending_program->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Delete all Lending Programs?'), P1_LENDING_PROGRAM_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_lending_program.
 */
function p1_lending_program_bulk_activate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lending_programs = p1_lending_program_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lending_programs as $lending_program) {
		$variables['items'][] = $lending_program->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Activate all Lending Programs?'), P1_LENDING_PROGRAM_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_lending_program.
 */
function p1_lending_program_bulk_deactivate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lending_programs = p1_lending_program_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lending_programs as $lending_program) {
		$variables['items'][] = $lending_program->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Deactivate all Lending Programs?'), P1_LENDING_PROGRAM_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_lending_program_bulk_delete_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lending_program_delete_multiple($ids);

	drupal_set_message(t('Lending Programs deleted'));
	drupal_goto(P1_LENDING_PROGRAM_URI);
}

/**
 * Implements hook_submit().
 */
function p1_lending_program_bulk_activate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lending_program_activate_multiple($ids);

	drupal_set_message(t('Lending Programs actived'));
	drupal_goto(P1_LENDING_PROGRAM_URI);
}

/**
 * Implements hook_submit().
 */
function p1_lending_program_bulk_deactivate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lending_program_deactivate_multiple($ids);

	drupal_set_message(t('Lending Programs actived'));
	drupal_goto(P1_LENDING_PROGRAM_URI);
}

/**
 * Implements hook_form().
 */
function p1_lending_program_view_entity($lending_program) {
	drupal_set_title($lending_program->name . ' Lending Program');

  $data = db_select('p1_lending_program_states')
    ->fields(NULL, array('state_code', 'is_use', 'created', 'updated'))
    ->condition('lending_program_id', $lending_program->id)
    ->execute()
    ->fetchAllAssoc('state_code');

	$index = array(
		'libor' => t('LIBOR'),
		'wsj' => t('WSJ Prime Rate'),
	);

	$lender = p1_helper_get_lenders();
	$program_type = p1_helper_get_program_types();
	$property_type = p1_helper_get_property_types();

	$header_top = array(
		'name' => array('data' => t('Name')),
		'active' => array('data' => t('Active')),
		'lender' => array('data' => t('Lender')),
		'program' => array('data' => t('Program')),
		'property' => array('data' => t('Property')),
	);

	$rows_top = array(array(
		'name' => $lending_program->name,
		'active' => ($lending_program->active == 0) ? 'No' : 'Yes',
		'lender' => $lender[$lending_program->lender_id],
		'program' => $program_type[$lending_program->program_type_id],
		'property' => $property_type[$lending_program->property_type_id],
	));

	$header_middle = array(
		'minimum_loan' => array('data' => t('Minimum loan')),
		'maximum_loan' => array('data' => t('Maximum loan')),
		'index' => array('data' => t('Index')),
		'margin' => array('data' => t('Margin')),
	);

	$rows_middle = array(array(
		'minimum_loan' => '$' . number_format($lending_program->min_loan, 2, '.', ','),
		'maximum_loan' => '$' . number_format($lending_program->max_loan, 2, '.', ','),
		'index' => $index[$lending_program->lp_index],
		'margin' => $lending_program->margin,
	));

	$header_middle_1 = array(
		'is_recourse' => array('data' => t('Is Recourse')),
		'underwriting_fees' => array('data' => t('Underwriting fees')),
		'processing_fee' => array('data' => t('Processing fee')),
		'application_deposit' => array('data' => t('Application deposit')),
		'doc_type' => array('data' => t('Doc type')),
	);

	$rows_middle_1 = array(array(
		'is_recourse' => ($lending_program->is_recourse == 1) ? 'On' : 'Off',
		'underwriting_fees' => '$' . number_format($lending_program->underwriting_fees, 2, '.', ','),
		'processing_fee' => '$' . number_format($lending_program->processing_fee, 2, '.', ','),
		'application_deposit' => '$' . number_format($lending_program->application_deposit, 2, '.', ','),
		'doc_type' => $lending_program->doc_type,
	));

	$header_middle_2 = array(
		'min_dscr' => array('data' => t('Min DSCR')),
		'min_vacancy_ratio' => array('data' => t('Min vacancy ratio')),
		'max_life_adjustment' => array('data' => t('Max life adjustment')),
		'max_first_adjustment' => array('data' => t('Max first adjustment')),
		'max_subsequent_adjustment' => array('data' => t('Max subsequent adjustment')),
	);

	$rows_middle_2 = array(array(
		'min_dscr' => $lending_program->min_dscr,
		'min_vacancy_ratio' => $lending_program->min_vacancy_ratio,
		'max_life_adjustment' => $lending_program->max_life_adjustment,
		'max_first_adjustment' => $lending_program->max_first_adjustment,
		'max_subsequent_adjustment' => $lending_program->max_subsequent_adjustment,
	));

	$header_middle_3 = array(
		'escrow_required' => array('data' => t('Escrow required')),
		'accept_bankruptcy' => array('data' => t('Accept bankruptcy')),
		'accept_foreclosure' => array('data' => t('Accept foreclosure')),
		'msa' => array('data' => t('MSA')),
	);

	$rows_middle_3 = array(array(
		'escrow_required' => $lending_program->escrow_required,
		'accept_bankruptcy' => $lending_program->accept_bankruptcy,
		'accept_foreclosure' => $lending_program->accept_foreclosure,
		'msa' => $lending_program->msa,
	));

	$header_bottom = array(
		'states' => array('data' => t('Offered in the following states'), 'colspan' => 4),
	);

	$states = p1_helper_get_states();
	$rows_bottom = array();
	$column = 1;
	$states_row = array();
	foreach ($states as $code => $state) {
		if ($data[$code]->is_use) {
			$state = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span> <strong>' . $state . '</strong>';
		}

		if ($column <= 4) {
			$states_row[] = array('data' => $state);
			$column++;
		} else {
			$rows_bottom[] = $states_row;
			$states_row = array();
			$states_row[] = array('data' => $state);
			$column = 2;
		}

	}

	$matrices = p1_helper_get_pricing_matrices_by_lending_program($lending_program->id);
	$fico = array();
	$ltv = array();

	foreach ($matrices as $matrix) {
		$fico[$matrix->fico] = $matrix->fico;
		$ltv[$matrix->ltv] = $matrix->ltv;
	}

	sort($ltv);
	rsort($fico);
	$flipped_ltv = array_flip($ltv);
	$flipped_fico = array_flip($fico);

	$header_matrix = array('fico\ltv' => array('data' => t('FICO \ LTV'), 'align' => 'center'));
	foreach ($ltv as $value) {
		$header_matrix[$value] = array('data' => number_format($value, 2, '.', ',') . '%', 'align' => 'center', 'class' => 'text-center');
	}

	$rows_matrix = array();

	foreach ($fico as $value) {
		$rows_matrix[] = array(array('data' => $value, ));
	}

	foreach ($matrices as $key => $value) {
		$rows_matrix[$flipped_fico[$value->fico]][$flipped_ltv[$value->ltv]+1] = array('data' => $value->interest . '%', 'align' => 'center');
	}

	$header_features = array(
		'name' => array('data' => t('Feature Name')),
		'adjusted' => array('data' => t('Adjusted by'), 'align' => 'center','class' => 'text-center'),
	);

	$features = p1_helper_get_features_by_lending_program($lending_program->id);

	$temp_features = array();
	$temp_features_id = array();
	foreach ($features as $feature) {
		$temp_features[] = $feature->id;
		$temp_features_id[] = $feature->type;
	}

	$features_types = p1_helper_get_features_type(array(0,1));
	array_multisort($temp_features_id, $temp_features);
	$rows_features = array();
	foreach ($temp_features as $value) {

		$prefix = '';
		$suffix = '';
		$adjusted_by = $features[$value]->adjusted_by;

		if ($features[$value]->operation == 'increase') {
			$prefix = '+';
		} elseif ($features[$value]->operation == 'decrease') {
			$prefix = '-';
		}

		if ($features[$value]->unit == 'interest') {
			$suffix = '%';
		} elseif ($features[$value]->unit == 'point') {
			$suffix = ' point LTV';
		}

		if ($adjusted_by == '0.00') {
			$adjusted_by = 'N/A';
		} else {
			$adjusted_by = $prefix . (number_format($adjusted_by, 3, '.', ',') + 0) . $suffix;
		}


		$rows_features[] = array('name' => $features_types[$features[$value]->type], array('data' => $adjusted_by, 'align' => 'center'));
	}


	$output = '';
	$output .= theme('table', array('header' => $header_top, 'rows' => $rows_top));
	$output .= theme('table', array('header' => $header_middle, 'rows' => $rows_middle));
	$output .= theme('table', array('header' => $header_middle_1, 'rows' => $rows_middle_1));
	$output .= theme('table', array('header' => $header_middle_2, 'rows' => $rows_middle_2));
	$output .= theme('table', array('header' => $header_middle_3, 'rows' => $rows_middle_3));
	$output .= theme('table', array('header' => $header_bottom, 'rows' => $rows_bottom));
	$output .= theme('table', array('header' => $header_matrix, 'rows' => $rows_matrix));
	$output .= theme('table', array('header' => $header_features, 'rows' => $rows_features));

	return $output;
}
