<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_msa entity
 */

/**
 * Implements hook_form().
 */
function p1_msa_form($form, &$form_state, $msa = NULL) {
	$form = array();

	$form['zipcode'] = array(
		'#markup' => t('Postal code: ') . $msa->zipcode,
		'#prefix' => '<div>',
		'#suffix' => '</div>',
	);

	$form['state'] = array(
		'#markup' => t('State: ') . $msa->state_name,
		'#prefix' => '<div>',
		'#suffix' => '</div>',
	);

	$form['city'] = array(
		'#markup' => t('City: ') . $msa->city,
		'#prefix' => '<div>',
		'#suffix' => '</div>',
	);

	$form['position'] = array(
		'#title' => t('position'),
		'#type' => 'textfield',
		'#size' => 5,
		'#maxlength' => 5,
		'#default_value' => $msa->position,
		'#element_validate' => array('element_validate_integer'),
	);

	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
			'#type' => 'submit',
			'#value' => t('Update MSA'),
		));
	return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_msa_form_validate($form, &$form_state) {

}

/**
 * Implements hook_form_submit().
 */
function p1_msa_form_submit($form, &$form_state) {
	$msa = entity_ui_form_submit_build_entity($form, $form_state);
	$entities = p1_msa_load_multiple(FALSE, array('state' => $msa->state, 'city' => $msa->city), FALSE);
	foreach ($entities as $entity) {
		$entity->position = $msa->position;
		$entity->updated = $msa->updated;
		$entity->save();
		drupal_set_message(t('@zipcode MSA has been saved.', array('@zipcode' => $entity->zipcode)));
	}
	$form_state['redirect'] = P1_MSA_VIEW_URI;
}
