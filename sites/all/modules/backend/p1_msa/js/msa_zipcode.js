(function ($) {
  Drupal.behaviors.zipcode = {
    attach : function (context, settings) {
      $('.msa-zipcode').blur(function () {
        var zip = $('.msa-zipcode').val();
        var url = '/admin/backend/msa/'+zip;
        $.ajax({
          type: "GET",
          url: url,
          data: {},
          success: function(data){
            $('.msa-state').select2('val', data[0]);
            $('.msa-city').val(data[1]);
          }
        });
      });
    }
  };
})(jQuery);