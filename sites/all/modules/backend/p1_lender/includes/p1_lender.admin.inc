<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_lender entity
 */

/**
 * Implements hook_form().
 */
function p1_lender_form($form, &$form_state, $lender = NULL) {
	$form = array();

	// Get only the active lender types
	$types = p1_helper_get_lender_type('active');

	// Get the US states
	$states = p1_helper_get_states();

	$form['#attached']['js'] = array(
		drupal_get_path('module', 'p1_helper') . '/js/msa_zipcode.js' => array(
			'type' => 'file',
		),
	);

	$form['name'] = array(
		'#title' => t('Name'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->name) ? $lender->name : '',
		'#required' => TRUE,
		'#maxlength' => 255,
	);

	$form['active'] = array(
		'#title' => t('Active'),
		'#type' => 'select',
		'#options' => array(
			0 => 'No',
			1 => 'Yes',
		),
		'#default_value' => isset($lender->active) ? $lender->active : 0,
		'#required' => TRUE,
	);

	$form['type'] = array(
		'#title' => t('Type'),
		'#type' => 'select',
		'#options' => $types,
		'#empty_option' => '- Chose Lender Type -',
		'#default_value' => isset($lender->type) ? $lender->type : NULL,
		'#required' => TRUE,
	);

	$form['zipcode'] = array(
		'#title' => t('Zip code'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->zipcode) ? $lender->zipcode : NULL,
		'#maxlength'=> 10,
		'#attributes' => array('class' => array('msa-zipcode')),
	);

	$form['state'] = array(
		'#title' => t('State'),
		'#type' => 'select',
		'#options' => $states,
		'#default_value' => isset($lender->state) ? $lender->state : NULL,
		'#required' => TRUE,
		'#attributes' => array('class' => array('msa-state')),
	);

	$form['city'] = array(
		'#title' => t('City'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->city) ? $lender->city : NULL,
		'#attributes' => array('class' => array('msa-city')),
	);

	$form['address'] = array(
		'#title' => t('Address'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->address) ? $lender->address : NULL,
	);

	$form['suite'] = array(
		'#title' => t('Suite'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->suite) ? $lender->suite : NULL,
	);

	$form['contact_person'] = array(
		'#title' => t('Contact person'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->contact_person) ? $lender->contact_person : NULL,
	);

	$form['phone'] = array(
		'#title' => t('Phone'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->phone) ? $lender->phone : NULL,
		'#element_validate' => array('element_validate_integer_positive'),
		'#maxlength'=> 10,
	);

	$form['fax'] = array(
		'#title' => t('Fax'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->fax) ? $lender->fax : NULL,
		'#element_validate' => array('element_validate_integer_positive'),
		'#maxlength'=> 10,
	);
	$form['email'] = array(
		'#title' => t('E-Mail'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->email) ? $lender->email : NULL,
		'#element_validate' => array('p1_lender_email_element_validate'),
	);

	$form['website'] = array(
		'#title' => t('Website'),
		'#type' => 'textfield',
		'#default_value' => isset($lender->website) ? $lender->website : NULL,
		'#element_validate' => array('p1_lender_url_element_validate'),
	);

	$form['comments'] = array(
		'#title' => t('Comments'),
		'#type' => 'textarea',
		'#default_value' => isset($lender->comments) ? $lender->comments : NULL,
	);


	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
			'#type' => 'submit',
			'#value' => isset($lender->id) ? t('Update Lender') : t('Save Lender'),
		),
		'delete_link' => array(
			'#markup' => isset($lender->id) ? l(t('Delete'), P1_LENDER_URI . $lender->id . '/delete', array('attributes' => array('id' => array('p1_lender-delete-' . $lender->id), 'class' => array('button remove')))) : ''));
	return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_lender_form_validate($form, &$form_state) {
	$element = $form['name'];
	$entity = entity_load('p1_lender', FALSE, array('name' => $element['#value']), FALSE);
	if (count($entity)) {
		if ($form_state['op'] == 'add' || $form_state['p1_lender']->id != array_shift($entity)->id) {
			form_error($element, t('Lender with inputted name is exist. Please choose another name.'));
		}
	}
}

/**
 * Implements hook_form_submit().
 */
function p1_lender_form_submit($form, &$form_state) {
	$lender = entity_ui_form_submit_build_entity($form, $form_state);
	$lender->save();
	drupal_set_message(t('@name Lender has been saved.', array('@name' => $lender->name)));
	$form_state['redirect'] = P1_LENDER_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_lender.
 */
function p1_lender_bulk_delete($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lenders = p1_lender_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lenders as $lender) {
		$variables['items'][] = $lender->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Delete all Lenders?'), P1_LENDER_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_lender.
 */
function p1_lender_bulk_activate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lenders = p1_lender_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lenders as $lender) {
		$variables['items'][] = $lender->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Activate all Lenders?'), P1_LENDER_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_lender.
 */
function p1_lender_bulk_deactivate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$lenders = p1_lender_load_multiple($ids);

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($lenders as $lender) {
		$variables['items'][] = $lender->name;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);

	return confirm_form($form, t('Deactivate all Lenders?'), P1_LENDER_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_lender_bulk_delete_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lender_delete_multiple($ids);

	drupal_set_message(t('Lenders deleted'));
	drupal_goto(P1_LENDER_URI);
}

/**
 * Implements hook_submit().
 */
function p1_lender_bulk_activate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lender_activate_multiple($ids);

	drupal_set_message(t('Lender Types actived'));
	drupal_goto(P1_LENDER_URI);
}

/**
 * Implements hook_submit().
 */
function p1_lender_bulk_deactivate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_lender_deactivate_multiple($ids);

	drupal_set_message(t('Lender Types actived'));
	drupal_goto(P1_LENDER_URI);
}

/**
 * E-mail validation
 */
function p1_lender_email_element_validate($element, &$form_state) {
	if ($element['#value'] != '' && !valid_email_address($element['#value'])) {
		form_error($element, t('Please enter a valid email address.'));
	}
}

/**
 * Website validation
 */
function p1_lender_url_element_validate($element, &$form_state) {
	if ($element['#value'] != '' && !valid_url($element['#value'], TRUE)) {
		form_error($element, t('Invalid Website URL'));
	}

}
