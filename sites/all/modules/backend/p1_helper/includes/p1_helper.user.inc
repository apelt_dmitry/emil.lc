<?php

function p1_helper_user_login(&$edit, $account) {
  if (!isset($_POST['form_id']) || $_POST['form_id'] != 'user_pass_reset') {
      $_GET['destination'] = '/';
  }
}


// Get all user types
function p1_helper_get_all_user_types()
{
  $user_types_list = array(
    0 => 'Mortgage Broker',
    1 => 'Realtor',
    2 => 'Other'
  );

  return $user_types_list;
}

// Get user type by uid
function p1_helper_get_user_type($uid = NULL) {
  $wrapper = entity_metadata_wrapper('p1_user_registration', $uid);
  $type_num = $wrapper->type->value();

  $user_type = p1_helper_get_all_user_types();
  $type_str = $user_type[$type_num];

  return $type_str;
}

// Get all access types / single by id
function p1_helper_get_access_types($id = NULL) {
  if ($id != NULL) {
    $id = $id;
    $wrapper = entity_metadata_wrapper('p1_access_role', $id);
    $type_num = $wrapper->name->value();
    return $type_num;
  }
  $all_access_roles = array();
  $access_role = entity_load('p1_access_role', FALSE, array(), TRUE);
  foreach ((array) $access_role as $key => $val) {
    $all_access_roles[$key] = $val->name;
  }
  return $all_access_roles;
}

function p1_helper_get_access_role_status($name_field = NULL, $var = NULL) {
  if ($var) {
    switch ($name_field) {
      case 'deal':
        return 'View';
        break;
      case 'document_view_name':
        return 'View';
        break;
      case 'document_upload':
        return 'Upload';
        break;
      case 'document_download':
        return 'Download';
        break;
      case 'client_view':
        return 'View';
        break;
      case 'client_change':
        return 'Change';
        break;
      case 'client_sign':
        return 'Sign';
        break;
      case 'follow_up_view_messages':
        return 'View messages';
        break;
      case 'follow_up_reply':
        return 'Reply';
        break;
    }
  } else {
    return 'N/A';
  }
}

function p1_helper_get_permission_by_role ($id) {
  $output = array();
  $permissions = p1_access_role_permissions_load_multiple(FALSE, array('role_id' => $id));
  foreach ($permissions as $permission) {
    $output[] = $permission->permission;
  }
  return $output;
}

function p1_helper_role_access($deal) {
  if (!$deal) {
    return array();
  }
  if (is_string($deal)) {
    $deal = p1_deal_load($deal);
  }
  global $user;
  if($user->uid == 0){
    return array();
  }
    $wrapper = entity_load('p1_access', FALSE, array(
      'email' => $user->mail,
      'id_deal' => $deal->id
    ));

    if ($wrapper) {
      $role = array_shift($wrapper);

      return p1_helper_get_permission_by_role($role->role);
    }
  return array();
}
