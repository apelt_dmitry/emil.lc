<?php

function p1_helper_form_alter(&$form, $form_state, $form_id) {

  if ($form_id == 'user_login') {

    $form['name']['#title'] = t('Enter your E-mail Address');
    unset($form['name']['#description']);

    $form['pass']['#title'] = t('Enter your Password');
    unset($form['pass']['#description']);

    unset($form['actions']['submit']['#attributes']['class']);
    $form['actions']['submit']['#attributes']['class'] = array('btn btn-success form-submit');
    $form['op']['#attributes']['class'] = array('btn btn-success form-submit');

    $form['new_password'] = array(
      '#markup' =>  l(t('Request new password'), '/user/password'),
    );
    $form['register'] = array(
      '#markup' =>  l(t('Need to register?'), '/user/register'),
    );
  }

  if ($form_id == 'user_pass') {
    $form['name']['#title'] = t('Enter your E-mail Address');

    unset($form['actions']['submit']['#attributes']['class']);
    $form['actions']['submit']['#attributes']['class'] = array('btn btn-success form-submit');

    $form['login'] = array(
      '#markup' =>  l(t('Need to login?'), '/user/login'),
    );
  }

  if ($form_id == 'user_register_form') {
    $form['name']['#title'] = t('Enter your E-mail Address');
    unset($form['account']['mail']['#description']);

    unset($form['first_name']['#description']);
    unset($form['last_name']['#description']);
    unset($form['company_name']['#description']);
    unset($form['telefone']['#description']);
    unset($form['fax']['#description']);
    unset($form['type']['#description']);

    $form['login'] = array(
      '#markup' =>  l(t('Need to login?'), '/user/login'),
    );
  }

  if ($form_id == 'user_profile_form') {
    $form['account']['name']['#type'] = 'hidden';
    $form['account']['status']['#type'] = 'select';
    $form['timezone']['#type'] = 'markup';
    $form['actions']['submit']['#value'] = t('Update Profile');

    unset($form['timezone']['timezone']['#description']);
    unset($form['account']['mail']['#description']);
    unset($form['account']['current_pass']['#description']);
    unset($form['account']['pass']['#description']);

    unset($form['first_name']['#description']);
    unset($form['last_name']['#description']);
    unset($form['company_name']['#description']);
    unset($form['telefone']['#description']);
    unset($form['fax']['#description']);
    unset($form['type']['#description']);
    unset($form['photo']['#description']);

  }

}


