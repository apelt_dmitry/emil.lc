<?php

/**
 * Implements hook_libraries_info().
 */
function p1_helper_libraries_info() {
  $libraries['form-validation'] = array(
    'name' => 'Form Validator',
    'vendor url' => 'https://www.google.com',
    'version' => '0.71',
    'files' => array(
      'css' => array(
        'css/formValidation.min.css',
      ),
      'js' => array(
        'js/formValidation.min.js',
        'js/framework/bootstrap.min.js',
      ),clearstatcache()
    ),
  );

  $libraries['jquery.mask'] = array(
    'name' => 'jQuery Mask',
    'vendor url' => 'https://www.google.com',
    'version' => '1.14.0',
    'files' => array(
      'js' => array(
        'jquery.mask.min.js',
      ),clearstatcache()
    ),
  );

  $libraries['custom.common'] = array(
    'name' => 'Custom common',
    'vendor url' => 'https://www.google.com',
    'version' => '1.0.0',
    'files' => array(
      'css' => array(
        'css/custom.common.css',
      ),
      'js' => array(
        'js/custom.common.js' => array(
          'scope' => 'footer',
          'weight' => 100,
          )
      ),clearstatcache()
    ),
  );

  return $libraries;
}

/**
 * Implements hook_preprocess_html().
 *
 * Purposefully only load on page requests and not hook_init(). This is
 * required so it does not increase the bootstrap time of Drupal when it isn't
 * necessary.
 */
function p1_helper_preprocess_html() {

  $library = libraries_load('form-validation');

  if (!$library['loaded']) {
    drupal_set_message($library['error message'] . t('Please make sure that '
            . 'form-validation was download & extracted at sites/all/libraries/form-validation directory.'),
            'warning');
  }

  $library = libraries_load('jquery.mask');

  if (!$library['loaded']) {
    drupal_set_message($library['error message'] . t('Please make sure that '
            . 'jquery.mask was download & extracted at sites/all/libraries/jquery.mask directory.'),
            'warning');
  }



  $library = libraries_load('custom.common');

  if (!$library['loaded']) {
    drupal_set_message($library['error message'] . t('Please make sure that '
            . 'custom.common was download & extracted at sites/all/libraries/custom.common directory.'),
            'warning');
  }



}
