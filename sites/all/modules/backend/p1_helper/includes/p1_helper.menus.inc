<?php

/**
 * Implementation of hook_menu_alter().
 * Remember to clear the menu cache after adding/editing this function.
 */
function p1_helper_menu_alter(&$items) {

  $items['user/register']['type'] = MENU_CALLBACK;
  $items['user']['type'] = MENU_CALLBACK;
  $items['user/password']['type'] = MENU_CALLBACK;

}
