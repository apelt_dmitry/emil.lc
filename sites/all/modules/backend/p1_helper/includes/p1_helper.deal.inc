<?php

/**
 *  Get list of deal occupancy
 */
function p1_helper_get_occupancy(){
  $result = array(
    0 => 'No-Owner occupied',
    1 => 'Owner occupied',
    2 => 'Investment',
  );

  return $result;
}

/**
 *  Get list of deal phases
 */
function p1_helper_get_phase(){
  $result = array(
    0 => 'Initial',
    1 => 'Phase 1',
    2 => 'Phase 2',
    3 => 'Phase 3',
    4 => 'Phase 4',
    5 => 'Locked'
  );

  return $result;
}

/**
 *  Get list of deal status sources
 */
function p1_helper_get_deal_status_types() {
  return array(
    0 => 'Deal create',
    1 => 'Deal update',
    2 => 'Term sheet',
    3 => 'Document upload',
    4 => 'Phase change',
    5 => 'Communication',
    6 => 'Document review',
    7 => 'Deal access',
  );
}

/**
 *  Get list of type review for deal files (conditions)
 */
function p1_helper_get_deal_file_statuses() {
  return array(
    1 => 'Accepted',
    0 => 'Rejected',
  );
}

/**
 *  Get list of access types for deal status
 */
function p1_helper_get_deal_access_types() {
  return array(
    0 => 'Public',
    1 => 'Private',
  );
}

/**
 *  Get deal status passing the deal ID
 */
function p1_helper_get_deal_status_by_id($id) {
  $deal_status = p1_deal_status_load($id);
  return $deal_status->type;
}


/**
 *  Get list of features for drop downs
 */
function p1_helper_feature_getter($type, $lending_program_id = '') {

  $query = db_select('p1_lending_program', 'lp');
  $query->leftJoin('p1_features', 'f', 'f.lending_program_id = lp.id');
  $query->leftJoin('p1_features_type', 'ft', 'ft.id = f.type');
  $query->condition('ft.type', $type);
  $query->condition('f.active', 1);
  $query->condition('ft.active', 1);
  $query->condition('lp.active', 1);

  if ($lending_program_id != '') {
    $query->condition('lp.id', intval($lending_program_id));
  }

  $query->fields('f');
  $query->fields('ft', array('name'));

  return $query->execute()->fetchAll();

}

/**
 *  Get feature name from ID
 */
function p1_helper_feature_get_name($type, $id) {

  $query = db_select('p1_lending_program', 'lp');
  $query->leftJoin('p1_features', 'f', 'f.lending_program_id = lp.id');
  $query->leftJoin('p1_features_type', 'ft', 'ft.id = f.type');
  $query->condition('ft.type', $type);
  $query->condition('f.id', $id);
  $query->condition('f.active', 1);
  $query->condition('ft.active', 1);
  $query->condition('lp.active', 1);
  $query->fields('ft', array('name'));

  return $query->execute()->fetchField();

}

/**
 *  Get deal available programs (price matrix)
 */
function p1_helper_get_price_matrix($lending_program, $deal) {
  $min_fico = 0;
  $max_fico = 0;
  $min_ltv = 0;
  $max_ltv = 0;

  $query = db_select('p1_pricing_matrix', 'pm');
  $query->condition('pm.lending_program_id', $lending_program->id);
  $query->condition('pm.active', 1);
  $query->condition('pm.fico', $deal->fico_score, '>=');
  $query->condition('pm.ltv', $deal->loan_to_value, '>=');
  $query->orderBy('pm.fico', 'ASC');

  $result = $query->execute();

  $count = $result->rowCount();

  if ($result->rowCount() > 0) {

    $data = $result->fetchAll();

    foreach ($data as $pricing_matrix) {

      $min_fico = min($min_fico, $pricing_matrix->fico);
      $max_fico = max($max_fico, $pricing_matrix->fico);
      $min_ltv = max($min_ltv, $pricing_matrix->ltv);
      $max_ltv = max($max_ltv, $pricing_matrix->ltv);

      // $deal->fico_score <= $pricing_matrix->fico
      // $deal->loan_to_value

    }

  }

}


/**
 *  Get deal available programs
 */
function p1_helper_available_programs($deal) {
  $error = FALSE;
  $output = array();
  $rows = array();

  // Match
  // Program type
  // Property type
  // Min / Max Loan
  // Doc type

  $query = db_select('p1_lending_program', 'lp');
  $query->condition('lp.program_type_id', $deal->program_type_id);
  $query->condition('lp.property_type_id', $deal->property_type_id);
  $query->condition('lp.doc_type', $deal->doc_id);
  $query->condition('lp.min_loan', $deal->purchase_price, '<=');
  $query->condition('lp.max_loan', $deal->purchase_price, '>=');
  $query->condition('lp.min_vacancy_ratio', $deal->vacancy_factor, '>=');
  $query->condition('lp.active', 1);
  $query->fields('lp');

  $result = $query->execute();

  $count = $result->rowCount();
  $data = $result->fetchAll();

  if ($result->rowCount() < 1) {
    $output['error'] = 'There are currently no lending programs matching your criteria. Review the values entered in Program and Property types as well as the Min and Max loan amounts and the doc type.';
  }

  foreach ($data as $lending_program) {
    //dsm($lending_program);

    // Check bankruptcy date (ignored for now)
    // $deal->bankruptcy_date

    // Check foreclosure date (ignored for now)
    // $deal->foreclosure_date

    // Check MSA (ignored for now)
    // $deal->zipcode




    $rows[] = array(
      'rate' => '',
      'max_ltv' => '',
      'min_fico' => '',
      'p_with_i' => '',
      'cost' => p1_helper_currency($lending_program->lender_fee + $lending_program->underwriting_fees + $lending_program->processing_fee + $lending_program->appraisal_fee),
    );
  }

  //dsm($rows);

}

function p1_helper_available_program_types($changes = NULL) {
  $output = [];
  $query = db_select('p1_lending_program', 'lp');
  $query->leftJoin('p1_program_type', 'pt', 'pt.id = lp.program_type_id');
  $query->condition('pt.active', 1);
  $query->condition('lp.active', 1);

  if (isset($changes['property_type_id']) && $changes['property_type_id'] != '') {
    $query->condition('lp.property_type_id', intval($changes['property_type_id']));
  }

  if (isset($changes['loan_amount']) && $changes['loan_amount'] != '') {
    $query->condition('lp.min_loan', intval($changes['loan_amount']), '<=');
    $query->condition('lp.max_loan', intval($changes['loan_amount']), '>=');
  }

  $query->fields('pt', ['id', 'name']);
  $results = $query->execute()
    ->fetchall();
  foreach ($results as $entity) {
    $output[$entity->id] = $entity->name;
  }
  return $output;
}

function p1_helper_available_years($type, $changes = NULL) {
  $output = [];
  $query = db_select('p1_lending_program', 'lp');
  $query->leftJoin('p1_features', 'f', 'f.lending_program_id = lp.id');
  $query->leftJoin('p1_features_type', 'ft', 'ft.id = f.type');
  $query->condition('ft.type', $type);
  $query->condition('f.active', 1);
  $query->condition('ft.active', 1);
  $query->condition('lp.active', 1);

  if (isset($changes['property_type_id']) && $changes['property_type_id'] != '') {
    $query->condition('lp.property_type_id', intval($changes['property_type_id']));
  }

  if (isset($changes['loan_amount']) && $changes['loan_amount'] != '') {
    $query->condition('lp.min_loan', intval($changes['loan_amount']), '<=');
    $query->condition('lp.max_loan', intval($changes['loan_amount']), '>=');
  }

  $query->fields('ft', ['id', 'name']);
  $results = $query->execute()
    ->fetchall();
  foreach ($results as $entity) {
    $output[$entity->id] = $entity->name;
  }
  return $output;
}


function p1_helper_available_prepayments($property_type = NULL) {
  $output = [];
  $query = db_select('p1_lending_program', 'lp');
  $query->leftJoin('p1_features', 'f', 'f.lending_program_id = lp.id');
  $query->leftJoin('p1_features_type', 'ft', 'ft.id = f.type');
  $query->condition('ft.type', 'Pre-Payment');
  $query->condition('f.active', 1);
  $query->condition('ft.active', 1);
  $query->condition('lp.active', 1);

  if ($property_type != '') {
    $query->condition('lp.property_type_id', intval($property_type));
  }

  $query->fields('ft', ['id', 'name']);
  $results = $query->execute()
    ->fetchall();
  foreach ($results as $entity) {
    $output[$entity->id] = $entity->name;
  }
  return $output;
}

function p1_helper_get_deal_years($deal, $lending_program_id) {
  $query = db_select('p1_features', 'f');
  $query->leftJoin('p1_lending_program', 'lp', 'lp.id = f.lending_program_id');
  $query->leftJoin('p1_features_type', 'ft', 'ft.id = f.type');
  $query->condition('ft.id', $deal->amortization);
  $query->condition('lp.id', $lending_program_id);
  $query->condition('ft.type', 'Amortization');
  $query->fields('f', ['years']);
  $amortization = $query->execute()
    ->fetchField();
  $years = $amortization;
  return $years;
}

function p1_helper_get_deal_prepayment($deal, $lending_program_id) {
  $query = db_select('p1_features', 'f');
  $query->leftJoin('p1_lending_program', 'lp', 'lp.id = f.lending_program_id');
  $query->leftJoin('p1_features_type', 'ft', 'ft.id = f.type');
  $query->condition('ft.id', $deal->pre_payment_penalty);
  $query->condition('lp.id', $lending_program_id);
  $query->condition('ft.type', 'Pre-Payment');
  $query->fields('f', ['operation', 'adjusted_by']);
  $result = $query->execute()
    ->fetchAll();
  if (!$result) {
    return 0;
  }
  else {
    $result = array_pop($result);
    if ($result->operation == 'increase') {
      return $result->adjusted_by;
    }
    else {
      return -$result->adjusted_by;
    }
  }
}
