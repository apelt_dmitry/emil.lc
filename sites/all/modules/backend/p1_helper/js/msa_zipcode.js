(function ($) {
  Drupal.behaviors.zipcode = {
    attach : function (context, settings) {
      $('.msa-zipcode').blur(function () {
        var zip = $('.msa-zipcode').val();
        var url = '/admin/backend/msa/'+zip;
        $.ajax({
          type: "GET",
          url: url,
          data: {},
          success: function(data){
            $('.msa-state').val(data[0]);
            $('.msa-city').val(data[1]);
          }
        });
      });
      $('.msa-zipcode-2').blur(function () {
        var zip = $('.msa-zipcode-2').val();
        var url = '/admin/backend/msa/'+zip;
        $.ajax({
          type: "GET",
          url: url,
          data: {},
          success: function(data){
            $('.msa-state-2').val(data[0]);
            $('.msa-city-2').val(data[1]);
          }
        });
      });
    }
  };
})(jQuery);
