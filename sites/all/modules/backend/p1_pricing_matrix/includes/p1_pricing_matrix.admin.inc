<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_pricing_matrix entity
 */

/**
 * Implements hook_form().
 */
function p1_pricing_matrix_form($form, &$form_state, $pricing_matrix = NULL) {
	$form = array();

	$pricing_matrices = p1_helper_get_lending_programs();

	$form['lending_program_id'] = array(
		'#title' => t('lending_program_id'),
		'#type' => 'select',
		'#options' => $pricing_matrices,
		'#default_value' => isset($pricing_matrix->lending_program_id) ? $pricing_matrix->lending_program_id : '',
		'#required' => TRUE,
		'#maxlength' => 255,
	);

	$form['fico'] = array(
		'#title' => t('FICO'),
		'#type' => 'textfield',
		'#required' => TRUE,
		'#maxlength' => 10,
		'#size' => 10,
		'#default_value' => isset($pricing_matrix->fico) ? $pricing_matrix->fico : '0',
	);

	$form['ltv'] = array(
		'#title' => t('LTV'),
		'#type' => 'textfield',
		'#required' => TRUE,
		'#maxlength' => 10,
		'#size' => 10,
		'#default_value' => isset($pricing_matrix->ltv) ? $pricing_matrix->ltv : '0.0',
	);

	$form['interest'] = array(
		'#title' => t('Interest'),
		'#type' => 'textfield',
		'#required' => TRUE,
		'#maxlength' => 10,
		'#size' => 10,
		'#default_value' => isset($pricing_matrix->interest) ? $pricing_matrix->interest : '0.000',
	);

	$form['active'] = array(
		'#title' => t('Active'),
		'#type' => 'select',
		'#options' => array(
			0 => 'No',
			1 => 'Yes',
		),
		'#default_value' => isset($pricing_matrix->active) ? $pricing_matrix->active : 1,
	);

	$form['actions'] = array(
		'#type' => 'actions',
		'submit' => array(
			'#type' => 'submit',
			'#value' => isset($pricing_matrix->id) ? t('Update Pricing Matrix') : t('Save Pricing Matrix'),
		),
		'delete_link' => array(
			'#markup' => isset($pricing_matrix->id) ? l(t('Delete'), P1_PRICING_MATRIX_MANAGE_URI . $pricing_matrix->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''));
	return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_pricing_matrix_form_validate($form, &$form_state) {
	$element = $form['lending_program_id'];
	$lending_program_id = $form['lending_program_id'];
	$fico = $form['fico'];
	$ltv = $form['ltv'];
	$entity = entity_load('p1_pricing_matrix', FALSE, array(
		'lending_program_id' => $lending_program_id['#value'],
		'fico' => $fico['#value'],
		'ltv' => $ltv['#value'],
	), FALSE);
	if (count($entity)) {
		if ($form_state['op'] == 'add' || $form_state['p1_pricing_matrix']->id != array_shift($entity)->id) {
			form_error($element, t('Pricing matrix with inputted FICO and LTV is exist for chosen lending program.'));
		}
	}
}

/**
 * Implements hook_form_submit().
 */
function p1_pricing_matrix_form_submit($form, &$form_state) {
	$pricing_matrix = entity_ui_form_submit_build_entity($form, $form_state);
	$pricing_matrix->save();
	drupal_set_message(t('Pricing Matrix has been saved.'));
	$form_state['redirect'] = P1_PRICING_MATRIX_VIEW_URI;
}

/**
 * Confirmation before bulk deleting p1_pricing_matrix.
 */
function p1_pricing_matrix_bulk_delete($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$pricing_matrices = p1_pricing_matrix_load_multiple($ids);

	$lending_programs = p1_helper_get_lending_programs();

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($pricing_matrices as $pricing_matrix) {
		$variables['items'][] = $lending_programs[$pricing_matrix->lending_program_id] . ', FICO: ' . $pricing_matrix->fico . ', LTV: ' . $pricing_matrix->ltv;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);
	return confirm_form($form, t('Delete all Pricing Matrices?'), P1_PRICING_MATRIX_URI, '', t('Delete all'), t('Cancel'));
}

/**
 * Confirmation before bulk activating p1_pricing_matrix.
 */
function p1_pricing_matrix_bulk_activate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$pricing_matrices = p1_pricing_matrix_load_multiple($ids);

	$lending_programs = p1_helper_get_lending_programs();

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($pricing_matrices as $pricing_matrix) {
		$variables['items'][] = $lending_programs[$pricing_matrix->lending_program_id] . ', FICO: ' . $pricing_matrix->fico . ', LTV: ' . $pricing_matrix->ltv;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);
	
	return confirm_form($form, t('Activate Pricing Matrices?'), P1_PRICING_MATRIX_URI, '', t('Activate all'), t('Cancel'));
}

/**
 * Confirmation before bulk deactivating p1_pricing_matrix.
 */
function p1_pricing_matrix_bulk_deactivate($form, &$form_state, $ids) {
	$ids = explode('|', $ids);
	$pricing_matrices = p1_pricing_matrix_load_multiple($ids);

	$lending_programs = p1_helper_get_lending_programs();

	$form = array();
	$form_state['ids'] = $ids;

	$variables = array(
		'type' => 'ul',
		'items' => array(),
		'title' => '',
		'attributes' => array(),
	);

	foreach ($pricing_matrices as $pricing_matrix) {
		$variables['items'][] = $lending_programs[$pricing_matrix->lending_program_id] . ', FICO: ' . $pricing_matrix->fico . ', LTV: ' . $pricing_matrix->ltv;
	}

	$form['summary'] = array(
		'#markup' => theme_item_list($variables),
	);
	return confirm_form($form, t('Deactivate Pricing Matrices?'), P1_PRICING_MATRIX_URI, '', t('Dectivate all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_pricing_matrix_bulk_delete_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_pricing_matrix_delete_multiple($ids);

	drupal_set_message(t('Pricing Matrices deleted'));
	drupal_goto(P1_PRICING_MATRIX_URI);
}

/**
 * Implements hook_submit().
 */
function p1_pricing_matrix_bulk_activate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_pricing_matrix_activate_multiple($ids);

	drupal_set_message(t('Pricing Matrices actived'));
	drupal_goto(P1_PRICING_MATRIX_URI);
}

/**
 * Implements hook_submit().
 */
function p1_pricing_matrix_bulk_deactivate_submit($form, &$form_state) {
	$ids = $form_state['ids'];
	p1_pricing_matrix_deactivate_multiple($ids);

	drupal_set_message(t('Pricing Matrices actived'));
	drupal_goto(P1_PRICING_MATRIX_URI);
}
