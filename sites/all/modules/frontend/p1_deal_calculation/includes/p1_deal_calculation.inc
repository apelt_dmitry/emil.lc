<?php

function p1_deal_calculation_page($deal_id, $row_id) {
  global $user;
  $output = '';
  $variables = array();

  $deal = p1_deal_load($deal_id);

  $variables['deal_id'] = $deal_id;

  $data = unserialize($deal->hidden);
  $row = $data[$row_id];
  $interest = $row['rate'];

  $lending_program_id = $row['id'];
  $lending_program = p1_lending_program_load($lending_program_id);
  $loan_type = p1_helper_get_features_type_by_type('Loan type');
  $years = p1_helper_get_deal_years($deal, $lending_program_id);

  $amort = new amort($deal->loan_amount, $interest, $years);

  if ($amort->npmts) {
    $ebal[0] = $amort->amount;
    $ccint[0] = 0.0;
    $cpmnt[0] = 0.0;
    for ($i = 1; $i <= $amort->npmts; $i++) {
      $bbal[$i] = $ebal[$i - 1];
      $ipmnt[$i] = $bbal[$i] * $amort->mrate;
      $ppmnt[$i] = $amort->pmnt - $ipmnt[$i];
      $ebal[$i] = $bbal[$i] - $ppmnt[$i];
      $ccint[$i] = $ccint[$i - 1] + $ipmnt[$i];
      $cpmnt[$i] = $cpmnt[$i - 1] + $amort->pmnt;
    }

    $interest_array = [0, 0, 0];
    $principal_array = [0, 0, 0];

    for ($i = 0; $i <= 2; $i++) {
      for ($j = 1; $j <= 12; $j++) {
        $interest_array[$i] += $ipmnt[$i * 12 + $j];
        $principal_array[$i] += $ppmnt[$i * 12 + $j];
      }
    }

    $variables['term_sheet'] = '/deal/' . $deal_id . '/term/' . $row_id;

    $variables['program'] = $lending_program->name;
    $variables['property_type'] = p1_helper_get_property_type_name_by_id($lending_program->property_type_id);
    $variables['program_type'] = p1_helper_get_program_type_name_by_id($lending_program->program_type_id);
    $variables['loan_to_value'] = $deal->loan_to_value;
    $variables['loan_type'] = $loan_type[$deal->loan_type];

    $variables['address'] = $deal->address;
    $variables['address_2'] = $deal->address_2;
    $variables['city'] = $deal->city;
    $variables['state'] = $deal->state;
    $variables['zipcode'] = $deal->zipcode;

    $variables['price'] = p1_helper_currency($deal->purchase_price);
    $variables['loan_amount'] = p1_helper_currency($deal->loan_amount);
    $variables['rate'] = $interest . '% for ' . $years . ' years';
    $variables['principal_with_interest'] = p1_helper_currency($ipmnt[1] + $ppmnt[1]);

    $header = array(
      'income' => array('data' => t('Income')),
    );

    $current_year = date("Y");

    for ($i = 0; $i < 3; $i++) {
      $header[$current_year] = array('data' => $current_year);
      $current_year++;
    }

    $gross_schedule_income[0] = $deal->property_income;
    $plus_other_income[0] = $deal->other_income;
    $total_income[0] = $gross_schedule_income[0] + $plus_other_income[0];
    $less_vacancy_credit_loss[0] = 0.05 * $gross_schedule_income[0];
    $gross_operating_income[0] = $total_income[0] - $less_vacancy_credit_loss[0];
    $expenses[0] = $deal->expenses ? $deal->expenses : 0.35 * $gross_schedule_income[0];
    $net_operating_income[0] = $gross_operating_income[0] - $expenses[0];
    $less_total_annual_debt_services[0] = $interest_array[0] + $principal_array[0];
    $cash_flow_before_taxes[0] = $net_operating_income[0] - $less_total_annual_debt_services[0];
    $dscr = $net_operating_income[0] / $less_total_annual_debt_services[0] * 12;

    for ($i = 1; $i < 3; $i++) {
      $gross_schedule_income[$i] = $gross_schedule_income[$i - 1] * 1.03;
      $plus_other_income[$i] = $plus_other_income[$i - 1] * 1.03;
      $total_income[$i] = $gross_schedule_income[$i] + $plus_other_income[$i];
      $less_vacancy_credit_loss[$i] = $less_vacancy_credit_loss[$i - 1] * 1.03;
      $gross_operating_income[$i] = $total_income[$i] - $less_vacancy_credit_loss[$i];
      $expenses[$i] = $expenses[$i - 1] * 1.03;
      $net_operating_income[$i] = $gross_operating_income[$i] - $expenses[$i];
      $less_total_annual_debt_services[$i] = $interest_array[$i] + $principal_array[$i];
      $cash_flow_before_taxes[$i] = $net_operating_income[$i] - $less_total_annual_debt_services[$i];
      $cash_flow_before_taxes[$i] = $net_operating_income[$i] - $less_total_annual_debt_services[$i];
    }

    $rows = array(
      'gross_schedule_income' => array(
        'data' => array(
        'Gross schedule income',
        '$' . p1_helper_currency($gross_schedule_income[0]),
        '$' . p1_helper_currency($gross_schedule_income[1]),
        '$' . p1_helper_currency($gross_schedule_income[2])
        ),
        'class' => array('green-tr-1')
      ),
      'plus_other_income' => array(
        'data' => array(
        'Plus other income',
        '$' . p1_helper_currency($plus_other_income[0]),
        '$' . p1_helper_currency($plus_other_income[1]),
        '$' . p1_helper_currency($plus_other_income[2])
        ),
        'class' => array('green-tr-2')
      ),
      'total_income' => array(
        'data' => array(
        'Total income',
        '$' . p1_helper_currency($total_income[0]),
        '$' . p1_helper_currency($total_income[1]),
        '$' . p1_helper_currency($total_income[2])
        ),
        'class' => array('green-tr-3')
      ),
      'less_vacancy_credit_loss' => array(
        'data' => array(
        'Less: Vacancy / Credit Loss',
        '$' . p1_helper_currency($less_vacancy_credit_loss[0]),
        '$' . p1_helper_currency($less_vacancy_credit_loss[1]),
        '$' . p1_helper_currency($less_vacancy_credit_loss[2])
        ),
        'class' => array('red-tr-1')
      ),
      'gross_operating_income' => array(
        'data' => array(
        'Gross operating income',
        '$' . p1_helper_currency($gross_operating_income[0]),
        '$' . p1_helper_currency($gross_operating_income[1]),
        '$' . p1_helper_currency($gross_operating_income[2])
        ),
        'class' => array('green-tr-4')
      ),
      'expenses' => array(
        'data' => array(
          'Expenses',
          '$' . p1_helper_currency($expenses[0]),
          '$' . p1_helper_currency($expenses[1]),
          '$' . p1_helper_currency($expenses[2])
        ),
        'class' => array('red-tr-2')
      ),
      'net_operating_income' => array(
        'data' => array(
          'Net Operation Income',
          '$' . p1_helper_currency($net_operating_income[0]),
          '$' . p1_helper_currency($net_operating_income[1]),
          '$' . p1_helper_currency($net_operating_income[2])
        ),
        'class' => array('green-tr-5')
      ),
      'less_total_annual_debt_services' => array(
        'data' => array(
        'Less: Total Annual Debt Services',
        '$' . p1_helper_currency($less_total_annual_debt_services[0]),
        '$' . p1_helper_currency($less_total_annual_debt_services[1]),
        '$' . p1_helper_currency($less_total_annual_debt_services[2])
        ),
        'class' => array('red-tr-3')
      ),
      'cash_flow_before_taxes' => array(
        'data' => array(
        'CASH FLOW BEFORE TAXES',
        '$' . p1_helper_currency($cash_flow_before_taxes[0]),
        '$' . p1_helper_currency($cash_flow_before_taxes[1]),
        '$' . p1_helper_currency($cash_flow_before_taxes[2])
        ),
        'class' => array('green-tr-6')
      )
    );

    $variables['calculation_table'] = theme('table', array('header' => $header, 'rows' => $rows));

    if (p1_helper_currency($dscr) > $lending_program->min_dscr) {
      $variables['class'] = 'bg-green';
      $variables['message'] = '';
    }
    else {
      $variables['class'] = 'bg-red';
      $variables['message'] = 'DSCR does not meet our minimum requirments. However, other programs might be available, please call our head office at 1-(888) 358-3444</p>';
    }

    $variables['current_dscr'] = p1_helper_currency($dscr);

    if ($lending_program->min_dscr == '0' || $lending_program->min_dscr == '0.00') {
      $variables['minimum_dscr'] = 'Not required';
    }
    else {
      $variables['minimum_dscr'] = $lending_program->min_dscr;
    }

  }
  else {
    $variables['message'] = "Sorry, we are not able calculate cash flow for you by. We need more data to calculate.";
  }

  return theme('calculation', $variables);
}
