<?php

function p1_term_sheet_pdf_view($deal_id, $term_id) {
    require_once drupal_get_path('module', 'print') . '/print_pdf/print_pdf.pages.inc';
    return print_pdf_controller("deal", $deal_id, "term", $term_id, "view");
}
