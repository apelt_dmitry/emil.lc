<?php

function px_status_menu() {
  $items = array();

  $items['deal/%/status'] = array(
      'title' => 'Status',
      'page callback' => 'px_status_page',
      'page arguments' => array(1),
      'access arguments' => array('access content'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 8,
  );

  $items['deal/%/view'] = array(
      'title' => 'View',
      'page callback' => 'px_status_redirect',
      'page arguments' => array(1),
      'access arguments' => array('access content'),
      'type' => MENU_LOCAL_TASK,
      'weight' => -8,
  );

  return $items;
}

function px_status_permission() {
  return array(
    'administer deal status' => array(
      'title' => t('Administer deal status'),
      'description' => t('Perform administration tasks for deal status.'),
    ),
    'view deal status' => array(
      'title' => t('View deal status'),
      'description' => t('Perform view of the deal status.'),
    ),
  );
}

function px_status_redirect($deal_id) {
  drupal_goto('deal/'.$deal_id);
}

function px_status_type($type) {

  $types = array(
                'Condition' => 1,
                'Deal' => 2,
                'Client' => 3,
                'Access' => 4,
                'Message' => 5,
                );

  return empty($type) ? $types : $types[$type];
}

function px_status_access($access) {

  $accesses = array(
                'Public' => 0,
                'Private' => 1,
                );

  return empty($access) ? $accesses : $accesses[$access];
}

function px_status_docs($doc) {

  $docs = array(
                'N/A' => 0,
                'Pending' => 1,
                'Accepted' => 2,
                'Rejected' => 3,
                );

  return empty($doc) ? $docs : $docs[$doc];
}


function px_status_page($deal_id) {
  return drupal_get_form('px_status_form', $deal_id);
}

function px_status_form($form, &$form_state, $deal_id) {

  $deal = p1_deal_load($deal_id);

  $form['comments'] = array(
    '#title' => t('Ask a question'),
    '#type' => 'textarea',
    '#description' => t(''),
    '#default_value' => '',
    '#required' => TRUE,
  );

  $form['deal_id'] = array(
    '#type' => 'hidden',
    '#value' => $deal_id
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send message'),
    '#attributes' => array('class' => array('btn-lg btn-warning')),
  );

  $form['list'] = array(
    '#markup' => px_status_list($deal_id),
  );

  $form['created'] = array(
    '#markup' => format_interval(REQUEST_TIME - $deal->created, 2) . t(' ago'),
  );

  $image = path_to_theme() . '/images/phase-'.$deal->status.'.png';

  $form['status'] = array(
    '#markup' => '<img src="/'.$image.'" class="img-responsive" alt="Deal Status">',
  );

  return $form;
}

function px_status_form_validate($form, &$form_state) {
  // Validation logic.
}

function px_status_form_submit($form, &$form_state) {
  global $user;

  $record = array (
    'user_id' => $user->uid,
    'id_deal' =>  $form_state['values']['deal_id'],
    'type' =>  px_status_type('Message'),
    'access' =>  px_status_access('Public'),
    'doc_id' =>  '',
    'doc_status' =>  '',
    'comments' =>  check_plain(trim($form_state['values']['comments'])),
    'created' => REQUEST_TIME,
  );

  drupal_write_record ('px_status', $record);

}

function px_status_logger($data) {
  global $user;

  $record = array (
    'user_id' => $user->uid,
    'id_deal' =>  $data['deal_id'],
    'type' =>  $data['type'],
    'access' =>  $data['access'],
    'doc_id' =>  $data['doc_id'],
    'doc_status' =>  $data['doc_status'],
    'comments' =>  check_plain(trim($data['comments'])),
    'created' => REQUEST_TIME,
  );

  drupal_write_record ('px_status', $record);

}

function px_status_list($deal_id) {

  $types = array_flip(px_status_type(''));
  $accesses = array_flip(px_status_access(''));
  $docs = array_flip(px_status_docs(''));

  $header = array(
                  'User',
                  'Type',
                  'Access',
                  'Doc Status',
                  'Comments',
                  'Created',
                  );
  $rows = array();

  $query = db_select('px_status', 's');
  $query->leftJoin('users', 'u', 'u.uid = s.user_id');
  $query->fields('u', array('name'));
  $query->condition('s.id_deal', $deal_id, '=')
        ->fields('s')
        ->orderBy('s.created', 'DESC')
        ->range(0, 50);
  $result = $query->execute();

  while ($status = $result->fetchObject()) {

    $rows[] = array(
                    $status->name,
                    $types[$status->type],
                    $accesses[$status->access],
                    $docs[$status->doc_status],
                    $status->comments,
                    format_date($status->created, 'short'),
                    );

   }

  return theme('table', array('header' => $header, 'rows' => $rows));

}








