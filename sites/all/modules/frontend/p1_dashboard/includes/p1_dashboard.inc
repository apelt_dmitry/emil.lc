<?php

function p1_dashboard_callback() {
  if (user_is_anonymous()) {
    drupal_goto('user/login');
  }
  else {
    return drupal_get_form('p1_dashboard_form');
  }
}

function p1_dashboard_form($form, &$form_state) {
  global $user;

  $program_type = isset($_GET['program_type']) ? (int) $_GET['program_type'] : NULL;
  $property_type = isset($_GET['property_type']) ? (int) $_GET['property_type'] : NULL;
  $loan_type = isset($_GET['loan_type']) ? (int) $_GET['loan_type'] : NULL;
  $address = isset($_GET['address']) ? $_GET['address'] : '';

  $program_types = p1_helper_get_program_types();
  $property_types = p1_helper_get_property_types();
  $loan_types = p1_helper_get_features_type_by_type('Loan Type');

  $options = array(
    'uid' => $user->uid,
  );

  if($program_type) {
    $options['program_type_id'] = $program_type;
  }

  if($property_type) {
    $options['property_type_id'] = $property_type;
  }

  if($loan_type) {
    $options['loan_type'] = $loan_type;
  }

  if($address) {
    $options['address'] = $address;
  }

  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'p1_deal');
  $query->propertyCondition('uid', $user->uid);
  if($program_type) {
    $query->propertyCondition('program_type_id', $program_type);
  }
  if($property_type) {
    $query->propertyCondition('property_type_id', $property_type);
  }
  if($loan_type) {
    $query->propertyCondition('loan_type', $loan_type);
  }
  if($address) {
    $query->propertyCondition('address', '%' .$address . '%', 'like');
  }

  $query->pager(P1_DASHBOARD_PER_PAGE);
  $result = $query->execute();

  $ids = array();
  if (isset($result['p1_deal'])) {
    foreach ($result['p1_deal'] as $deal) {
      $ids[] = $deal->id;
    }
  }

  $deals = p1_deal_load_multiple($ids);
  $header = array(
    'address' => array('data' => t('Address'), 'field' => 'address'),
    'fico_score' => array('data' => t('FICO Score'), 'field' => 'fico_score'),
    'loan_type' => array('data' => t('Loan Type'), 'field' => 'loan_type'),
    'program_type' => array('data' => t('Program Type'), 'field' => 'program_type'),
    'property_type' => array('data' => t('Property Type'), 'field' => 'property_type'),
    'status' => array('data' => t('Phase'), 'field' => 'status'),
    'created' => array('data' => t('Created'), 'field' => 'created'),
    'operations' => array('data' => t('Operations'), 'field' => 'operations', 'align' => 'center','class' => 'text-center'),
  );

  $rows = array();

  foreach ($deals as $deal) {
    $rows[] = array(
      'address' => l($deal->address, P1_DEAL_VIEW_URI . $deal->id),
      'fico_score' => $deal->fico_score,
      'loan_type'=> $loan_types ? $loan_types[$deal->loan_type] : NULL,
      'program_type' => $program_types ? $program_types[$deal->program_type_id] : NULL,
      'property_type' => $property_types ? $property_types[$deal->property_type_id] : NULL,
      'status' => $deal->status == 0 ? 'Initial' : 'Phase '.$deal->status,
      'created' => format_interval(REQUEST_TIME - $deal->created, 2) . t(' ago'),
      'operations' => array('data' => l(t('Edit'), P1_DEAL_VIEW_URI . $deal->id . '/edit'), 'align' => 'center','class' => 'text-center'),
    );
  }

  $form['search'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['search']['address'] = array(
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => array('Search by Address')),
    '#default_value' => $address,
  );
  $form['search']['program_type'] = array(
    '#type' => 'select',
    '#options' => p1_helper_get_program_types(),
    '#empty_option' => t('- Program Type -'),
    '#default_value' => $program_type,
  );
  $form['search']['property_type'] = array(
    '#type' => 'select',
    '#options' => p1_helper_get_property_types(),
    '#empty_option' => t('Property Type'),
    '#default_value' => $property_type,
  );
  $form['search']['loan_type'] = array(
    '#type' => 'select',
    '#options' => p1_helper_get_features_type_by_type('Loan type'),
    '#empty_option' => t('Loan Type'),
    '#default_value' => $loan_type,
  );

  $form['search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#submit' => array('p1_dashboard_submit'),
  );

  $form['#submit'][] = 'p1_dashboard_submit';

  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('<div class="alert alert-block alert-info m-20"><p class="lead text-center">There are no deals yet. Go ahead and <a href="/deal/add" class="btn btn-lg btn-success">Add A Deal</a></p></div>'),
  );

  $form['pager']['#markup'] = theme('pager', $query->pager);

  return $form;
}

function p1_dashboard_submit($form, &$form_state){
  $values = $form_state['input'];
  $options = array();
  if (strlen($values['program_type'])) {
    $options['program_type'] = $values['program_type'];
  }
  if (strlen($values['property_type'])) {
    $options['property_type'] = $values['property_type'];
  }
  if (strlen($values['loan_type'])) {
    $options['loan_type'] = $values['loan_type'];
  }
  if (strlen($values['address'])) {
    $options['address'] = $values['address'];
  }
  if (!empty($options)) {
    $options = array('query' => $options);
  }
  drupal_goto(current_path(), $options);
}
