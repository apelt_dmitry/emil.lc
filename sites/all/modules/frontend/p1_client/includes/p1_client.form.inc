<?php

/**
 * Form builder.
 */
function p1_client_form($form, &$form_state, $id_deal, $update = NULL) {

  // load single client entity
  $client = entity_load_single('p1_client', $id_deal);

  $form_state['id'] = $id_deal;
  if($update){
  // if client not create, need create first
    if(!$client){
      drupal_goto('/deal/' . $id_deal . '/client/create');
    }
    $form_state['update'] = TRUE;
  }

  $form['first_name'] = array(
    '#title' => 'First Name',
    '#type' => 'textfield',
    '#default_value' => isset($client->first_name) ? $client->first_name : '',
    '#required' => TRUE,
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,

  );
  $form['last_name'] = array(
    '#title' => 'Last Name',
    '#type' => 'textfield',
    '#default_value' => isset($client->last_name) ? $client->last_name : '',
    '#required' => TRUE,
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['email'] = array(
    '#title' => 'Email',
    '#type' => 'textfield',
    '#default_value' => isset($client->email) ? $client->email : '',
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['cell'] = array(
    '#title' => 'Cell',
    '#type' => 'textfield',
    '#default_value' => isset($client->cell) ? $client->cell : '',
    '#required' => TRUE,
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['telephone'] = array(
    '#title' => 'Telephone',
    '#type' => 'textfield',
    '#default_value' => isset($client->telephone) ? $client->telephone : '',
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['fax'] = array(
    '#title' => 'Fax',
    '#type' => 'textfield',
    '#default_value' => isset($client->fax) ? $client->fax : '',
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,

  );
  $form['address'] = array(
    '#title' => 'Address',
    '#type' => 'textfield',
    '#default_value' => isset($client->address) ? $client->address : '',
    '#required' => TRUE,
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['address_2'] = array(
    '#title' => 'Address 2',
    '#type' => 'textfield',
    '#default_value' => isset($client->address_2) ? $client->address_2 : '',
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['city'] = array(
    '#title' => 'City',
    '#type' => 'textfield',
    '#default_value' => isset($client->city) ? $client->city : '',
    '#required' => TRUE,
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['state'] = array(
    '#title' => t('State'),
    '#type' => 'select',
    '#options' => p1_helper_get_states(),
    '#default_value' => isset($client->state) ? $client->state : NULL,
    '#required' => TRUE,
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['zipcode'] = array(
    '#title' => t('Zip code'),
    '#type' => 'textfield',
    '#default_value' => isset($client->zipcode) ? $client->zipcode : NULL,
    '#maxlength' => 10,
    '#required' => TRUE,
    '#disabled' => (!empty($client) && !$update) ? TRUE : FALSE,
  );
  $form['signature'] = array(
    '#title' => t('Signature'),
    '#type' => 'signaturefield',
    '#color' => '#000',
    '#default_value' => isset($client->signature) ? $client->signature : NULL,
    '#required' => TRUE,
  );

  if (!empty($form_state['signature'])) {
    $form['signature'] = array(
      '#value' => theme('signaturefield_display', $form_state['signature'])
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',
  );

  if (!$entity = entity_load_single('p1_client', $form_state['id']) && !$update) {
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save',
    );
    $form['#validate'][] = 'p1_client_validate';
  }
  return $form;
}

/**
 * Implements hook_validate_client_form().
 */
function p1_client_validate(&$form, &$form_state)
{
    //validate email
    $email = trim($form_state['values']['email']);
    if ($error = user_validate_mail($email)) {
        form_set_error('email', $error);
    }
    //validate first_name
    $first_name = trim($form_state['values']['first_name']);
    if ($first_name != '') {
        if ($error = user_validate_name($first_name)) {
            form_set_error('first_name', $error);
        }
    }
    //validate last_name
    $last_name = trim($form_state['values']['last_name']);
    if ($last_name != '') {
        if ($error = user_validate_name($last_name)) {
            form_set_error('first_name', $error);
        }
    }
    //validate telephone
    $telephone = trim($form_state['values']['telephone']);
    if ($telephone != '' && !preg_match('#\d{10}#', $telephone)) {
        form_set_error('profile_phone', 'Field Telephone is not valid. Example of correct filling: 0001112222');
    }
    //validate zipcode
    $zipcode = trim($form_state['values']['zipcode']);
    if ($zipcode != '') {
        if (!preg_match('/(^\d{5}$)|(^\d{5}-\d{4}$)/', $zipcode)) {
            form_set_error('zipcode', t('Zipcode must be in format 12345.'));
        }
    }
    //validate fax
    $fax = trim($form_state['values']['fax']);
    if ($fax != '') {
        if (!preg_match('#\d{10}#', $fax)) {
            form_set_error('fax', t('Fax number must be in format 0001112222.'));
        }
    }

}

/**
 * Form submit callback.
 */
function p1_client_form_submit($form, &$form_state) {
  $entity_type = 'p1_client';
  if (isset($form_state['update'])) {
    $entity = entity_load_single($entity_type, $form_state['id']);
    $entity->id = $form_state['id'];
    $entity->first_name = $form_state['values']['first_name'];
    $entity->last_name = $form_state['values']['last_name'];
    $entity->email = $form_state['values']['email'] ? $form_state['values']['email'] : NULL;
    $entity->cell = $form_state['values']['cell'];
    $entity->telephone = $form_state['values']['telephone'] ? $form_state['values']['telephone'] : NULL;
    $entity->fax = $form_state['values']['fax'] ? $form_state['values']['fax'] : NULL;
    $entity->address = $form_state['values']['address'];
    $entity->address_2 = $form_state['values']['address_2'];
    $entity->city = $form_state['values']['city'];
    $entity->state = $form_state['values']['state'];
    $entity->zipcode = $form_state['values']['zipcode'];
    $entity->signature = $form_state['values']['signature'];
    $entity->save();

  }else{
    $values = array();
    $values['id'] = $form_state['id'];
    $values['first_name'] = $form_state['values']['first_name'];
    $values['last_name'] = $form_state['values']['last_name'];
    $values['email'] = $form_state['values']['email'] ? $form_state['values']['email'] : NULL;
    $values['cell'] = $form_state['values']['cell'];
    $values['telephone'] = $form_state['values']['telephone'] ? $form_state['values']['telephone'] : NULL;
    $values['fax'] = $form_state['values']['fax'] ? $form_state['values']['fax'] : NULL;
    $values['address'] = $form_state['values']['address'];
    $values['address_2'] = $form_state['values']['address_2'];
    $values['city'] = $form_state['values']['city'];
    $values['state'] = $form_state['values']['state'];
    $values['zipcode'] = $form_state['values']['zipcode'];
    $values['signature'] = $form_state['values']['signature'];

    $entity = entity_create($entity_type, $values);
    $entity->save();
  }
  if(module_exists('px_status')) {
    // Log the status
    $logger = array();
    $logger['deal_id'] = $form_state['id'];
    $logger['type'] = px_status_type('Client');
    $logger['access'] = px_status_access('Public');
    $logger['doc_id'] = '';
    $logger['doc_status'] = '';
    if (isset($form_state['update'])) {
      $logger['comments'] = 'Updating client data for deal ' . $form_state['id'];
    } else {
      $logger['comments'] = 'Creating client data for deal ' . $form_state['id'];
    }

    px_status_logger($logger);
  }

  drupal_goto('/deal/' . $form_state['id'] . '/client');
  drupal_set_message('Client data saved');
}
