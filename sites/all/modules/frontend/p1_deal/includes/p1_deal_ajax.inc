<?php

function p1_deal_property_ajax($property) {
    $changes = [];
    $changes['property_type_id'] = $property;
    $terms = p1_helper_available_years('Term', $changes);
    $programs = p1_helper_available_program_types($changes);
    $amortizations = p1_helper_available_years('Amortization', $changes);
    $penalties = p1_helper_available_prepayments($property);
    $docs = p1_helper_get_docs_category();
    return drupal_json_output(['program_type' => $programs, 'terms' => $terms, 'amortization' => $amortizations, 'penalty' => $penalties, 'doc' => $docs]);
}

function p1_deal_property_ajax_all() {
    $programs = p1_helper_available_program_types();
    $terms = p1_helper_available_years('Term');
    $amortizations = p1_helper_available_years('Amortization');
    $penalties = p1_helper_available_prepayments();
    $docs = p1_helper_get_docs_category();
    return drupal_json_output(['program_type' => $programs, 'terms' => $terms, 'amortization' => $amortizations, 'penalty' => $penalties, 'doc' => $docs]);
}