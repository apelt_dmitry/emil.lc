<?php
/**
 * @file
 * The file for admin forms and functionality for the p1_deal entity
 */

/**
 * Implements hook_form().
 */
function p1_deal_form($form, &$form_state, $deal = NULL) {
  $form = array();
  $terms = array();
  $amortizations = array();
  $pre_payments = array();

  // Terms
  $terms_query_result = p1_helper_feature_getter('Term');

  foreach ($terms_query_result as $term) {
    $terms[$term->id] = $term->name;
  }

  // Amortizations
  $amortizations_query_result = p1_helper_feature_getter('Amortization');

  foreach ($amortizations_query_result as $amortization) {
    $amortizations[$amortization->id] = $amortization->name;
  }

  // Pre-Payment Penalty
  $pre_payment_query_result = p1_helper_feature_getter('Pre-Payment');

  foreach ($pre_payment_query_result as $pre_payment) {
    $pre_payments[$pre_payment->id] = $pre_payment->name;
  }

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'p1_helper') . '/js/msa_zipcode.js' => array(
      'type' => 'file',
    ),
    drupal_get_path('module', 'p1_deal') . '/js/p1_deal.js' => array(
        'type' => 'file',
    ),
    drupal_get_path('module', 'p1_term_sheet') . '/js/jquery.signaturepad.min.js' => array(
      'type' => 'file',
    ),
  );

  $form['loan_type'] = array(
    '#title' => t('Loan for'),
    '#type' => 'select',
    '#options' => p1_helper_get_features_type_by_type('Loan type'),
    '#empty_option' => t('Loan types'),
    '#default_value' => isset($deal->loan_type) ? $deal->loan_type : NULL,
    '#required' => TRUE,
    '#ajax' => [
       'callback' => 'p1_deal_loan_type_ajax_callback',
       'wrapper' => '',
    ],
  );

  $form['purchase_wrapper'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#prefix' => '<div id="ajax_purchase">',
    '#suffix' => '</div>'
  );

  $form['purchase_wrapper']['purchase_price'] = array(
    '#title' => t('Purchase Price'),
    '#attributes' => array('class' => array('currency')),
    '#type' => 'textfield',
    '#default_value' => isset($deal->purchase_price) ? $deal->purchase_price : '',
    '#access' => isset($form_state['values']['loan_type']) && $form_state['values']['loan_type'] == "5" ? TRUE : FALSE,
    '#field_prefix' => '$',
    '#input_group' => TRUE,
  );

  $form['fico_score'] = array(
    '#title' => t('FICO Score'),
    '#attributes' => array(),
    '#type' => 'textfield',
    '#default_value' => isset($deal->fico_score) ? $deal->fico_score : '',
    '#required' => TRUE,
  );

  $form['down_payment_percentage_wrapper'] = array(
    '#container' => TRUE,
    '#prefix' => '<div id="ajax_down_percentage">',
    '#suffix' => '</div>',
  );

  $form['down_payment_percentage_wrapper']['down_payment_percentage'] = array(
    '#title' => t('Down Payment percentage'),
    '#attributes' => array(),
    '#type' => 'textfield',
    '#default_value' => isset($deal->down_payment_percentage) ? $deal->down_payment_percentage : '',
    '#required' => TRUE,
    '#access' => isset($form_state['values']['loan_type']) && $form_state['values']['loan_type'] == "5" ? TRUE : FALSE,
    '#field_suffix' => '%',
    '#input_group' => TRUE,
  );

  $form['down_payment_amount_wrapper'] = array(
    '#container' => TRUE,
    '#prefix' => '<div id="ajax_down_amount">',
    '#suffix' => '</div>',
  );

  $form['down_payment_amount_wrapper']['down_payment_amount'] = array(
    '#title' => t('Down Payment amount'),
    '#attributes' => array('class' => array('currency')),
    '#type' => 'textfield',
    '#default_value' => isset($deal->down_payment_amount) ? $deal->down_payment_amount : '',
    '#required' => TRUE,
    '#access' => isset($form_state['values']['loan_type']) && $form_state['values']['loan_type'] == "5" ? TRUE : FALSE,
    '#field_prefix' => '$',
    '#input_group' => TRUE,
  );

  $form['property_type_id'] = array(
    '#title' => t('Property Type'),
    '#type' => 'select',
    '#options' => p1_helper_get_property_types(),
    '#empty_option' => t('Property type'),
    '#default_value' => isset($deal->property_type_id) ? $deal->property_type_id : NULL,
    '#required' => TRUE,

  );

  $form['appraised_value_wrapper'] = array(
    '#container' => TRUE,
    '#prefix' => '<div id="ajax_apprised">',
    '#suffix' => '</div>',
  );

  $form['appraised_value_wrapper']['appraised_value'] = array(
    '#title' => t('Appraised Value'),
    '#attributes' => array('class' => array('currency')),
    '#type' => 'textfield',
    '#default_value' => isset($deal->appraised_value) ? $deal->appraised_value : '',
    '#access' => isset($form_state['values']['loan_type']) && $form_state['values']['loan_type'] != "5" ? TRUE : FALSE,
    '#field_prefix' => '$',
    '#input_group' => TRUE,
  );

  $form['loan_amount'] = array(
    '#title' => t('Loan Amount'),
    '#attributes' => array('class' => array('currency')),
    '#type' => 'textfield',
    '#default_value' => isset($deal->loan_amount) ? $deal->loan_amount : '',
    '#required' => TRUE,
  );

  $form['loan_to_value'] = array(
    '#title' => t('Loan to Value'),
    '#attributes' => array(),
    '#type' => 'textfield',
    '#default_value' => isset($deal->loan_to_value) ? $deal->loan_to_value : '',
    '#required' => TRUE,
  );

  $form['bankruptcy_date'] =array(
    '#title' => t('Bankruptcy date'),
    '#description' => t('Only if applicable'),
    '#type' => 'date_popup',
    '#date_label_position' => 'none',
    '#date_format' => 'm/d/Y',
    '#attributes' => array('autocomplete' =>'off','readonly' => 'readonly', 'class' => array('date')),
    '#default_value' => isset($deal->bankruptcy_date) ? date('Y-m-d', $deal->bankruptcy_date) : NULL,
  );

  $form['foreclosure_date'] =array(
    '#title' => t('Foreclosure date'),
    '#description' => t('Only if applicable'),
    '#type' => 'date_popup',
    '#date_label_position' => 'none',
    '#date_format' => 'm/d/Y',
    '#attributes' => array('autocomplete' =>'off','readonly' => 'readonly', 'class' => array('date')),
    '#default_value' => isset($deal->foreclosure_date) ? date('Y-m-d', $deal->foreclosure_date) : NULL,
  );

  $form['address'] = array(
    '#title' => t('Address'),
    '#attributes' => array(),
    '#default_value' => isset($deal->address) ? $deal->address : NULL,
    '#type' => 'textfield',
  );

  $form['address_2'] = array(
    '#title' => t('Suite'),
    '#attributes' => array(),
    '#default_value' => isset($deal->address_2) ? $deal->address_2 : NULL,
    '#type' => 'textfield',
  );

  $form['city'] = array(
    '#title' => t('City'),
    '#type' => 'textfield',
    '#default_value' => isset($deal->city) ? $deal->city : NULL,
    '#attributes' => array('class' => array('msa-city')),
  );

  $form['state'] = array(
    '#title' => t('State'),
    '#type' => 'select',
    '#options' => p1_helper_get_states(),
    '#default_value' => isset($deal->state) ? $deal->state : NULL,
    '#required' => TRUE,
    '#empty_option' => t('State'),
    '#attributes' => array('class' => array('msa-state')),
  );

  $form['zipcode'] = array(
    '#title' => t('Zip code'),
    '#type' => 'textfield',
    '#default_value' => isset($deal->zipcode) ? $deal->zipcode : '',
    '#maxlength'=> 10,
    '#attributes' => array('class' => array('msa-zipcode')),
  );

  $form['vacancy_factor'] = array(
    '#title' => t('Any Vacancy Factor'),
    '#type' => 'textfield',
    '#default_value' => isset($deal->vacancy_factor) ? $deal->vacancy_factor : '',
    '#attributes' => array(),
  );

  $form['available_units'] = array(
    '#title' => t('Available units'),
    '#type' => 'textfield',
    '#default_value' => isset($deal->available_units) ? $deal->available_units : '',
    '#attributes' => array(),
  );

  $form['occupancy'] = array(
    '#title' => t('Occupancy'),
    '#type' => 'select',
    '#options' => p1_helper_get_occupancy(),
    '#default_value' => isset($deal->occupancy) ? $deal->occupancy : 0,
    '#required' => TRUE,
  );

  $form['property_income'] = array(
    '#title' => t('Annual potential gross income'),
    '#type' => 'textfield',
    '#default_value' => isset($deal->property_income) ? $deal->property_income : '',
    '#attributes' => array('class' => array('currency')),
  );

  $form['other_income'] = array(
    '#title' => t('Other annual potential income'),
    '#type' => 'textfield',
    '#default_value' => isset($deal->other_income) ? $deal->other_income : '',
    '#attributes' => array('class' => array('currency')),
  );

  $form['expenses'] = array(
    '#title' => t('Annual gross expenses'),
    '#type' => 'textfield',
    '#default_value' => isset($deal->expenses) ? $deal->expenses : '',
    '#attributes' => array('class' => array('currency')),
  );

  $form['lending_program'] = array(
    '#type' => 'hidden',
    '#value' => isset($deal->lending_program) ? $deal->lending_program : 0,
  );

  if (isset($form_state['values']['property_type_id'])) {
    $changes = array();
    $changes['property_type_id'] = $form_state['values']['property_type_id'];
    $changes['loan_type'] = $form_state['values']['loan_type'];
    $changes['loan_amount'] = $form_state['values']['loan_amount'];

    $terms = p1_helper_available_years('Term', $changes);
    $amortizations = p1_helper_available_years('Amortization', $changes);
    $penaltys = p1_helper_available_prepayments($form_state['values']['property_type_id']);
  }
  elseif (isset($deal->property_type_id)) {
    $changes = array();
    $changes['property_type_id'] = $deal->property_type_id;
    $changes['loan_type'] = $deal->loan_type;
    $changes['loan_amount'] = $deal->loan_amount;

    $terms = p1_helper_available_years('Term', $changes);
    $amortizations = p1_helper_available_years('Amortization', $changes);
    $penaltys = p1_helper_available_prepayments($deal->property_type_id);
  }
  else {
    $changes = array();
    $terms = p1_helper_available_years('Term');
    $amortizations = p1_helper_available_years('Amortization');
    $penaltys = p1_helper_available_prepayments();
  }

  $programs= p1_helper_available_program_types($changes);

  $form['program_type_id'] = array(
    '#title' => t('Program type'),
    '#type' => 'select',
    '#options' => $programs,
    '#empty_option' => t(' - Pick one - '),
    '#default_value' => isset($deal->program_type_id) ? $deal->program_type_id : NULL,
    '#required' => TRUE,
    '#prefix' => '<div id="ajax_program_type">',
    '#suffix' => '</div>'
  );

  $form['terms'] = array(
    '#title' => t('Terms (years)'),
    '#type' => 'select',
    '#options' => $terms,
    '#empty_option' => t(' - Pick one - '),
    '#default_value' => isset($deal->terms) ? $deal->terms : '',
    '#requred' => TRUE,
    '#prefix' => '<div id="ajax_terms">',
    '#suffix' => '</div>'
  );

  $form['amortization'] = array(
    '#title' => t('Amortization (years)'),
    '#type' => 'select',
    '#options' => $amortizations,
    '#empty_option' => t(' - Pick one - '),
    '#default_value' => isset($deal->amortization) ? $deal->amortization : '',
    '#requred' => TRUE,
    '#prefix' => '<div id="ajax_amortization">',
    '#suffix' => '</div>'
  );

  $form['pre_payment_penalty'] = array(
    '#title' => t('Pre payment penalty'),
    '#type' => 'select',
    '#options' => $penaltys,
    '#empty_option' => t(' - Required - '),
    '#default_value' => isset($deal->pre_payment_penalty) ? $deal->pre_payment_penalty : '',
    '#required' => TRUE,
    '#prefix' => '<div id="ajax_penalty">',
    '#suffix' => '</div>'
  );

  $form['doc_id'] = array(
    '#title' => t('Doc'),
    '#type' => 'select',
    '#options' => p1_helper_get_docs_category(),
    '#empty_option' => t('Select doc'),
    '#default_value' => isset($deal->doc_id) ? $deal->doc_id : 'stated',
    '#required' => TRUE,
    '#prefix' => '<div id="doc">',
    '#suffix' => '</div>'
  );

  $form['status'] = array(
    '#type' => 'hidden',
    '#value' => isset($deal->status) ? $deal->status : 0,
  );

  $form['disclaimer'] = array(
    '#markup' => t('<strong>Warning:</strong> A  Conditional Loan Pre-Approval (CLP) will be provided to you in less than 3 minutes based on the information you have inputed into our software. Therefore, the CLP is subject to further data and/or document verification.'),
  );

  $form['#validate'][] = 'p1_deal_form_validate';

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => isset($deal->id) ? t('Update Deal') : t('Get Programs'),
      '#attributes' => array('class' => array('btn-lg btn-success')),
    ),
    'delete_link' => array(
      '#markup' => isset($deal->id) ? l(t('Delete'), P1_DEAL_MANAGE_URI . $deal->id . '/delete', array('attributes' => array('class' => array('button remove')))) : ''),
    '#suffix' => '</div>'
  );
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function p1_deal_form_validate($form, &$form_state) {
  $form_state['values']['purchase_price'] = str_replace(',', '', $form_state['values']['purchase_wrapper']['purchase_price']);
  $form_state['values']['appraised_value'] = str_replace(',', '', $form_state['values']['appraised_value']);
  $form_state['values']['loan_amount'] = str_replace(',', '', $form_state['values']['loan_amount']);
  $form_state['values']['down_payment_amount'] = str_replace(',', '', $form_state['values']['down_payment_amount']);

  $form_state['values']['property_income'] = str_replace(',', '', $form_state['values']['property_income']);
  $form_state['values']['other_income'] = str_replace(',', '', $form_state['values']['other_income']);
  $form_state['values']['expenses'] = str_replace(',', '', $form_state['values']['expenses']);

  $vacancy_factor = trim($form_state['values']['vacancy_factor']);
  if (!is_numeric($vacancy_factor)) {
    form_set_error('vacancy_factor', 'Vacancy Factor must be numeric.');
  }
  $property_income = trim($form_state['values']['property_income']);
  if (!is_numeric($property_income)) {
    form_set_error('property_income', 'Annual potential gross income is required.');
  }
  $other_income = trim($form_state['values']['other_income']);
  if (!is_numeric($other_income)) {
    form_set_error('other_income', 'Other annual potential income is required.');
  }
  $expenses = trim($form_state['values']['expenses']);
  if (!is_numeric($expenses)) {
    form_set_error('expenses', 'Annual gross expenses is required.');
  }

  $property_type = p1_helper_get_property_types();
  $pt_id = $form_state['values']['property_type_id'];

  if ( $property_type[$pt_id] == 'Multifamily' || $property_type[$pt_id] == 'Mixed-Used' ) {
    //if ($form_state['values']['loan_amount'] < 500000 && $form_state['values']['loan_to_value'] > 70)

    if ($form_state['values']['loan_to_value'] > 70 && $form_state['values']['loan_amount'] < 500000) {
      form_set_error('loan_to_value', 'LTV is outside the allowed range for the type of property selected.');
    }

    if ($form_state['values']['loan_to_value'] > 75 && $form_state['values']['loan_amount'] > 500000) {
      form_set_error('loan_to_value', 'LTV is outside the allowed range for the type of property selected.');
    }

  }

}

/**
 * Implements hook_form_submit().
 */
function p1_deal_form_submit($form, &$form_state) {
  global $user;

  unset($form_state['values']['disclaimer']);

  //Remove comas if any
  $form_state['values']['purchase_price'] = str_replace(',', '', $form_state['values']['purchase_price']);
  $form_state['values']['appraised_value'] = str_replace(',', '', $form_state['values']['appraised_value']);
  $form_state['values']['loan_amount'] = str_replace(',', '', $form_state['values']['loan_amount']);
  $form_state['values']['down_payment_amount'] = str_replace(',', '', $form_state['values']['down_payment_amount']);

  $form_state['values']['property_income'] = str_replace(',', '', $form_state['values']['property_income']);
  $form_state['values']['other_income'] = str_replace(',', '', $form_state['values']['other_income']);
  $form_state['values']['expenses'] = str_replace(',', '', $form_state['values']['expenses']);

  if (empty($form_state['values']['purchase_price'])) {
    $form_state['values']['purchase_price'] = 0;
  }

  if (intval($form_state['values']['purchase_price']) > 1 && empty($form_state['values']['appraised_value'])) {
    $form_state['values']['appraised_value'] = $form_state['values']['purchase_price'];
  }

  $deal = entity_ui_form_submit_build_entity($form, $form_state);
  $deal->uid = $user->uid;
  if (!empty($form_state['values']['bankruptcy_date'])) {
    $deal->bankruptcy_date = strtotime($form_state['values']['bankruptcy_date']);
  }
  if (!empty($form_state['values']['bankruptcy_date'])) {
    $deal->foreclosure_date = strtotime($form_state['values']['foreclosure_date']);
  }
  $deal->terms = $form_state['values']['terms'];
  $deal->amortization = $form_state['values']['amortization'];
  $deal->pre_payment_penalty = $form_state['values']['pre_payment_penalty'] ? $form_state['values']['pre_payment_penalty'] : NULL;

  // Calulate load amount


  // Calculate loan to value (LTV)
  if ($deal->loan_type == 5) {
    $deal->loan_amount = $deal->purchase_price - $deal->down_payment_amount;
    $deal->loan_to_value = ( $deal->loan_amount / $deal->purchase_price ) * 100;
  }
  else {
    $deal->loan_amount = $deal->appraised_value * $deal->loan_to_value / 100;
    //$deal->loan_to_value = ( $deal->loan_amount / $deal->appraised_value ) * 100;
  }

  $deal->save();
  drupal_set_message(t('Deal has been saved.'));

  if (in_array('admin', $user->roles)) {
    $form_state['redirect'] = '/node';
  }
  else {
    $form_state['redirect'] = P1_DEAL_VIEW_URI . $deal->id;
  }
}

/**
 * Confirmation before bulk deleting p1_deal.
 */
function p1_deal_bulk_delete($form, &$form_state, $ids) {
  $ids = explode('|', $ids);
  $deals = p1_deal_load_multiple($ids);

  $form = array();
  $form_state['ids'] = $ids;

  $variables = array(
    'type' => 'ul',
    'items' => array(),
    'title' => '',
    'attributes' => array(),
  );

  foreach ($deals as $deal) {
    $variables['items'][] = $deal->id;
  }

  $form['summary'] = array(
    '#markup' => theme_item_list($variables),
  );
  return confirm_form($form, t('Delete all Deals?'), '/node', '', t('Delete all'), t('Cancel'));
}

/**
 * Implements hook_submit().
 */
function p1_deal_bulk_delete_submit($form, &$form_state) {
  $ids = $form_state['ids'];
  p1_deal_delete_multiple($ids);

  drupal_set_message(t('Deals deleted'));
  drupal_goto('/node');
}


function p1_deal_property_type_ajax_callback($form, $form_state) {
  $commands = [];
  $commands[] = ajax_command_replace("#ajax_program_type", drupal_render($form['program_type_id']));
  $commands[] = ajax_command_replace("#ajax_terms", drupal_render($form['terms']));
  $commands[] = ajax_command_replace("#ajax_amortization", drupal_render($form['amortization']));
  $commands[] = ajax_command_replace("#ajax_penalty", drupal_render($form['pre_payment_penalty']));
  $commands[] = ajax_command_replace("#ajax_doc", drupal_render($form['doc_id']));
  return ['#type' => 'ajax', '#commands' => $commands];
}


function p1_deal_loan_type_ajax_callback(&$form, &$form_state) {
    drupal_rebuild_form($form, $form_state);
    $commands = [];
    $commands[] = ajax_command_replace("#ajax_purchase", drupal_render($form['purchase_wrapper']));
    $commands[] = ajax_command_replace("#ajax_down_amount", drupal_render($form['down_payment_amount_wrapper']));
    $commands[] = ajax_command_replace("#ajax_down_percentage", drupal_render($form['down_payment_percentage_wrapper']));
    $commands[] = ajax_command_replace("#ajax_apprised", drupal_render($form['appraised_value_wrapper']));
    return ['#type' => 'ajax', '#commands' => $commands];
}
