<?php

/**
 * Form builder.
 */
function p1_deal_upload_form($form, &$form_state, $deal_id) {
  $deal = p1_deal_load($deal_id);
  $doc = p1_docs_load($deal->doc_id);

  $statuses = p1_helper_get_deal_file_statuses();
  $phases = p1_helper_get_phase();
  $phase = $deal->status;
  $conditions = p1_helper_get_docs($deal, $phase);
  $review_status = '';
  $form_state['deal'] = $deal;

  // Update deal status if all conditions are meet
  p1_deal_upload_update_deal_status($deal->id);

  $form_state['deal'] = $deal;

  global $user;
  $permissions = p1_helper_role_access($deal);

  if (p1_deal_access_menu_callback('upload', $deal)) {
    $phase = $deal->status;
    if ($phase < 5) {
      $form['type'] = array(
        '#title' => t('Type'),
        '#type' => 'select',
        '#options' => $conditions,
        '#empty option' => t('- Select type -'),
        '#required' => TRUE,
      );

      $form['file'] = array(
        '#type' => 'managed_file',
        '#title' => t('Document'),
        '#description' => t('Only PDF documents allowed.'),
        '#upload_location' => 'private://deals/deals-' . $deal->id,
        '#upload_validators' => array(
          'file_validate_extensions' => array('pdf'),
        ),
      );

      $form['comment'] = array(
        '#title' => t('Comments'),
        '#type' => 'textarea',
        '#rows' => 1,
      );

      $form['phase'] = array(
        '#markup' => $phases[$deal->status],
      );

      $form['conditions'] = array(
        '#markup' => theme('item_list', array('items' => $conditions)),
      );

      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit condition'),
      );
    }
  }

  if (p1_deal_access_menu_callback('view documents', $deal)) {

    $header = array(
      'name' => array('data' => t('Document name'), 'field' => 'filename'),
      'provided_by' => array(
        'data' => t('Provided by'),
        'field' => 'provided_by'
      ),
      'uploaded_date' => array(
        'data' => t('Uploaded date'),
        'field' => 'created'
      ),
      'file_status' => array(
        'data' => t('Status'),
        'field' => 'file_status'
      ),
      'phase' => array(
        'data' => t('Phase'),
        'field' => 'phase'
      ),
    );

    if (p1_deal_access_menu_callback('download', $deal)) {
      $header['operations'] = array('data' => t('Operations'), 'colspan' => 2);
    }

    $rows = array();

    $category = db_select('p1_deal', 'deal');
    $category->leftJoin('p1_docs', 'doc', 'doc.category = deal.doc_id');
    $category = $category->condition('deal.id', $deal->id);
    $category = $category->fields('doc', array('category'));
    $category = $category->execute()->fetchField();

    $query = db_select('p1_deal_files', 'df');
    $query->leftJoin('p1_docs', 'd', 'd.id = df.doc_id');
    $query->leftJoin('p1_deal_status', 's', 's.fid = df.id');
    $query = $query->fields('df', array('created', 'doc_id', 'fid', 'did', 'id'));
    $query = $query->fields('d', array('name', 'provided_by', 'phase'));
    $query = $query->fields('s', array('file_status'));
    $query = $query->condition('df.did', $deal->id);
    $query = $query->condition('d.category', $category);
    $result = $query->execute()->fetchAll();

    $statuses = p1_helper_get_deal_file_statuses();
    $review_status = '';
    foreach ($result as $key => $value) {
      $file = file_load($value->fid);

      if ($value->file_status == '') {
        $review_status = 'Pending review';
      }
      else {
        $review_status = $statuses[$value->file_status];
      }

      $rows[$key] = array(
        'name' => $value->name,
        'provided_by' => ucfirst($value->provided_by),
        'uploaded_date' => format_date($value->created, 'short'),
        'status' => $review_status,
        'phase' => $phases[$value->phase],
      );
      if (p1_deal_access_menu_callback('download', $deal)) {
        $rows[$key]['operations'] = l(
          t('Download'),
          file_create_url($file->uri),
          array('attributes' => array('class' => 'btn btn-success'))
        );
      }
      if (module_exists('p1_deal_status')) {
        $status = db_select('p1_deal_status')
          ->condition('fid', $value->id)
          ->condition('did', $value->did)
          ->fields(NULL, ['file_status'])
          ->execute()
          ->fetchField();

        if (module_exists('p1_deal_status')) {
          if (!isset($status) && p1_deal_status_access_callback('review', $deal)) {
            $rows[$key]['review'] = l(
              t('Review'),
              '/deal/' . $value->did . '/doc/' . $value->id . '/review',
              array('attributes' => array('class' => 'btn btn-warning'))
            );
          }
          elseif ($status == 1 && p1_deal_status_access_callback('email', $deal)) {
            $rows[$key]['review'] = l(
              t('Email'),
              '/deal/' . $value->did . '/doc/' . $value->id . '/email',
              array('attributes' => array('class' => 'btn btn-danger'))
            );
          }
          else {
            $rows[$key]['review'] = '';
          }
        }
      }
    }

    $form['table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
  }

  return $form;
}

/**
 * Form submit callback.
 */
function p1_deal_upload_form_submit($form, &$form_state) {
  $file = file_load($form_state['values']['file']);
  if ($file) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    $deal = $form_state['deal'];
    $deal_file_id = db_insert('p1_deal_files')
      ->fields(array(
        'uid' => $deal->uid,
        'did' => $deal->id,
        'fid' => $file->fid,
        'doc_id' => $form_state['values']['type'],
        'user_comment' => $form_state['values']['comment'],
        'created' => REQUEST_TIME,
        'updated' => REQUEST_TIME,
      ))
      ->execute();
    drupal_set_message('File uploaded');

  if (module_exists('p1_deal_status')) {
    global $user;
    entity_create('p1_deal_status', array(
      'uid' => $deal->uid,
      'did' => $deal->id,
      'fid' => $deal_file_id,
      'comment' => 'File was uploaded by ' . $user->mail . ($form_state['values']['comment'] ? ' with comment: "' . $form_state['values']['comment'] . '"' : ''),
      'type' => 3,
      'access' => 1,
      'created' => REQUEST_TIME,
    ))->save();
  }

  }
}

function p1_deal_upload_update_deal_status($deal_id) {
  $deal = p1_deal_load($deal_id);
  $conditions = p1_helper_get_docs($deal, $deal->status);

  if (count($conditions) > 0) {

    $query = db_select('p1_deal_files', 'df');
    $query->leftJoin('p1_deal_status', 's', 's.fid = df.id');
    $query = $query->fields('df', array('doc_id'));
    $query = $query->condition('df.did', $deal->id);
    $query = $query->condition('s.file_status', 1);
    $result = $query->execute()->fetchAll();

    foreach ($result as $key => $value) {
       unset($conditions[$value->doc_id]);
    }

    if (count($conditions) == 0) {
      // All conditions are meet lets update the deal status
      $deal->status = $deal->status + 1;
      $deal->save();
    }

  }

}
