(function ($) {
    Drupal.behaviors.deal_edit = {
        attach : function (context, settings) {
            $('#edit-loan-type').change(function () {
                if ($('#edit-loan-type').val()) {
                    $('#card-2').removeClass('hidden');
                    $('#card-3').removeClass('hidden')
                }
                else {
                    $('#card-2').addClass('hidden');
                    $('#card-3').addClass('hidden')
                }
            });
            $('#edit-property-type-id').change(function () {
                var property = $(this).val();
                if (!$(this).hasClass('ajax-' + property)) {
                    var url = '/deal/property/'+property;
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: {},
                        success: function(data){
                            var program = $("#edit-program-type-id");
                            var amortization = $("#edit-amortization");
                            var terms = $("#edit-terms");
                            var penalty = $("#edit-pre-payment-penalty");
                            var doc = $("#edit-doc-id");
                            var pick_text = "- Pick one -";
                            program.empty();
                            terms.empty();
                            amortization.empty();
                            penalty.empty();
                            doc.empty();
                            program.append($("<option></option>")
                                .attr("value", "").text(pick_text));
                            terms.append($("<option></option>")
                                .attr("value", "").text(pick_text));
                            penalty.append($("<option></option>")
                                .attr("value", "").text(pick_text));
                            doc.append($("<option></option>")
                                .attr("value", "").text(pick_text));
                            $.each(data.program_type, function(key,value) {
                                program.append($("<option></option>")
                                    .attr("value", key).text(value));
                                if (Object.keys(data.program_type).length == 1) {
                                    program.val(key);
                                }
                            });
                            $.each(data.amortization, function(key,value) {
                                amortization.append($("<option></option>")
                                    .attr("value", key).text(value));
                                if (Object.keys(data.amortization).length == 1) {
                                    amortization.val(key);
                                }
                            });
                            $.each(data.terms, function(key,value) {
                                terms.append($("<option></option>")
                                    .attr("value", key).text(value));
                                if (Object.keys(data.terms).length == 1) {
                                    terms.val(key);
                                }
                            });
                            $.each(data.penalty, function(key,value) {
                                penalty.append($("<option></option>")
                                    .attr("value", key).text(value));
                                if (Object.keys(data.penalty).length == 1) {
                                    penalty.val(key);
                                }
                            });
                            $.each(data.doc, function(key,value) {
                                doc.append($("<option></option>")
                                    .attr("value", key).text(value));
                                if (Object.keys(data.doc).length == 1) {
                                    doc.val(key);
                                }
                            });
                        }
                    });
                    $(this).removeClass (function (index, css) {
                        return (css.match (/(^|\s)ajax-\d+/g) || []).join(' ');
                    });
                    $(this).addClass('ajax-' + property)
                }
            });
        }
    };
})(jQuery);
