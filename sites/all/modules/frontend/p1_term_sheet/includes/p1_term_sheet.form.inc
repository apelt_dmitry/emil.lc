<?php

/**
 * Form builder.
 */
function p1_term_sheet_form($form, &$form_state, $id_deal, $update = NULL) {

  $output = '';
  $output .= '<div class="sigPad" id="smoothed">';
  // $output .= '<p class="clearButton btn btn-warning text-right">Clear</p>';
  $output .= '<div class="sig sigWrapper current" style="width: 325px; height: 100%; display: block;">';
  $output .= '<div class="typed" style="display: none;"></div>';
  $output .= '<canvas class="pad" id="signature" width="320px"></canvas>';

  $output2 = '';
  $output2 .= '<div class="sigPad" id="co-smoothed">';
  // $output .= '<p class="clearButton btn btn-warning text-right">Clear</p>';
  $output2 .= '<div class="sig sigWrapper current" style="width: 325px; height: 100%; display: block;">';
  $output2 .= '<div class="typed" style="display: none;"></div>';
  $output2 .= '<canvas class="pad" id="co-signature" width="320px"></canvas>';

  drupal_add_js(drupal_get_path('module', 'p1_term_sheet') . '/js/jquery.signaturepad.min.js');
  drupal_add_js(drupal_get_path('module', 'p1_term_sheet') . '/js/json2.min.js');
  drupal_add_js(drupal_get_path('module', 'p1_term_sheet') . '/js/signature-init.js');

  // load single term_sheet entity
  $term_sheet = entity_load('p1_term_sheet', FALSE, ['did' => $id_deal]);
  if ($term_sheet) {
    $term_sheet = array_pop($term_sheet);
    $form_state['term_sheet'] = $term_sheet;
    $flag = TRUE;
  }
  else {
    $flag = FALSE;
  }

  $editable = p1_term_sheet_access_callback('edit', $id_deal);
  $signable = p1_term_sheet_access_callback('sign', $id_deal);

  $form_state['did'] = $id_deal;

  $form['#attached']['js'] = array(
    drupal_get_path('module', 'p1_helper') . '/js/msa_zipcode.js' => array(
      'type' => 'file',
    ),
  );

  /**
   *  Markup for HTML wrapped form
   */

  $deal = entity_load_single('p1_deal', $id_deal);
  $data = unserialize($deal->hidden);
  $selection = $data[arg(3)];
  $lender = user_load($deal->uid);
  $lender_user = entity_load_single('p1_user_registration', $deal->uid);
  $lending_program = entity_load_single('p1_lending_program', $selection['id']);
  $loan_type = p1_helper_get_features_type_by_type('Loan type');
  $property_types = p1_helper_get_property_types();
  $penaltys = p1_helper_available_prepayments();

  $form['saved_broker_origination_fee'] = array(
    '#markup' => (isset($term_sheet->broker_origination_fee) && $term_sheet->broker_origination_fee > 0) ? $term_sheet->broker_origination_fee.'%' : 'PAR (No Origination Fee)',
  );

  $form['saved_lender_origination_fee'] = array(
    '#markup' => (isset($term_sheet->lender_origination_fee) && $term_sheet->lender_origination_fee > 0) ? $term_sheet->lender_origination_fee.'%' : 'PAR (No Origination Fee)',
  );

  $form['lender_name'] = array(
    '#markup' => $lender_user->first_name .' '. $lender_user->last_name,
  );

  $form['lender_phone'] = array(
    '#markup' => p1_helper_phone_formatter($lender_user->telefone),
  );

  $form['lender_mail'] = array(
    '#markup' => $lender->mail,
  );

  $form['address'] = array(
    '#markup' => $deal->address .' '. $deal->address_2 .' '. $deal->city .', '. $deal->state .' '.$deal->zipcode,
  );

  $form['loan_type'] = array(
    '#markup' => $loan_type[$deal->loan_type],
  );

  $form['property_type'] = array(
    '#markup' => $property_types[$deal->property_type_id],
  );

  $form['pre_payment_penalty'] = array(
    '#markup' => $penaltys[$deal->pre_payment_penalty],
  );

  $form['loan_program'] = array(
    '#markup' => $selection['program'],
  );

  $form['min_fico'] = array(
    '#markup' => $selection['min_fico'],
  );

  $form['rate'] = array(
    '#markup' => $selection['rate'],
  );

  $form['loan_amount'] = array(
    '#markup' => p1_helper_currency($deal->loan_amount),
  );

  $form['appraised_value'] = array(
    '#markup' => p1_helper_currency($deal->appraised_value),
  );

  $form['amortization'] = array(
    '#markup' => p1_helper_currency($selection['p_i']),
  );

  $form['ltv'] = array(
    '#markup' => $deal->loan_to_value+0,
  );

  $form['years'] = array(
    '#markup' => $selection['amortization'],
  );

  $form['terms'] = array(
    '#markup' => $selection['terms'],
  );

  $form['underwriting_fees'] = array(
    '#markup' => p1_helper_currency($lending_program->underwriting_fees),
  );

  $form['processing_fee'] = array(
    '#markup' => p1_helper_currency($lending_program->processing_fee),
  );

  $form['margin'] = array(
    '#markup' => $lending_program->margin+0,
  );

  $form['max_life_adjustment'] = array(
    '#markup' => $lending_program->max_life_adjustment+0,
  );

  $form['max_first_adjustment'] = array(
    '#markup' => $lending_program->max_first_adjustment+0,
  );

  $form['max_subsequent_adjustment'] = array(
    '#markup' => $lending_program->max_subsequent_adjustment+0,
  );

  $form['appraisal_fee'] = array(
    '#markup' => p1_helper_currency($lending_program->appraisal_fee),
  );





  /**
   *  Markup for HTML wrapped form
   */

  $form['borrower_entity'] = array(
    '#title' => 'Borrowing Entity',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_entity) ? $term_sheet->borrower_entity : '',
    '#required' => TRUE,
    '#disable' => (!$editable),
  );
  $form['borrower_first_name'] = array(
    '#title' => 'First Name',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_first_name) ? $term_sheet->borrower_first_name : '',
    '#required' => TRUE,
    '#disable' => (!$editable),
  );
  $form['borrower_last_name'] = array(
    '#title' => 'Last Name',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_last_name) ? $term_sheet->borrower_last_name : '',
    '#required' => TRUE,
    '#disable' => (!$editable),
  );
  $form['borrower_email'] = array(
    '#title' => 'Email',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_email) ? $term_sheet->borrower_email : '',
    '#disable' => (!$editable),
  );
  $form['borrower_telephone'] = array(
    '#title' => 'Phone',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_telephone) ? $term_sheet->borrower_telephone : '',
    '#disable' => (!$editable),
  );
  $form['borrower_address'] = array(
    '#title' => 'Address',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_address) ? $term_sheet->borrower_address : '',
    '#disable' => (!$editable),
  );
  $form['borrower_address_2'] = array(
    '#title' => 'Address 2',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_address_2) ? $term_sheet->borrower_address_2 : '',
    '#disable' => (!$editable),
  );
  $form['borrower_city'] = array(
    '#title' => 'City',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_city) ? $term_sheet->borrower_city : '',
    '#attributes' => array('class' => array('msa-city')),
    '#disable' => (!$editable),
  );
  $form['borrower_state'] = array(
    '#title' => t('State'),
    '#type' => 'select',
    '#options' => p1_helper_get_states(),
    '#default_value' => isset($term_sheet->borrower_state) ? $term_sheet->borrower_state : NULL,
    '#attributes' => array('class' => array('msa-state')),
    '#disable' => (!$editable),
  );
  $form['borrower_zipcode'] = array(
    '#title' => t('Zip code'),
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->borrower_zipcode) ? $term_sheet->borrower_zipcode : NULL,
    '#maxlength' => 10,
    '#attributes' => array('class' => array('msa-zipcode')),
    '#disable' => (!$editable),
  );
  $form['borrower_signature'] = array(
    '#type' => 'hidden',
    '#attributes' => array('class' => 'output', 'value' => isset($term_sheet->borrower_signature) ? $term_sheet->borrower_signature : NULL),
    '#disable' => (!$signable),
  );
  $form['co_borrower_entity'] = array(
    '#title' => 'Borrowing Entity',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_entity) ? $term_sheet->co_borrower_entity : '',
    '#disable' => (!$editable),
  );
  $form['co_borrower_first_name'] = array(
    '#title' => 'First Name',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_first_name) ? $term_sheet->co_borrower_first_name : '',
    '#disable' => (!$editable),
  );
  $form['co_borrower_last_name'] = array(
    '#title' => 'Last Name',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_last_name) ? $term_sheet->co_borrower_last_name : '',
    '#disable' => (!$editable),
  );
  $form['co_borrower_email'] = array(
    '#title' => 'Email',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_email) ? $term_sheet->co_borrower_email : '',
    '#disable' => (!$editable),
  );
  $form['co_borrower_telephone'] = array(
    '#title' => 'Phone',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_telephone) ? $term_sheet->co_borrower_telephone : '',
    '#disable' => (!$editable),
  );
  $form['co_borrower_address'] = array(
    '#title' => 'Address',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_address) ? $term_sheet->co_borrower_address : '',
    '#disable' => (!$editable),
  );
  $form['co_borrower_address_2'] = array(
    '#title' => 'Address 2',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_address_2) ? $term_sheet->co_borrower_address_2 : '',
    '#disable' => (!$editable),
  );
  $form['co_borrower_city'] = array(
    '#title' => 'City',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_city) ? $term_sheet->co_borrower_city : '',
    '#attributes' => array('class' => array('msa-city-2')),
    '#disable' => (!$editable),
  );
  $form['co_borrower_state'] = array(
    '#title' => t('State'),
    '#type' => 'select',
    '#options' => p1_helper_get_states(),
    '#default_value' => isset($term_sheet->co_borrower_state) ? $term_sheet->co_borrower_state : NULL,
    '#attributes' => array('class' => array('msa-state-2')),
    '#disable' => (!$editable),
  );
  $form['co_borrower_zipcode'] = array(
    '#title' => t('Zip code'),
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->co_borrower_zipcode) ? $term_sheet->co_borrower_zipcode : NULL,
    '#maxlength' => 10,
    '#attributes' => array('class' => array('msa-zipcode-2')),
    '#disable' => (!$editable),
  );
  $form['co_borrower_signature'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($term_sheet->co_borrower_signature) ? $term_sheet->co_borrower_signature : '[{}]',
    '#attributes' => array('class' => 'output', 'value' => isset($term_sheet->co_borrower_signature) ? $term_sheet->co_borrower_signature : NULL),
    '#disable' => (!$signable),
  );
  $form['broker_entity'] = array(
    '#title' => 'Broker Entity',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->broker_entity) ? $term_sheet->broker_entity : '',
    '#required' => TRUE,
    '#disable' => (!$editable),
  );
  $form['broker_first_name'] = array(
    '#title' => 'First Name',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->broker_first_name) ? $term_sheet->broker_first_name : '',
    '#required' => TRUE,
    '#disable' => (!$editable),
  );
  $form['broker_last_name'] = array(
    '#title' => 'Last Name',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->broker_last_name) ? $term_sheet->broker_last_name : '',
    '#required' => TRUE,
    '#disable' => (!$editable),
  );
  $form['broker_email'] = array(
    '#title' => 'Broker Email',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->broker_email) ? $term_sheet->broker_email : '',
    '#disable' => (!$editable),
  );
  $form['broker_telephone'] = array(
    '#title' => 'Broker Phone',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->broker_telephone) ? $term_sheet->broker_telephone : '',
    '#disable' => (!$editable),
  );

  $form['broker_origination_fee'] = array(
    '#title' => 'Broker Origination Fee',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->broker_origination_fee) ? $term_sheet->broker_origination_fee : '',
    '#disable' => (!$editable),
  );

  $form['lender_origination_fee'] = array(
    '#title' => 'Lender Origination Fee',
    '#type' => 'textfield',
    '#default_value' => isset($term_sheet->lender_origination_fee) ? $term_sheet->lender_origination_fee : '',
    '#disable' => (!$editable),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );


  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Data',
    '#attributes' => array('class' => array('btn-lg')),
  );
  $form['#validate'][] = 'p1_term_sheet_validate';

  if ($flag) {
    $form['view'] = [
      '#type' => 'submit',
      '#value' => 'View Term Sheet',
      '#attributes' => array('class' => array('btn-lg btn-info')),
      '#submit' => ['p1_term_sheet_view_sheet_submit'],
    ];

    if (module_exists('p1_term_sheet_pdf')) {

      $form['pdf'] = [
        '#type' => 'submit',
        '#value' => 'View Term Sheet as PDF',
        '#attributes' => array('class' => array('btn-lg btn-info')),
        '#submit' => ['p1_term_sheet_view_sheet_pdf_submit'],
      ];
    }

    $form['email'] = [
      '#type' => 'button',
      '#value' => 'E-Mail Term Sheet',
      '#attributes' => array('class' => array('btn-lg btn-primary')),
    ];
  }
  return $form;
}

/**
 * Implements hook_validate_term_sheet_form().
 */
function p1_term_sheet_validate(&$form, &$form_state) {
  $errors = 0;
  //validate email
  $bor_email = trim($form_state['values']['borrower_email']);
  if ($bor_email != '') {
    if ($error = user_validate_mail($bor_email)) {
      form_set_error('borrower_email', $error);
      $errors++;
    }
  }
  $co_bor_email = trim($form_state['values']['co_borrower_email']);
  if ($co_bor_email != '') {
    if ($error = user_validate_mail($co_bor_email)) {
      form_set_error('co_borrower_email', $error);
      $errors++;
    }
  }
  $broker_email = trim($form_state['values']['broker_email']);
  if ($error = user_validate_mail($broker_email)) {
    form_set_error('broker_email', $error);
    $errors++;
  }
  //validate first_name
  $bor_first_name = trim($form_state['values']['borrower_first_name']);
  if ($bor_first_name != '') {
    if ($error = user_validate_name($bor_first_name)) {
      form_set_error('borrower_first_name', $error);
      $errors++;
    }
  }
  $co_bor_first_name = trim($form_state['values']['co_borrower_first_name']);
  if ($co_bor_first_name != '') {
    if ($error = user_validate_name($co_bor_first_name)) {
      form_set_error('co_borrower_first_name', $error);
      $errors++;
    }
  }
  //validate last_name
  $bor_last_name = trim($form_state['values']['borrower_last_name']);

  if ($bor_last_name != '') {
    if ($error = user_validate_name($bor_last_name)) {
      form_set_error('borrower_last_name', $error);
      $errors++;
    }
  }

  $co_bor_last_name = trim($form_state['values']['co_borrower_last_name']);
  if ($co_bor_last_name != '') {
    if ($error = user_validate_name($co_bor_last_name)) {
      form_set_error('co_borrower_last_name', $error);
      $errors++;
    }
  }

  //validate telephone
  $bor_telephone = trim($form_state['values']['borrower_telephone']);
  if ($bor_telephone != '' && !preg_match('#\d{10}#', $bor_telephone)) {
    form_set_error('borrower_telephone', 'Field Telephone is not valid. Example of correct filling: 0001112222');
    $errors++;
  }
  $co_bor_telephone = trim($form_state['values']['co_borrower_telephone']);
  if ($co_bor_telephone != '' && !preg_match('#\d{10}#', $bor_telephone)) {
    form_set_error('co_borrower_telephone', 'Field Telephone is not valid. Example of correct filling: 0001112222');
    $errors++;
  }
  $broker_telephone = trim($form_state['values']['broker_telephone']);
  if ($broker_telephone != '' && !preg_match('#\d{10}#', $broker_telephone)) {
    form_set_error('broker_telephone', 'Field Telephone is not valid. Example of correct filling: 0001112222');
    $errors++;
  }

  //validate zipcode
  $bor_zipcode = trim($form_state['values']['borrower_zipcode']);
  if ($bor_zipcode != '') {
    if (!preg_match('/(^\d*$)/', $bor_zipcode)) {
      form_set_error('borrower_zipcode', t('Zipcode must be in format 12345.'));
      $errors++;
    }
  }
  $co_bor_zipcode = trim($form_state['values']['co_borrower_zipcode']);
  if ($co_bor_zipcode != '') {
    if (!preg_match('/(^\d*$)/', $co_bor_zipcode)) {
      form_set_error('co_borrower_zipcode', t('Zipcode must be in format 12345.'));
      $errors++;
    }
  }

  if ($errors) {
    $form_state['values']['borrower_signature'] = '';
    $form_state['input']['borrower_signature'] = '';
    $form['borrower_signature']['#value'] = '';
    $form_state['values']['co_borrower_signature'] = '';
    $form_state['input']['co_borrower']['signature'] = '';
    $form['co_borrower_signature']['#value'] = '';
  }
}

/**
 * Form submit callback.
 */
function p1_term_sheet_form_submit($form, &$form_state) {
  global $user;
  $entity_type = 'p1_term_sheet';
  $values = array();
  if (isset($form_state['term_sheet'])) {
    $term_sheet = $form_state['term_sheet'];
    $term_sheet->borrower_entity = $form_state['values']['borrower_entity'];
    $term_sheet->borrower_first_name = $form_state['values']['borrower_first_name'];
    $term_sheet->borrower_last_name = $form_state['values']['borrower_last_name'];
    $term_sheet->borrower_address = $form_state['values']['borrower_address'];
    $term_sheet->borrower_address_2 = $form_state['values']['borrower_address_2'];
    $term_sheet->borrower_city = $form_state['values']['borrower_city'];
    $term_sheet->borrower_state = $form_state['values']['borrower_state'];
    $term_sheet->borrower_zipcode = $form_state['values']['borrower_zipcode'];
    $term_sheet->borrower_telephone = $form_state['values']['borrower_telephone'] ? $form_state['values']['borrower_telephone'] : NULL;
    $term_sheet->borrower_email = $form_state['values']['borrower_email'] ? $form_state['values']['borrower_email'] : NULL;
    $term_sheet->borrower_signature = $form_state['values']['borrower_signature'];

    $term_sheet->co_borrower_entity = $form_state['values']['co_borrower_entity'];
    $term_sheet->co_borrower_first_name = $form_state['values']['co_borrower_first_name'];
    $term_sheet->co_borrower_last_name = $form_state['values']['co_borrower_last_name'];
    $term_sheet->co_borrower_address = $form_state['values']['co_borrower_address'];
    $term_sheet->co_borrower_address_2 = $form_state['values']['co_borrower_address_2'];
    $term_sheet->co_borrower_city = $form_state['values']['co_borrower_city'];
    $term_sheet->co_borrower_state = $form_state['values']['co_borrower_state'];
    $term_sheet->co_borrower_zipcode = $form_state['values']['co_borrower_zipcode'];
    $term_sheet->co_borrower_telephone = $form_state['values']['co_borrower_telephone'] ? $form_state['values']['co_borrower_telephone'] : NULL;
    $term_sheet->co_borrower_email = $form_state['values']['co_borrower_email'] ? $form_state['values']['co_borrower_email'] : NULL;
    $term_sheet->co_borrower_signature = $form_state['values']['co_borrower_signature'];

    $term_sheet->broker_entity = $form_state['values']['broker_entity'];
    $term_sheet->broker_first_name = $form_state['values']['broker_first_name'];
    $term_sheet->broker_last_name = $form_state['values']['broker_last_name'];
    $term_sheet->broker_telephone = $form_state['values']['broker_telephone'] ? $form_state['values']['broker_telephone'] : NULL;
    $term_sheet->broker_email = $form_state['values']['broker_email'] ? $form_state['values']['broker_email'] : NULL;

    $term_sheet->broker_origination_fee = $form_state['values']['broker_origination_fee'] ? $form_state['values']['broker_origination_fee'] : 0;
    $term_sheet->lender_origination_fee = $form_state['values']['lender_origination_fee'] ? $form_state['values']['lender_origination_fee'] : 0;

    $term_sheet->save();
  }
  else {
    $values['uid'] = $user->uid;
    $values['did'] = $form_state['did'];

    $values['borrower_entity'] = $form_state['values']['borrower_entity'];
    $values['borrower_first_name'] = $form_state['values']['borrower_first_name'];
    $values['borrower_last_name'] = $form_state['values']['borrower_last_name'];
    $values['borrower_address'] = $form_state['values']['borrower_address'];
    $values['borrower_address_2'] = $form_state['values']['borrower_address_2'];
    $values['borrower_city'] = $form_state['values']['borrower_city'];
    $values['borrower_state'] = $form_state['values']['borrower_state'];
    $values['borrower_zipcode'] = $form_state['values']['borrower_zipcode'];
    $values['borrower_telephone'] = $form_state['values']['borrower_telephone'] ? $form_state['values']['borrower_telephone'] : NULL;
    $values['borrower_email'] = $form_state['values']['borrower_email'] ? $form_state['values']['borrower_email'] : NULL;
    $values['borrower_signature'] = $form_state['values']['borrower_signature'];

    $values['co_borrower_entity'] = $form_state['values']['co_borrower_entity'];
    $values['co_borrower_first_name'] = $form_state['values']['co_borrower_first_name'];
    $values['co_borrower_last_name'] = $form_state['values']['co_borrower_last_name'];
    $values['co_borrower_address'] = $form_state['values']['co_borrower_address'];
    $values['co_borrower_address_2'] = $form_state['values']['co_borrower_address_2'];
    $values['co_borrower_city'] = $form_state['values']['co_borrower_city'];
    $values['co_borrower_state'] = $form_state['values']['co_borrower_state'];
    $values['co_borrower_zipcode'] = $form_state['values']['co_borrower_zipcode'];
    $values['co_borrower_telephone'] = $form_state['values']['co_borrower_telephone'] ? $form_state['values']['co_borrower_telephone'] : NULL;
    $values['co_borrower_email'] = $form_state['values']['co_borrower_email'] ? $form_state['values']['co_borrower_email'] : NULL;
    $values['co_borrower_signature'] = $form_state['values']['co_borrower_signature'];

    $values['broker_entity'] = $form_state['values']['broker_entity'];
    $values['broker_first_name'] = $form_state['values']['broker_first_name'];
    $values['broker_last_name'] = $form_state['values']['broker_last_name'];
    $values['broker_telephone'] = $form_state['values']['broker_telephone'] ? $form_state['values']['broker_telephone'] : NULL;
    $values['broker_email'] = $form_state['values']['broker_email'] ? $form_state['values']['broker_email'] : NULL;

    $values['broker_origination_fee'] = $form_state['values']['broker_origination_fee'] ? $form_state['values']['broker_origination_fee'] : 0;
    $values['lender_origination_fee'] = $form_state['values']['lender_origination_fee'] ? $form_state['values']['lender_origination_fee'] : 0;

    $values['created'] = REQUEST_TIME;

    $entity = entity_create($entity_type, $values);
    $entity->save();
  }

  drupal_set_message('Term Sheet saved');
}

function p1_term_sheet_view_sheet_submit($form, &$form_state) {
  drupal_goto('/deal/' . $form_state['did'] . '/term/'.arg(3).'/view');
}

function p1_term_sheet_view_sheet_pdf_submit($form, &$form_state) {
    drupal_goto('/deal/' . $form_state['did'] . '/term/'.arg(3).'/pdf');
}

function p1_term_sheet_view($deal_id) {
  $deal = entity_load_single('p1_deal', $deal_id);
  $term_sheet = entity_load('p1_term_sheet', FALSE, ['did' => $deal_id]);
  $term_sheet = array_shift($term_sheet);

  $lender = user_load($deal->uid);
  $registration = entity_load_single('p1_user_registration', $deal->uid);

  $loan_type = p1_helper_get_features_type_by_type('Loan type');
  $property_types = p1_helper_get_property_types();


  $data = unserialize($deal->hidden);
  $selection = $data[arg(3)];
  $lending_program = entity_load_single('p1_lending_program', $selection['id']);
  $penaltys = p1_helper_available_prepayments();


  $conditions = array();
  for ($i=0; $i<5; $i++) {
    $conditions = array_merge($conditions, p1_helper_get_docs($deal, $i));
  }

    $variables = array();

  // Add all term sheet values
  foreach ($term_sheet as $key => $value) {
    $variables[$key] = $value;
  }

  // Add all registration values
  foreach ($lender as $lkey => $lvalue) {
    if (in_array($lkey, array('name', 'mail'))) {
      $variables['lender_'.$lkey] = $lvalue;
    }
  }

  foreach ($registration as $rkey => $rvalue) {
    $variables['lender_'.$rkey] = $rvalue;
  }

  $variables['address'] = $deal->address .' '. $deal->address_2 .' '. $deal->city .', '. $deal->state .' '.$deal->zipcode;
  $variables['loan_type'] = $loan_type[$deal->loan_type];
  $variables['property_type'] = $property_types[$deal->property_type_id];
  $variables['pre_payment_penalty'] = $penaltys[$deal->pre_payment_penalty];
  $variables['loan_program'] = $selection['program'];
  $variables['min_fico'] = $selection['min_fico'];
  $variables['loan_amount'] = p1_helper_currency($deal->loan_amount);
  $variables['appraised_value'] = p1_helper_currency($deal->appraised_value);
  $variables['amortization'] = p1_helper_currency($selection['p_i']);
  $variables['ltv'] = $deal->loan_to_value+0;
  $variables['years'] = $selection['amortization'];
  $variables['terms'] = $selection['terms'];
  $variables['underwriting_fees'] = p1_helper_currency($lending_program->underwriting_fees);
  $variables['processing_fee'] = p1_helper_currency($lending_program->processing_fee);
  $variables['margin'] = $lending_program->margin+0;
  $variables['max_life_adjustment'] = $lending_program->max_life_adjustment+0;
  $variables['max_first_adjustment'] = $lending_program->max_first_adjustment+0;
  $variables['max_subsequent_adjustment'] = $lending_program->max_subsequent_adjustment+0;

  $variables['address'] = $deal->address;
  $variables['address_2'] = $deal->address_2;
  $variables['city'] = $deal->city;
  $variables['state'] = $deal->state;
  $variables['zipcode'] = $deal->zipcode;

  $variables['loan_type'] = $loan_type[$deal->loan_type];
  $variables['property_type'] = $property_types[$deal->property_type_id];

  $variables['conditions'] = $conditions;

  return theme('term_sheet', $variables);
}

function p1_term_sheet_theme($existing, $type, $theme, $path) {
    return array(
        'term_sheet' => array(
            'variables' => array(
                'nid' => NULL,
                'title' => NULL
            ),
            'template' => 'termsheet',
            'path' => drupal_get_path('theme', 'p1') . '/templates/deal',
        )
    );
}

