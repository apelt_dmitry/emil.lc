(function ($) {
    $(document).ready(function() {
        $('html').removeClass('no-js');
        var sig = $('#smoothed .output').attr('value');
        var cosig = $('#smoothed1 .output').attr('value');
        var pad;
        var copad;
        if (!sig) {
            pad = $('#smoothed').signaturePad({
                drawOnly: true,
                drawBezierCurves: true,
                variableStrokeWidth: true,
                lineTop: 200,
                validateFields: true,
            });
        }
        else {
            pad = $('#smoothed').signaturePad({
                drawOnly: true,
                drawBezierCurves: true,
                variableStrokeWidth: true,
                lineTop: 200,
                validateFields: true,
            }).regenerate(sig);
        }
        if (!cosig) {
            copad = $('#smoothed1').signaturePad({
                drawOnly: true,
                drawBezierCurves: true,
                variableStrokeWidth: true,
                lineTop: 200,
                validateFields: true,
            });
        }
        else {
            copad = $('#smoothed1').signaturePad({
                drawOnly: true,
                drawBezierCurves: true,
                variableStrokeWidth: true,
                lineTop: 200,
                validateFields: true,
            }).regenerate(cosig);
        }
        $('#clearButton').click(function(){
            pad.clearCanvas();
            return false;
        });

        $('#co-clearButton').click(function(){
            copad.clearCanvas();
            return false;
        });
    });
}(jQuery));
