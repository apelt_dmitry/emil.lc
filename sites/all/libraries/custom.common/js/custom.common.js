(function ($) {
  Drupal.behaviors.customcommon = {
    attach : function (context, settings) {

    // Variables
    var percentage = 0;
    var price = 0;
    var appraised = 0;
    var event_action = ("ontouchend" in document.documentElement ? "touchend" : "click");

    // Masks
    $('.date').mask('00/00/0000', {placeholder: "mm/dd/yyyy"});
    $('.phone').mask('(000) 000-0000', {placeholder: "(000) 000-0000"});
    $('.currency').mask("#,##0.00", {placeholder: "0.00", reverse: true});

    $("#edit-property-income, #edit-other-income").on("blur",function() {
        var income   = parseFloat($("#edit-property-income").val().replace(/[^\d\.\-]/g, ""));
        var extra    = parseFloat($("#edit-other-income").val().replace(/[^\d\.\-]/g, ""));
        var expenses   = parseFloat($("#edit-expenses").val().replace(/[^\d\.\-]/g, ""));
        var vacancy    = parseFloat($("#edit-vacancy-factor").val().replace(/[^\d\.\-]/g, ""));

        if ($("#edit-property-type-id").find("option:selected").text() == 'SFR') {
          var property_type   = 'R';
        } else {
          var property_type   = 'C';
        }

        if (property_type == "C") {
            vacancy         = 5;
            expenses        = ((income+0) + (extra+0))*((35 + vacancy)/100);
        } else {
            vacancy         = 5;
            expenses        = ((income+0) + (extra+0))*((20 + vacancy)/100);
        }

        if (!isNaN(vacancy)) $("#edit-vacancy-factor").val(Math.round(vacancy));
        if (!isNaN(expenses)) $("#edit-expenses").val(expenses.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());

    });

    $("#edit-property-income, #edit-other-income").each(function() {
        $(this).trigger("blur");
    });

    $("#edit-loan-type, #edit-down-payment-percentage, #edit-down-payment-amount, #edit-purchase-price, #edit-appraised-value, #edit-loan-amount").on("blur",function() {

        if ($("#edit-loan-type").find("option:selected").text() == 'Purchase') {
          var loan_type   = 'P';
        } else {
          var loan_type   = 'R';
        }


        if ($("#edit-down-payment-percentage").length) {
            var down_perc   = parseFloat($("#edit-down-payment-percentage").val().replace(/[^\d\.\-]/g, ""));
        }
        if ($("#edit-down-payment-amount").length) {
            var down_amt = parseFloat($("#edit-down-payment-amount").val().replace(/[^\d\.\-]/g, ""));
        }
        if ($("#edit-purchase-price").length) {
            var price       = parseFloat($("#edit-purchase-price").val().replace(/[^\d\.\-]/g, ""));
        }
        if ($("#edit-appraised-value").length) {
            var appraised = parseFloat($("#edit-appraised-value").val().replace(/[^\d\.\-]/g, ""));
        }
        var loan_amount = parseFloat($("#edit-loan-amount").val().replace(/[^\d\.\-]/g, ""));
        var ltv = parseFloat($("#edit-loan-to-value").val().replace(/[^\d\.\-]/g, ""));

        // Down Payment (%)
        if ($(this).attr("id") == "edit-down-payment-percentage" || $(this).attr("id") == "edit-purchase-price") {
            if (loan_type == "P" && price > 0) {
                down_amt        = price * down_perc / 100;
                loan_amount     = price - down_amt;
            } else if (appraised > 0 && $(this).attr("id") == "edit-down-payment-percentage") {
                down_amt        = appraised * down_perc / 100;
            }
        }

        // Loan Amount
        if ($(this).attr("id") == "edit-loan-amount") {
            if (loan_type == "P" && price > 0) {
                down_amt        = price - loan_amount;
                down_perc       = down_amt / price * 100;
            } else if (loan_type == "P" && appraised > 0) {
                down_amt        = appraised - loan_amount;
                down_perc       = down_amt / appraised * 100;
            }

        }

        // Purchase Price
        if ($(this).attr("id") == "edit-purchase-price" && loan_type == "P") {
            loan_amount = price - down_amt;
            appraised = price;
        }

        // Down Payment ($)
        if ($(this).attr("id") == "edit-down-payment-amount") {
            if (loan_type == "P" && price > 0) {
                down_perc       = down_amt / price * 100;
                loan_amount     = price - down_amt;
            } else if (loan_type == "P" && appraised > 0) {
                down_perc       = down_amt / appraised * 100;
            }
        }

        // Calculate LTV
        if (loan_type == "P") {
            ltv         = ((price+0) ? (loan_amount/price*100) : 0);
        } else {
            loan_amount = appraised * 0.70;
            down_amt    = 0;
            down_perc   = 0;
            //$("#edit-purchase-price").val(down_amt.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            ltv         = ((appraised+0) ? (loan_amount/appraised*100) : 0);
        }

        if (!isNaN(appraised)) $("#edit-appraised-value").val(appraised.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
        if (!isNaN(loan_amount)) $("#edit-loan-amount").val(loan_amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
        if (!isNaN(down_amt)) $("#edit-down-payment-amount").val(down_amt.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
        if (!isNaN(down_perc)) $("#edit-down-payment-percentage").val(Math.round(down_perc));
        if (!isNaN(ltv)) $("#edit-loan-to-value").val(Math.round(ltv));
    });

    $("#edit-loan-type, #edit-down-payment-percentage, #edit-down-payment-amount, #edit-purchase-price, #edit-appraised-value, #edit-loan-to-value, #edit-loan-amount").each(function() {
        $(this).trigger("blur");
    });



    $("#edit-loan-type").on("change",function() {

        if ($(this).find("option:selected").text() == 'Purchase') {
          var loan_type   = 'P';
        } else {
          var loan_type   = 'R';
        }

        if (loan_type == "P") {
            $("#edit-appraised-value").attr("disabled","disabled");
            $("#edit-purchase-price").removeAttr("disabled");
            $("#edit-down-payment-percentage").removeAttr("disabled");
            $("#edit-down-payment-amount").removeAttr("disabled");
        } else {
            $("#edit-appraised-value").removeAttr("disabled");
            $("#edit-purchase-price").attr("disabled","disabled");
            $("#edit-down-payment-percentage").attr("disabled","disabled");
            $("#edit-down-payment-amount").attr("disabled","disabled");
        }

    });

    $("#edit-loan-type").trigger("change");


    $('#p1-deal-form .form-submit').on(event_action, function(event){
        $("input,select").removeAttr("disabled");
    });




    // Deal Validation form
    $('#p1-deal-form').formValidation({
        live: 'enabled',
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            loan_type: {
                validators: {
                    notEmpty: {
                        message: 'Loan type is required'
                    }
                }
            },
            fico_score: {
                validators: {
                    notEmpty: {
                        message: 'The FICO Score is required'
                    },
                    between: {
                        min: 649,
                        max: 900,
                        message: 'The FICO Score must be between 650 and 900'
                    }
                }
            },
            loan_to_value: {
                validators: {
                    notEmpty: {
                        message: 'The LTV is required'
                    },
                    between: {
                        min: 0,
                        max: 75,
                        message: 'The LTV must be between 0% and 70%'
                    }
                }
            },
            down_payment_percentage: {
                validators: {
                    notEmpty: {
                        message: 'The down payment percentage is required'
                    }
                },
                between: {
                    min: 1,
                    max: 100,
                    message: 'The down payment percentage must be between 1 and 100'
                }
            },
            property_type_id: {
                validators: {
                    notEmpty: {
                        message: 'The property type is required'
                    }
                }
            },
            address: {
                validators: {
                    notEmpty: {
                        message: 'The address is required'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'The city is required'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'The state is required'
                    }
                }
            },
            zipcode: {
                validators: {
                    notEmpty: {
                        message: 'The zipcode is required'
                    }
                }
            },
            property_income: {
                validators: {
                    notEmpty: {
                        message: 'The property income is required'
                    }
                }
            },
             other_income: {
                validators: {
                    notEmpty: {
                        message: 'The other income is required'
                    }
                }
            },
            program_type_id: {
                validators: {
                    notEmpty: {
                        message: 'The program type is required'
                    }
                }
            },
            terms: {
                validators: {
                    notEmpty: {
                        message: 'The terms is required'
                    }
                }
            },
            amortization: {
                validators: {
                    notEmpty: {
                        message: 'The amortization is required'
                    }
                }
            },
        }
    });







    }
  };
})(jQuery);
