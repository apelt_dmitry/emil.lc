
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <title>TERM SHEET</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <link rel="stylesheet" href="/css/nebo.css" type="text/css" />
  <style type="text/css">a,a:link,a:hover,a:active,a:visited { color:#0000FF; }p,td,th,b,span,a,div {font-family:arial,sans-serif;font-size:13px;line-height:18px;}</style>
</head>
<body style="color:#000;background:#fff;margin:0px;padding:0px;">
<div width="950" style="width:750px; margin:0px auto;">

<img src="/sites/all/themes/p1/images/logo_term_sheets_banner.png" width="750" />

<table width="750" style="width:100%;padding:0px;border-collapse:collapse;">
  <tr>
    <td colspan="2" align="right" style="text-align: right; font: Verdana 14px; padding-bottom: 10px; font-style: italic;">
      Financing commercial real estate since 1999
    </td>
  </tr>
  <tr>
    <td valign="top" width="50%">
      <b style="font-size:16px;">TERM SHEET</b><br /><br />
    </td>
    <td valign="top" align="right" width="50%" style="width:240px;text-align:right;">Date: 07/26/2016<br />Expires: EOD<br /><br /></td>
  </tr>
</table>


<table cellspacing="2" cellpadding="0" border="0" bgcolor="#cccccc" width="750" style="width:100%;padding:0px;border-collapse:collapse;border:1px solid #cccccc;">
  <tr bgcolor="#ffffff">
    <td valign="top" width="20%" style="padding:5px;border:1px solid #44a241;width:20%;text-align:right;"><b>Borrower</b></td>
    <td valign="top" width="30%" style="padding:5px;border:1px solid #44a241;width:30%;"></td>
    <td valign="top" width="20%" style="padding:5px;border:1px solid #44a241;width:20%;text-align:right;"><b>Broker</b></td>
    <td valign="top" width="30%" style="padding:5px;border:1px solid #44a241;width:30%;"></td>
  </tr>
  <tr bgcolor="#cccccc">
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#cccccc;text-align:right;"><b>Property Address</b></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#cccccc;"></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#cccccc;text-align:right;"><b>Executive</b></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#cccccc;"></td>
  </tr>
  <tr bgcolor="#ffffff">
    <td valign="top" style="padding:5px;border:1px solid #44a241;width:20%;text-align:right;"><b>Type of Property</b></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;width:30%;"></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;width:20%;text-align:right;"><b>Email</b></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;width:30%;"></td>
  </tr>
  <tr bgcolor="#cccccc">
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#cccccc;text-align:right;"><b>Purpose of Loan</b></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#cccccc;"></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#cccccc;text-align:right;"><b>Phone</b></td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#cccccc;"></td>
  </tr>
</table>

<br/>

<table cellspacing="2" cellpadding="0" border="0" bgcolor="#44a241" width="750" style="width:100%;padding:0px;border-collapse:collapse;border:1px solid #44a241;margin-bottom:20px;">
  <tr>
    <td align="center" bgcolor="#EAEAE0" valign="top" colspan="4" style="padding:5px;border:1px solid #44a241;text-align:center;">
      <b>Proposed Loan Terms</b>
    </td>
  </tr>
  <tr bgcolor="#d3e8d0">
    <td valign="top" style="padding:5px;border:1px solid #44a241;text-align:right;background:#81B859;">
      <b>Loan Program</b>
    </td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#d3e8d0;">

    </td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;text-align:right;background:#81B859;">
      <b>Mid FICO Score</b>
    </td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#d3e8d0;">

    </td>
  </tr>
  <tr bgcolor="#ffffff">
    <td valign="top" width="25%" style="padding:5px;border:1px solid #44a241;width:25%;text-align:right;background:#81B859;">
      <b>Proposed Loan Amount</b>
    </td>
    <td valign="top" width="25%" style="padding:5px;border:1px solid #44a241;width:25%;">

    </td>
    <td valign="top" width="21%" style="padding:5px;border:1px solid #44a241;width:21%;text-align:right;background:#81B859;">
      <b>***Star/Floor Rate</b>
    </td>
    <td valign="top" width="29%" style="padding:5px;border:1px solid #44a241;width:29%;">

    </td>
  </tr>

  <tr bgcolor="#d3e8d0">
    <td valign="top" style="padding:5px;border:1px solid #44a241;text-align:right;background:#81B859;">
      <b>Est. Property Value</b>
    </td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#d3e8d0;">

    </td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;text-align:right;background:#81B859;">
      <b>Estimated Mo. Payment</b>
    </td>
    <td valign="top" style="padding:5px;border:1px solid #44a241;background:#d3e8d0;">

    </td>
  </tr>
  <tr bgcolor="#ffffff">
    <td valign="top" width="25%" style="padding:5px;border:1px solid #44a241;width:25%;text-align:right;background:#81B859;">
      <b>Estimated LTV</b>
    </td>
    <td valign="top" width="25%" style="padding:5px;border:1px solid #44a241;width:25%;">

    </td>
    <td valign="top" width="21%" style="padding:5px;border:1px solid #44a241;width:21%;text-align:right;background:#81B859;">
      <b>Rate Fixed For</b>
    </td>
    <td valign="top" width="29%" style="padding:5px;border:1px solid #44a241;width:29%;">

    </td>
  </tr>

</table>


<br/>
<span>A preliminary review of your request for the financing of subject property has been completed.
Metwest Capital Group LLC (MCG or Lender) or its assignee is pleased to issue this “Term Sheet” as
an expression of its interest in considering a loan. This letter is not intended to constitute a
commitment or offer to invest on the part of MCG, but rather to summarize, for discussion
purposes, the loan that MCG is interested in considering at this time based upon
representations and other information provided by you. Outlined below is a summary of the
general terms and conditions for the subject loan. Since this is only a summary, it cannot be
construed as a commitment; summary is only for discussion purposes. <u>Nothing herein shall be
construed as a firm commitment to loan or invest. All agreements between the parties shall only
be in writing. No oral agreements shall be deemed to exist.</u></span>
<br/><br/>
<table width="750" style="width:100%;padding:0px;">
  <tr>
    <td valign="top" style="padding:5px;border:1px solid #44a241;text-align:center;" align="center" border = "1" bgcolor="#EAEAE0">
      <b>Preparation for the Processing</b>
    </td>
  </tr>
  <tr>
    <td valign="top" style="padding:5px;">
      Before we can open your loan file for processing, we need the following:
      <ul>
        <li>This Term Sheet signed and dated</li>
        <li>3rd Party Fee deposit</li>
        <li>Completed 1003</li>
        <li>Tri-Merge Credit report</li>
        <li>Signed purchase agreement (on purchases only)</li>
        <li>Information release authorization, signed and dated (incorporated into this Term Sheet)</li>
      </ul>

      <p>For a commercial/MF5+ property only, we need the following in addition to the above documents, before we can order the appraisal:</p>
      <ul>
        <li>Copies of all signed rental agreements / leases</li>
        <li>Dated rent roll (signed and dated by applicant/property owner)</li>
        <li>Operating statements for 2013, 2014 and YTD 2015 (signed and dated by applicant/property owner)</li>
        <li>Appraisal request form complete with contact information for property access</li>
        <li>Environmental disclosure (signed and dated by applicant)</li>
        <li>Structural disclosure (signed and dated by applicant)</li>
        <li>Capital Improvements disclosure (signed and dated by applicant)</li>
      </ul>
      <br/><br/>
      <p>Prior to completing our underwriting, we need the following:</p>
      <ul>
        <li>Full application package (MCG will provide you with a list of items we need to complete the loan file and/or complete our analysis of your loan request)</li>
        <li>Third-party reports engaged, received and reviewed by MCG</li>
      </ul>
    </td>
  </tr>
</table>
<br/>
<table width="750" style="width:100%;padding:0px;">
  <tr>
    <td valign="top" style="padding:5px;border:1px solid #44a241;text-align:center;" align="center" border = "1" bgcolor="#EAEAE0">
      <b>Information Release Authorization</b>
    </td>
  </tr>
  <tr>
    <td valign="top" style="padding:5px;">
      I/We hereby authorize the release of any and all information to Metwest Capital Group LLC for the
      purpose of evaluating our credit transaction. I/We authorize Metwest Capital Group LLC to release any
      such information to any entity deemed necessary for any purpose related to this credit transaction and we
      understand that Metwest Capital Group LLC will not release our information to any vendor or party of
      interest that is unrelated to this transaction. My/Our signature(s) on this form can be used and interpreted
      as my/our original signature(s) on any information request made by Metwest Capital Group LLC
      including pulling a full credit report reflecting FICO scores.
    </td>
  </tr>
</table>
<br/>
<table width="750" cellpadding="0" cellspacing="0" style="width:100%;padding:0px;border-collapse:collapse;">
  <tr>
    <td style="padding-right:5px;"></td>
  </tr>
  <tr>
    <td width="45%" bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td width="10%" bgcolor="#ffffff" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td width="45%" bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td valign="top" style="width:45%;vertical-align:top;font-size:10px;">Printed Name<br/><br/></td>
    <td style="width:10%">&nbsp;</td>
    <td valign="top" style="width:45%;vertical-align:top;font-size:10px;">Date</td>
  </tr>
  <tr>
    <td bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td bgcolor="#ffffff" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td bgcolor="#ffffff" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td valign="top" style="vertical-align:top;font-size:10px;">Signature<br/><br/></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td bgcolor="#ffffff" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td valign="top" style="vertical-align:top;font-size:10px;">Birthdate(day/month/yr)<br/><br/></td>
    <td>&nbsp;</td>
    <td valign="top" style="vertical-align:top;font-size:10px;">Social Security Number</td>
  </tr>
  <tr>
    <td colspan="3" bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td valign="top" colspan="3" style="vertical-align:top;font-size:10px;">Current Street Address<br/><br/></td>
  </tr>
  <tr>
    <td colspan="3" bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td valign="top" colspan="3" style="vertical-align:top;font-size:10px;">City, State, Zip<br/><br/></td>
  </tr>
</table>
<br/>
<table width="750" style="width:100%;padding:0px;">
  <tr>
    <td valign="top" style="padding:5px;border:1px solid #44a241;text-align:center;" align="center" border = "1" bgcolor="#EAEAE0">
      <b>Applicant Declarations</b>
    </td>
  </tr>
  <tr>
    <td valign="top" style="padding:5px;">
      <ol>
        <li>Certifying that all statements and documents in the loan application package are true, correct and complete; and,
that you agree to promptly notify MCG of any material change in such information;</li>
        <li>Acknowledging that all signature copies and faxes can be relied upon by MCG as if they were original signatures;</li>
        <li>Authorizing MCG to rely upon such statements, make inquiries, and gather information as deemed necessary and
reasonable for the purpose of verifying information provided to MCG (including pulling credit reports or requesting
tax transcripts); and, to exchange this information with business credit reporting or credit bureau agencies and
creditors of the undersigned;</li>
        <li>Authorizing MCG to share information with any federal, state, or other authority and/or lender for the purpose of
processing the loan application and funding the loan;</li>
        <li>Authorizing MCG to verify the borrower’s identity;</li>
        <li>Authorizing MCG to obtain all payoff, satisfaction/release information and/or paid in full documents.</li>
        <li>Certifying that any property and/or proceeds from the loan will be used for commercial purpose only and not for
any personal, family or household purposes, and that the proposed loan is considered a business loan which is
exempted from the disclosure requirements of Regulation Z - Truth in Lending Act. You agree to indemnify and
hold MCC harmless from any and all claims, loss or damage resulting or caused by the request being subject to
any of the provisions of the federal Consumer Credit Protection Act (Truth-in-Lending Act) and Regulation Z.</li>
        <li>Certifying that you have full authority to request the loan and sign all documents pertaining to the loan request
and/or loan funding.</li>
        <li>Acknowledging and agreeing that you and/or your representative(s) will not disclose any information contained in
this Term Sheet to any other party without consent from MCG.</li>
      </ol>
    </td>
  </tr>
</table>
<br/>
<span>The loan application process may be terminated by either party (MCG or the borrower) at any time during the process for any reason.</span>
<br/>
<span>Please submit the signed Term Sheet indicating your approval to the terms herein, accompanied by the
3rd Party Fee deposit before expiration of this Term Sheet (cited at the top of page 1). A copy of the
signed original of this Term Sheet shall be considered effective consent. Please contact your Metwest
representative for payment instructions.</span>
<br/>
<span>Respectfully,<br />Metwest Capital Group LLC</span>
<br /><br />
<table width="750" cellpadding="0" cellspacing="0" style="width:100%;padding:0px;border-collapse:collapse;">
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td width="45%" bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td width="10%" bgcolor="#ffffff" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td width="45%" bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td valign="top" style="width:45%;vertical-align:top;font-size:10px;">By: Isidoro Riguero (President)<br/><br/></td>
    <td style="width:10%">&nbsp;</td>
    <td valign="top" style="width:45%;vertical-align:top;font-size:10px;">Date</td>
  </tr>
  <tr>
    <td bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td bgcolor="#ffffff" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td valign="top" style="width:45%;vertical-align:top;font-size:10px;">By: Broker<br/><br/></td>
    <td style="width:10%">&nbsp;</td>
    <td valign="top" style="width:45%;vertical-align:top;font-size:10px;">Date</td>
  </tr>
  <tr>
    <td bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td bgcolor="#ffffff" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
    <td bgcolor="#000000" height="1"><img src="/sites/all/themes/p1/images/spacer.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td valign="top" style="width:45%;vertical-align:top;font-size:10px;">By: Borrower/Applicant<br/><br/></td>
    <td style="width:10%">&nbsp;</td>
    <td valign="top" style="width:45%;vertical-align:top;font-size:10px;">Date</td>
  </tr>
  <tr valign="top">
    <td>&nbsp;</td>
  </tr>
</table>
<br/>
<table width="750" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center">Corporate Headquarters | 2525 Ponce De Leon 3rd Floor Coral Gables, FL 33134 | P.888.358.3444 F.888.247.9876</td>
  </tr>
  <tr>
    <td align="center"><a href="http://www.metwestonline.com">www.metwestonline.com</a> | <a href="mailto:info@metwestonline.com">info@metwestonline.com</a></td>
  </tr>
</table>
</div>
</body>
</html>
