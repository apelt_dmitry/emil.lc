<ul class="tabs--primary nav nav-tabs">
<li><a href="/deal/<?php print $deal_id; ?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Deal</a></li>
</ul>

<div class="card">

  <div class="card-header">
  <h1>Financials for Deal <?php print $deal_id; ?> ( <strong><?php print $loan_type; ?></strong> )</h1>
  </div>

  <?php if (strlen($message) > 0): ?>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="alert alert-danger lead text-center" role="alert"><?php print $message; ?></div>
      </div>
    </div>
  <?php endif; ?>


  <div class="card-header">
    <div class="row lead m-no m-5">
      <div class="col-md-2 text-right">Program:</div>
      <div class="col-md-4 text-left"><strong><?php print $program; ?></strong></div>
      <div class="col-md-2 text-right">Price:</div>
      <div class="col-md-4 text-left"><strong>$<?php print $price; ?></strong></div>
    </div>

    <div class="row lead m-no m-5">
      <div class="col-md-2 text-right">Property Type:</div>
      <div class="col-md-4 text-left"><strong><?php print $property_type; ?></strong></div>
      <div class="col-md-2 text-right">Loan Amount:</div>
      <div class="col-md-4 text-left"><strong>$<?php print $loan_amount; ?></strong></div>
    </div>

    <div class="row lead m-no m-5">
      <div class="col-md-2 text-right">Loan to Value:</div>
      <div class="col-md-4 text-left"><strong><?php print $loan_to_value+0; ?>%</strong></div>
      <div class="col-md-2 text-right">Rate:</div>
      <div class="col-md-4 text-left"><strong><?php print $rate; ?></strong></div>
    </div>

    <div class="row lead m-no m-5">
      <div class="col-md-2 text-right">Address:</div>
      <div class="col-md-4 text-left"><strong><?php print $address . ' ' . $address_2 . '<br />' . $city . ', ' . $state . ' ' . $zipcode; ?></strong></div>
      <div class="col-md-1 col-md-offset-1 text-right bg-green p-15 text-white">P & I:</div>
      <div class="col-md-2 text-left bg-green p-15 text-white"><strong>$<?php print $principal_with_interest; ?></strong></div>
    </div>
  </div>

</div>


<div class="card">
  <div class="card-header ch-alt">
    <div class="row">

      <div class="col-sm-3">
        <h1>Cash Flow Projections</h1>
      </div>

      <div class="col-sm-2">
        <div class="<?php print $class; ?> p-15 text-white">
          <div class="lead m-no">Actual DSCR</div>
          <h2 class="text-white m-no text-right"><?php print $current_dscr; ?></h2>
        </div>
      </div>

      <div class="col-sm-2">
        <div class="bg-blue p-15 text-white">
          <div class="lead m-no">Min. DSCR Required</div>
          <h2 class="text-white m-no text-right"><?php print $minimum_dscr; ?></h2>
        </div>
      </div>

      <div class="col-sm-2 text-right">
        <div class="step">
          <br />
          <h2>
            <span class="label label-success lg-text"><strong>Step 5</strong></span>
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          </h2>
        </div>
      </div>

      <div class="col-sm-2">
        <div class="p-15 text-white text-right">
          <a class="btn btn-lg btn-danger" href="<?php print $term_sheet ?>" >Go To Term Sheet</a>
        </div>
      </div>

    </div>
  </div>

  <div class="table-responsive lead">
    <?php print $calculation_table; ?>
  </div>

</div>
