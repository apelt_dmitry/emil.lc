<ul class="tabs--primary nav nav-tabs">
<li><a href="/deal/<?php print arg(1); ?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Deal</a></li>
</ul>

<div class="card">
    <div class="card-header">
        <h2>Condition Review
            <small>Review the provide document. When possible leave comments for the users.</small>
        </h2>
    </div>

    <div class="row card-body card-padding">

      <div class="col-sm-12">

        <object id="pdf_reader" data="<?php print $file_url; ?>#view=FitH" type="application/pdf" width="100%" height="500px">
        <embed src="<?php print $file_url; ?>#view=FitH" width="100%" height="500px" type="application/pdf">
          <p>It appears your Web browser is not configured to display PDF files.
            <a href="http://www.adobe.com/products/reader.html">Download adobe Acrobat </a> or
            <a href="<?php print $file_url; ?>">click here to download the PDF file.</a>
          </p>
          <parm name="view" value="FitH"/>
        </object>


      </div>
  </div>
</div>

<div class="card">
    <div class="row card-body card-padding">
      <div class="col-sm-12">
        <?php print $review_form; ?>
      </div>
    </div>
</div>
