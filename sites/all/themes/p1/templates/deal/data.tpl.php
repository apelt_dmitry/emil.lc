<div class="card">

    <div class="card-header ch-alt">
      <div class="row">
        <div class="col-md-6">
          <h1><?php print $address .' ' . $address_2 .' ' . $city .', ' . $state .' ' . $zipcode;  ?></h1>
        </div>

        <div class="col-md-2">
          <div class="bg-gray p-15 text-white">
            <div class="lead m-no">Loan type</div>
            <h2 class="text-white m-no text-right"><?php print $loan_type; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-gray p-15 text-white">
            <div class="lead m-no">Property type</div>
            <h2 class="text-white m-no text-right"><?php print $property_type; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-gray p-15 text-white">
            <div class="lead m-no">FICO Score</div>
            <h2 class="text-white m-no text-right"><?php print intval($fico_score); ?></h2>
          </div>
        </div>

      </div>
    </div>


    <div class="card-header">

      <div class="row">

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Vacancy Factor</div>
            <h2 class="m-no text-right"><?php print $vacancy_factor; ?>%</h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Property income</div>
            <h2 class="m-no text-right">$<?php print $property_income; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Other income</div>
            <h2 class="m-no text-right">$<?php print $other_income; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Expenses</div>
            <h2 class="m-no text-right">$<?php print $expenses; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Program type</div>
            <h2 class="m-no text-right"><?php print $program_type; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-amber p-10">
            <div class="lead m-no">Purchase Price</div>
            <h2 class="m-no text-right">$<?php print $purchase_price; ?></h2>
          </div>
        </div>

      </div>

    </div>



    <div class="card-header">

      <div class="row">

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Term</div>
            <h2 class="m-no text-right"><?php print $terms; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Amortization</div>
            <h2 class="m-no text-right"><?php print $amortization; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Pre Payment Penalty</div>
            <h2 class="m-no text-right"><?php print $pre_payment_penalty; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Bankruptcy date</div>
            <h2 class="m-no text-right"><?php print $bankruptcy_date; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Foreclosure date</div>
            <h2 class="m-no text-right"><?php print $foreclosure_date; ?></h2>
          </div>
        </div>

        <div class="col-md-2">
          <div class="bg-grey p-10">
            <div class="lead m-no">Documents</div>
            <h2 class="m-no text-right"><?php print $doc; ?></h2>
          </div>
        </div>

      </div>

    </div>



    <div class="card-header">

      <div class="row">

        <div class="col-md-3">
          <div class="bg-orange p-15 text-white">
            <div class="lead m-no">Down Payment</div>
            <h2 class="text-white m-no text-right">$<?php print $down_payment_amount; ?></h2>
          </div>
        </div>

        <div class="col-md-3">
          <div class="bg-blue p-15 text-white">
            <div class="lead m-no">Appraised Value</div>
            <h2 class="text-white m-no text-right">$<?php print $appraised_value; ?></h2>
          </div>
        </div>

        <div class="col-md-3">
          <div class="bg-green p-15 text-white">
            <div class="lead m-no">Loan Amount</div>
            <h2 class="text-white m-no text-right">$<?php print $loan_amount; ?></h2>
          </div>
        </div>

        <div class="col-md-3">
          <div class="bg-red p-15 text-white">
            <div class="lead m-no">Loan to Value</div>
            <h2 class="text-white m-no text-right"><?php print $loan_to_value; ?></h2>
          </div>
        </div>

      </div>

    </div>

</div>
