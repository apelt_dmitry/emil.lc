<div class="card">
    <div class="card-header">
        <h2>Deal Status
            <small>Check the deal chronological updates on the table below.</small>
        </h2>
    </div>

    <div class="row card-body card-padding">

      <div class="col-sm-4 text-center">
        <p class="deal-image-status">
          <img src="/<?php print $image; ?>" class="img-responsive" alt="Deal Status">
        </p>
        <h1><?php print format_interval((time() - $timestamp) , 2) . t(' ago') ?></h1>
      </div>

      <div class="col-sm-4">
        <div class="well lead">
          <strong>Deal status: </strong><?php print $phase; ?>
          <br /><br />
          <strong>Required conditions to move to the next phase <?php print $next ?></strong>
          <?php print $conditions; ?>
        </div>
      </div>


      <div class="col-sm-4">
        <?php if (isset($message_form)): ?>
          <?php print $message_form; ?>
        <?php endif; ?>
      </div>
    </div>
</div>
<?php if(isset($filters_form) && isset($record_table)): ?>
<div class="card">
    <div class="row card-body card-padding">
      <div class="col-sm-12">
        <?php print $filters_form; ?>
      </div>

    </div>

    <div class="card-body table-responsive">
        <?php print $record_table; ?>
    </div>
</div>
<?php endif; ?>
