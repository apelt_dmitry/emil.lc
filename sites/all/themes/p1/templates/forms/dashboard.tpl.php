<div class="card">
    <div class="card-header">
        <h2>Your Deals
            <small>Find your deals listed below. Use the following form to filter them.</small>
        </h2>
    </div>

    <div class="row card-body card-padding">
        <div class="col-sm-4">
          <?php print render($form['search']['address']); ?>
        </div>
        <div class="col-sm-2">
          <?php print render($form['search']['program_type']); ?>
        </div>
        <div class="col-sm-2">
          <?php print render($form['search']['property_type']); ?>
        </div>
        <div class="col-sm-2">
          <?php print render($form['search']['loan_type']); ?>
        </div>
        <div class="col-sm-2">
          <?php print render($form['search']['submit']); ?>
        </div>
    </div>

    <div class="card-body table-responsive">
        <?php print drupal_render_children($form); ?>
    </div>

</div>
