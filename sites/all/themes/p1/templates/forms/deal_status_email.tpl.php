<ul class="tabs--primary nav nav-tabs">
<li><a href="/deal/<?php print arg(1); ?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Deal</a></li>
</ul>

<div class="card">
    <div class="card-header">
        <h2>Condition E-mail
            <small>You can email the reviewed document.</small>
        </h2>
    </div>

    <div class="row card-body card-padding">

      <div class="col-sm-6">
        <?php print render($form['email']); ?>
      </div>

      <div class="col-sm-6">
        <div class="alert alert-info lead" role="alert">
          <strong>Think before you email.</strong>
          This email and any files transmitted with it are confidential and
          intended solely for the use of the individual or entity to whom they
          are addressed.
        </div>
      </div>

      <div class="col-sm-12 card-top-margin">
        <?php print render($form['message']); ?>
      </div>

      <div class="col-sm-12 card-top-margin">
        <?php print render($form['submit']); ?>
      </div>

      </div>
  </div>
</div>

<?php print drupal_render_children($form); ?>
