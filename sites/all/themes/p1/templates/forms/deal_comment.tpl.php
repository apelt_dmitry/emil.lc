<div class="status-comments">

  <div class="col-md-12">
    <?php print render($form['message']); ?>
  </div>

  <div class="col-md-12">
    <br />
    <div class="text-right">
    <?php print str_replace('btn-default', 'btn-warning', render($form['submit'])); ?>
    </div>
  </div>

  <?php print drupal_render_children($form); ?>

</div>
