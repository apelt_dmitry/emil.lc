<div class="status-filter">

  <div class="col-md-2">
    <?php print render($form['comment']); ?>
  </div>
  <div class="col-md-2">
    <?php print render($form['from']); ?>
  </div>
  <div class="col-md-2">
    <?php print render($form['to']); ?>
  </div>

  <div class="col-md-2">
    <?php print render($form['user']); ?>
  </div>
  <div class="col-md-2">
    <?php print render($form['type']); ?></div>
  <div class="col-md-2">
    <?php print render($form['submit']); ?>
  </div>

  <?php print drupal_render_children($form); ?>

</div>
