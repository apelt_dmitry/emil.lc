<div class="card">
    <div class="card-header">
        <h2>Client Information</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row">

        <div class="col-sm-6">
          <?php print render($form['first_name']); ?>
        </div>

        <div class="col-sm-6">
          <?php print render($form['last_name']); ?>
        </div>

      </div>

</div></div>

<div class="card">
    <div class="card-header">
        <h2>Address</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row card-top-margin">
        <div class="col-sm-4">
          <?php print render($form['address']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['address_2']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['city']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['state']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['zipcode']); ?>
        </div>
      </div>

</div></div>

<div class="card">
    <div class="card-header">
        <h2>Contact</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row card-top-margin">

        <div class="col-sm-3">
          <?php print render($form['cell']); ?>
        </div>

        <div class="col-sm-3">
          <?php print render($form['telephone']); ?>
        </div>

        <div class="col-sm-3">
          <?php print render($form['fax']); ?>
        </div>

        <div class="col-sm-3">
          <?php print render($form['email']); ?>
        </div>

      </div>

</div></div>

<div class="card">
    <div class="card-header">
        <h2>Aggrement</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row card-top-margin">

        <div class="col-sm-6">
          <?php print render($form['signature']); ?>
        </div>

        <div class="col-sm-6">
          <?php print render($form['submit']); ?>
          <?php print render($form['actions']['submit']); ?>
        </div>

      </div>


    </div>

</div>

<?php print drupal_render_children($form); ?>
