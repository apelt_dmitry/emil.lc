
    <div class="card-header">
        <h2>Deal Access</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row">

        <div class="col-sm-6">
          <?php print render($form['role']); ?>
        </div>

        <div class="col-sm-6">
          <?php print render($form['notify']); ?>
        </div>

      </div>

      <div class="row card-top-margin">

        <div class="col-sm-6">
          <?php print render($form['email']); ?>
        </div>

        <div class="col-sm-6">
          <?php print render($form['confirm_email']); ?>
        </div>

      </div>

      <div class="row card-top-margin">

        <div class="col-sm-12 card-top-margin">
          <?php print str_replace('btn-default', 'btn-success', render($form['submit'])); ?>
          <?php print str_replace('btn-default', 'btn-success', render($form['actions']['submit'])); ?>
        </div>

      </div>

    </div>

    <div class="row">

      <div class="col-sm-12">
        <?php print render($form['table']); ?>
      </div>

    </div>


<?php print drupal_render_children($form); ?>
