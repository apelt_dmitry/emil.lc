<div class="main-container container-fluid">
  <div class="row">
    <section class="col-sm-10 col-sm-offset-1">
      <a id="main-content"></a>

      <div class="row">
        <div class="col-sm-4 col-sm-offset-4">

          <div class="card card-top-margin">

            <div class="card-header card-padding bg-primary">
              <h2 class="text-white">Oh, one of those days!</h2>
            </div>

            <div class="card-body card-padding">

              <div class="input-group card-top-margin">
                <?php print render($form['name']); ?>
              </div>

              <div class="row card-top-margin">
                <div class="col-sm-6 btn-group btn-group-justified">
                  <div class="btn-group" role="group">
                    <?php print render($form['actions']['submit']); ?>
                  </div>
                </div>
              </div>

              <div class="input-group card-top-margin">
                <?php print render($form['login']); ?>
              </div>

              <?php print drupal_render_children($form); ?>
            </div>

          </div>

        </div>
      </div>

    </section>
  </div>
</div>
