<!-- <div class="card"> -->
    <div class="card-header ch-alt">

      <div class="row">

        <div class="col-sm-3">
          <h1>Available Programs</h1>
        </div>

        <div class="col-sm-9 lead m-no bg-white p-15 text-black"><div class="row">

          <div class="col-sm-2">
            <?php print render($form['container']['lending_programs']); ?>
          </div>

          <div class="col-sm-2">
            <?php print render($form['container']['rebates']); ?>
          </div>

          <div class="col-sm-2">
            <?php print str_replace('btn-default', 'btn-success', render($form['container']['submit'])); ?>
          </div>

        </div></div>

      </div>

    </div>

<!-- </div> -->

<?php print drupal_render_children($form); ?>
