<ul class="tabs--primary nav nav-tabs">
<li><a href="/deal/<?php print arg(1); ?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Deal</a></li>
</ul>

<!-- Merge start -->

<div class="card">

  <div class="card-body card-padding">

  <div class="row">
    <div class="col-xs-6">
      <h2>Term Sheet</h2>
    </div>
    <div class="col-xs-6">
      <div class="text-right"><strong>Date:</strong> <?php print date('m/d/Y', REQUEST_TIME); ?></div>
      <div class="text-right"><strong>Expires:</strong> EOD</div>
    </div>
  </div>

  <div class="row">

    <div class="col-sm-4 card-body card-padding focus">

      <fieldset class="panel panel-default form-wrapper" id="edit-borrower">
        <legend class="panel-heading">
          <span class="panel-title fieldset-legend">Borrower information</span>
        </legend>
        <div class="panel-body">

          <div class="row">
            <div class="col-sm-12">
              <?php print render($form['borrower_entity']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-6">
              <?php print render($form['borrower_first_name']); ?>
            </div>
            <div class="col-sm-6">
              <?php print render($form['borrower_last_name']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-6">
              <?php print render($form['borrower_email']); ?>
            </div>
            <div class="col-sm-6">
              <?php print render($form['borrower_telephone']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-8">
              <?php print render($form['borrower_address']); ?>
            </div>
            <div class="col-sm-4">
              <?php print render($form['borrower_address_2']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-4">
              <?php print render($form['borrower_city']); ?>
            </div>
            <div class="col-sm-4">
              <?php print render($form['borrower_state']); ?>
            </div>
            <div class="col-sm-4">
              <?php print render($form['borrower_zipcode']); ?>
            </div>
          </div>
        </fieldset>

    </div>

    <div class="col-sm-4 card-body card-padding focus">

      <fieldset class="panel panel-default form-wrapper" id="edit-borrower">
        <legend class="panel-heading">
          <span class="panel-title fieldset-legend">Co-Borrower information (if applicable)</span>
        </legend>
        <div class="panel-body">

          <div class="row">
            <div class="col-sm-12">
              <?php print render($form['co_borrower_entity']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-6">
              <?php print render($form['co_borrower_first_name']); ?>
            </div>
            <div class="col-sm-6">
              <?php print render($form['co_borrower_last_name']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-6">
              <?php print render($form['co_borrower_email']); ?>
            </div>
            <div class="col-sm-6">
              <?php print render($form['co_borrower_telephone']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-8">
              <?php print render($form['co_borrower_address']); ?>
            </div>
            <div class="col-sm-4">
              <?php print render($form['co_borrower_address_2']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-4">
              <?php print render($form['co_borrower_city']); ?>
            </div>
            <div class="col-sm-4">
              <?php print render($form['co_borrower_state']); ?>
            </div>
            <div class="col-sm-4">
              <?php print render($form['co_borrower_zipcode']); ?>
            </div>
          </div>
        </fieldset>

    </div>

    <div class="col-sm-4 card-body card-padding focus">

      <fieldset class="panel panel-default form-wrapper" id="edit-borrower">
        <legend class="panel-heading">
          <span class="panel-title fieldset-legend">Broker information</span>
        </legend>
        <div class="panel-body">

          <div class="row">
            <div class="col-sm-12">
              <?php print render($form['broker_entity']); ?>
            </div>
          </div>

          <div class="row card-top-margin">
            <div class="col-sm-6">
              <?php print render($form['broker_first_name']); ?>
            </div>

            <div class="col-sm-6">
              <?php print render($form['broker_last_name']); ?>
            </div>

          </div>

          <div class="row card-top-margin">
            <div class="col-sm-6">
              <?php print render($form['broker_email']); ?>
            </div>
            <div class="col-sm-6">
              <?php print render($form['broker_telephone']); ?>
            </div>
          </div>


          <div class="row card-top-margin">
            <div class="col-sm-12">
            <div class="input-group">
              <?php print render($form['broker_origination_fee']); ?>
              <span class="input-group-addon" id="name-addon">%</span>
            </div>
            </div>
            <div class="col-sm-12">
              <div class="input-group">
              <?php print render($form['lender_origination_fee']); ?>
              <span class="input-group-addon" id="name-addon">%</span>
            </div>
            </div>
          </div>
        </div>
    </fieldset>
  </div>

  <div class="row">
    <div class="col-xs-12">
      <table class="table table-bordered table-striped">
        <tr>
          <td colspan="6" align="cnter" class="text-center"><strong>Proposed Loan Terms</strong></td>
        </tr>

        <tr>
          <td class="success text-right"><strong>Loan Program</strong></td>
          <td class="text-center"><?php print render($form['loan_program']); ?></td>
          <td class="success text-right"><strong>Mid FICO Score</strong></td>
          <td class="text-center"><?php print render($form['min_fico']); ?></td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Proposed Loan Amount</strong></td>
          <td class="text-center">$<?php print render($form['loan_amount']); ?></td>
          <td class="success text-right"><strong>***Star/Floor Rate</strong></td>
          <td class="text-center"><?php print render($form['rate']); ?>%</td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Est. Property Value</strong></td>
          <td class="text-center">$<?php print render($form['appraised_value']); ?></td>
          <td class="success text-right"><strong>Estimated Mo. Payment</strong></td>
          <td class="text-center">$<?php print render($form['amortization']); ?></td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Estimated LTV</strong></td>
          <td class="text-center"><?php print render($form['ltv']); ?></td>
          <td class="success text-right"><strong>Rate Fixed For</strong></td>
          <td class="text-center"><?php print render($form['terms']); ?></td>
        </tr>

        <tr>
          <td class="success text-right"><strong>Type of Loan Term</strong></td>
          <td class="text-center">Fixed-Hybrid</td>
          <td class="success text-right"><strong>Rate Adjusts After</strong></td>
          <td class="text-center"><?php print render($form['terms']) . ' Hybrid / ' . render($form['years']); ?></td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Amortization</strong></td>
          <td class="text-center"><?php print render($form['years']); ?></td>
          <td class="success text-right"><strong>Index @ Adjustment</strong></td>
          <td class="text-center">WSJ Prime Rate</td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Impounds</strong></td>
          <td class="text-center">Tax & Insurance</td>
          <td class="success text-right"><strong>Margin @ Adjustment</strong></td>
          <td class="text-center"><?php print render($form['margin']); ?>%</td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Recourse</strong></td>
          <td class="text-center">Full</td>
          <td class="success text-right"><strong>Max Life Adjustment</strong></td>
          <td class="text-center"><?php print render($form['max_life_adjustment']); ?>%</td>
        </tr>

        <tr>
          <td class="success text-right"><strong>Prepay</strong></td>
          <td class="text-center"><?php print render($form['pre_payment_penalty']); ?></td>
          <td class="success text-right"><strong>Max First Adjustment</strong></td>
          <td class="text-center"><?php print render($form['max_first_adjustment']); ?>%</td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Underwriting Fee</strong></td>
          <td class="text-center"><?php print render($form['underwriting_fees']); ?></td>
          <td class="success text-right"><strong>Max. Subsequent</strong></td>
          <td class="text-center"><?php print render($form['max_subsequent_adjustment']); ?>%</td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Processing Fee</strong></td>
          <td class="text-center"><?php print render($form['processing_fee']); ?></td>
          <td class="success text-right"><strong>Adj. Frequency</strong></td>
          <td class="text-center">Every 6 mos. After fixed period</td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Lender Points</strong></td>
          <td class="text-center"><?php print render($form['saved_lender_origination_fee']); ?></td>
          <td class="success text-right"><strong>Third Party Report</strong></td>
          <td class="text-center">$<?php print render($form['appraisal_fee']); ?></td>
        </tr>
        <tr>
          <td class="success text-right"><strong>Broker Points</strong></td>
          <td class="text-center"><?php print render($form['saved_broker_origination_fee']); ?></td>
          <td class="success text-right"><strong>Total Deposit</strong></td>
          <td class="text-center">$<?php print render($form['appraisal_fee']); ?></td>
        </tr>

      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">
      ***The Start/Floor Rate is subject to change without notice due to market volatility or market conditions therefore this rate is not locked.
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">
      <table class="table table-bordered table-striped">
        <tr class="success">
          <td class="text-center"><strong>Open File</strong></td>
          <td class="text-center"><strong>Prior to Engaging Appraisal</strong></td>
          <td class="text-center"><strong>Underwriting to Closing</strong></td>
        </tr>
        <tr>
          <td class="text-center">See Page 2 for Details</td>
          <td class="text-center">See Page 3 for Details</td>
          <td class="text-center">See Page 3 for Details</td>
        </tr>
      </table>

    </div>
  </div>

  <div class="row">
    <div class="col-xs-3" style="white-space:nowrap;">Term Sheet issued to</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>
    <div class="col-xs-3" style="white-space:nowrap;">For property located at</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"><?php print render($form['address']); ?></div>
    <div class="col-xs-3" style="white-space:nowrap;">Date of issuance</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"><?php print date('m/d/Y', REQUEST_TIME); ?></div>
  </div>


  <div class="row">
    <div class="col-xs-12">

      <p>
      Congratulations, on behalf of Metwest Capital Group, we are providing this non-binding Letter of Interest (LOI)
      as an expression of interest from our part. This letter is not a commitment to fund, rather a pre-approval subject
      to loan conditions. This letter will expire five (5) business days from the date shown above.
      </p>

      <p>
      For clarification purposes, any decision whether to approve your loan application can only be made by MCG's
      Loan Credit Officers upon completion of our internal underwriting process. No representation or promise is
      intended or made that approval of borrower's application will be made, either on the terms described herein or on
      any other terms whatsoever. Under no circumstances is any employee, agent or representative of MCG
      authorized to order or issue or make any promise or commitment to lend funds to borrower, whether orally or in
      writing. MCG requires a personal guarantee on all loans regardless of adjustments made to the terms and
      conditions listed hereto on said "LOI". To the extent of any inconsistency or conflict between the loan
      terms set forth in the final loan documents and this letter, the loan documents shall supersede this letter and shall
      be binding upon borrower. Final rate not approved until loan documents have been drawn. This letter supersedes
      any and all prior or contemporaneous discussions, representations, offers or statements, whether written or oral,
      made by MCG and is governed by the laws of the State of Florida.
      </p>

      <p>
      The undersigned: (1) certifies that all statements in the application and each document required to be submitted
      in connection herewith, including federal income tax returns(where applicable) are true, correct and complete and
      further agrees to notify MCG promptly of any material change in such information; (2) acknowledges that all
      signature copies and faxes can be relied upon by MCG as if they were original signatures; (3) authorizes MCG to
      rely upon such statements, make such inquiries, and gather such information as MCG deems necessary and
      reasonable to verify any information provided to MCG and to exchange this information with business credit
      reporting or credit bureau agencies and creditors of the undersigned; (4) further gives permission to MCG to
      share information with any federal, state, or other authorities and/or lenders for the purpose of processing the
      loan application; (5) authorizes MCG to verify my (our) identity; (6) authorizes MCG to obtain all payoff, satisfaction/
      release information and/or paid in full documentation.
      </p>

      <p>
      If your application for business credit is denied, pursuant to the Equal Credit Opportunity Act, MCG hereby
      provides notice that it will notify borrowers either (1) orally, (2) by email or (3) by certified mail of the borrowers'
      rights to receive a statement of reasons regarding an adverse credit decision made by MCG. The federal Equal
      Credit Opportunity Act prohibits creditors from discriminating against credit applicants on the basis of race, color,
      religion, national origin, sex, marital status, age (provided the applicant has the capacity to enter into a binding
      contract); because all or part of the applicant's income derives from any public assistance program; or because
      the applicant has in good faith exercised any right under the Consumer Credit Protection Act. The federal agency
      that administers compliance with this law concerning this creditor is Federal Trade Commission, Equal Credit
      Opportunity, Washington, DC 20580.
      </p>

      <p>
      Commercial Use: The undersigned certifies that any property and/or proceeds from the proposed request will be
      used by the applicant for commercial purpose only and not for any personal, family or household purposes, and
      that the proposed request would constitute a business loan which is exempted from the disclosure requirements
      of Regulation Z - Truth in Lending Act. The applicant agrees to indemnify and hold lender harmless from any and
      all claims, loss or damage resulting or caused by the request being subject to any of the provisions of the federal
      Consumer Credit Protection Act (Truth-in-Lending Act) and Regulation Z. The undersigned certifies that he/she
      has full authority to act on behalf of applicant in connection with the above referenced credit request.
      </p>

    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">

      <table class="table table-bordered table-striped">
        <tr class="text-center">
          <td class="text-center" colspan="2"><strong>From Open File to Conditions</strong></td>
        </tr>

        <tr>
          <td class="success text-center">Title</td>
          <td>
          Title insurance acceptable to Lender, insuring Lender's loan is in first position. If  is not provided it within
          a reasonable time, Lender will ordered through one of our approved companies.
          </td>
        </tr>

        <tr>
          <td class="success text-center">Prior to Appraisal ***items Approved by Underwriter</td>
          <td>
          _Fully executed Letter of Interest  (LOI)<br />
          _Including Page (3) executed “Information to Release Authorization”<br />
          _Completed 1003 or Personal Financial Statement (PFS)***<br />
          _Tri-Merge Credit report ***<br />
          _Signed purchase agreement (on purchases only)<br />
          _Dated rent roll (signed and dated by applicant/property owner)***<br />
          _Operating statements 2014 &  2015 (signed and dated by applicant/owner)***<br />
          _Photos (exterior)<br />
          _Three (3) Mo’s bank statements***<br />
          _Capital Expense schedule for recent improvements (if applicable)
          </td>
        </tr>

        <tr>
          <td class="success text-center" valign="middle">Conditions</td>
          <td>
          A survey will be required to close the loan<br />
          Verification of Mortgage payment 12 mo’s minimum<br />
          Mortgage Pay off quote good through 30 days<br />
          Proof of Property and Liability Insurance on subject<br />
          Business Entity legal documents (Certificate of Good Standing, Articles of Organization and Operating Agreement)<br />
          Other conditions may arrived prior to closing
          </td>
        </tr>

      </table>

    </div>
  </div>


  <div class="row">
    <div class="col-xs-3" style="white-space:nowrap;">Term Sheet issued to</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>
    <div class="col-xs-3" style="white-space:nowrap;">For property located at</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"><?php print render($form['address']); ?></div>
    <div class="col-xs-3" style="white-space:nowrap;">Date of issuance</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"><?php print date('m/d/Y', REQUEST_TIME); ?></div>
  </div>


  <div class="row">
    <div class="col-xs-12 lead">Contact Information</div>

    <div class="col-xs-3" style="white-space:nowrap;">Title Co</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Title Contact</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Phone Number</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Email</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>
  </div>

  <div class="row">
    <br />

    <div class="col-xs-3" style="white-space:nowrap;">Closing Firm</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Attorney/Closer</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Phone Number</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Email</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>
  </div>

  <div class="row">
    <br />

    <div class="col-xs-3" style="white-space:nowrap;">Insurance Co</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Agentr</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Phone Number</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Email</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>
  </div>


  <div class="row">
    <div class="col-xs-12">
      <h2 class="text-center">Information Release Authorization</h2>
      <p>
       I/We hereby authorize the release of any and all information to METWEST CAPITAL GROUP for the
      purpose of evaluating our credit transaction. I/We authorize METWEST CAPITAL GROUP to release any
      such information to any entity deemed necessary for any purpose related to this credit transaction and we
      understand that METWEST CAPITAL GROUP will not release our information to any vendor or party of
      interest that is unrelated to this transaction. My/Our signature(s) on this form can be used and interpreted
      as my/our original signature(s) on any information request made by METWEST CAPITAL GROUP
      including pulling a full credit report reflecting FICO scores.
      </p>
    </div>
  </div>


  <div class="row">
    <br />

    <div class="col-xs-3" style="white-space:nowrap;">Printed Name</div>
    <div class="col-xs-6" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-1" style="white-space:nowrap;">Date</div>
    <div class="col-xs-2" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Signature</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Birthday (dd/mm/yyyy)</div>
    <div class="col-xs-3" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Social Security Number</div>
    <div class="col-xs-3" style="border-bottom:1px solid #ccc;"> &nbsp;</div>

    <div class="col-xs-3" style="white-space:nowrap;">Current Address</div>
    <div class="col-xs-9" style="border-bottom:1px solid #ccc;"> &nbsp;</div>
  </div>


  <div class="row">
    <br />
    <div class="col-xs-12">
      <p>
      Please execute  this Letter of Interest,  indicating your approval to the
      terms herein, accompanied by the deposit before expiration of this Term
      Sheet (cited at the top of page 1).   A copy of the signed original of
      this Letter of Interest shall be considered effective consent.  Below, we
      are providing you with our Bank account information for the transfer  of funds.
      </p>
      <br />
      <p>
      Respectfully,<br />
      METWEST COMMERCIAL CAPITAL, LLC
      </p>
    </div>
  </div>


  <div class="row"><br /><br /><br />

    <div class="col-xs-6" style="border-top:1px solid #ccc;">

      <div class="col-sm-6 col-sm-offset-3 sig-well">
        <h6 class="text-center">Borrower Sign in the box below:</h6>
        <div class="sigPad" id="smoothed" style="width:325px; margin:auto;">
          <div class="sig sigWrapper current" style="width: 325px; height: 100%; display: block;">
            <div class="typed" style="display: none;"></div>
            <canvas class="pad" id="signature" width="320px"></canvas>
            <?php print render($form['borrower_signature']); ?>
          </div>
        </div>

      </div>

      <div class="col-md-12 text-center">
        <span id="clearButton" class="clearButton btn btn-warning">Clear Borrower Signature</span>
      </div>

    </div>


    <div class="col-xs-6" style="border-top:1px solid #ccc;">

      <div class="col-sm-6 col-sm-offset-3 sig-well">
        <h6 class="text-center">Co-Borrower Sign in the box below:</h6>
        <div class="sigPad" id="smoothed1" style="width:325px; margin:auto;">
          <div class="sig sigWrapper current" style="width: 325px; height: 100%; display: block;">
            <div class="typed" style="display: none;"></div>
            <canvas class="pad" id="signature" width="320px"></canvas>
            <?php print render($form['co_borrower_signature']); ?>
          </div>
        </div>

      </div>

      <div class="col-md-12 text-center">
        <span id="co-clearButton" class="clearButton1 btn btn-warning">Clear Co-Borrower Signature</span>
      </div>

    </div>


  </div>




  <div class="row">
    <div class="col-xs-4">
      <h2>PAYMENT METHODS</h2>
      <p>
      WIRE TRANSFER<br />
      Metwest Capital Group, LLC<br />
      SunTrust Bank<br />
      201 Alhambra Circle<br />
      Coral Gables, FL 33134<br />
      Routing Number: 061000104<br />
      Account Number: 1000186585989<br />
      Credit: $1,500.00
      </p>
    </div>

    <div class="col-xs-4">
      <h2>DEAL MISCELLANEOUS</h2>
      <strong>Entered By: </strong><?php print render($form['lender_name']); ?><br />
      <strong>E-mail: </strong><?php print render($form['lender_mail']); ?><br />
      <strong>Phone: </strong><?php print render($form['lender_phone']); ?><br />
      <strong>Operation: </strong><?php print render($form['loan_type']); ?><br />
      <strong>Type: </strong><?php print render($form['property_type']); ?>
    </div>






    <div class="col-xs-4">
        <div class="col-sm-12 card-top-margin text-center btn-group btn-group-justified">
          <div class="btn-group" role="group">
            <?php print render($form['submit']); ?>
            <?php print render($form['actions']['submit']); ?>
          </div>
        </div>
      <div class="col-sm-12 card-top-margin text-center btn-group btn-group-justified">
        <div class="btn-group" role="group">
          <?php print render($form['view']); ?>
        </div>
      </div>
      <?php if($form['pdf']): ?>
      <div class="col-sm-12 card-top-margin text-center btn-group btn-group-justified">
        <div class="btn-group" role="group">
          <?php print render($form['pdf']); ?>
        </div>
      </div>
      <?php endif; ?>
        <div class="col-sm-12 card-top-margin text-center btn-group btn-group-justified">
          <div class="btn-group" role="group">
            <?php print render($form['email']); ?>
          </div>
        </div>
    </div>

  </div>


  </div>

</div>

<!--  Merge ends -->




    </div>

  </div>

</div>

<?php print drupal_render_children($form); ?>
