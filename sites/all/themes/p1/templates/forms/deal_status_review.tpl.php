<div class="row">

  <div class="col-sm-8">
    <?php print render($form['comment']); ?>
  </div>

  <div class="col-sm-4 text-right">
    <?php print render($form['operation']); ?>
    <br />
    <?php print render($form['submit']); ?>
  </div>

</div>


<?php print drupal_render_children($form); ?>
