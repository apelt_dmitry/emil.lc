<div class="card">
    <div class="card-header">
        <h2>Deal Status
            <small>Check the deal chronological updates on the table below.</small>
        </h2>
    </div>

    <div class="row card-body card-padding">

      <div class="col-sm-6 text-center">
        <p class="deal-image-status"><?php print render($form['status']); ?></p>
        <h1><?php print render($form['created']); ?></h1>
      </div>

      <div class="col-sm-6">
        <?php print render($form['comments']); ?>

        <div class="text-right">
          <?php print render($form['submit']); ?>
        </div>

      </div>

    </div>

    <div class="card-body table-responsive">
        <?php print drupal_render_children($form); ?>
    </div>

</div>
