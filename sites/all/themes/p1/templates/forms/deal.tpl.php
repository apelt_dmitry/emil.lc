<div class="card">
    <div class="card-header">
        <h2><span class="label label-success lg-text"><strong>Step 1 </strong></span> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> The Transaction</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row">

        <div class="col-sm-2">
          <?php print render($form['property_type_id']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['loan_type']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['loan_amount']); ?>
        </div>

        <div class="col-sm-2">
          <div class="input-group">
            <?php print render($form['purchase_wrapper']); ?>
            <?php print render($form['appraised_value_wrapper']); ?>
          </div>
        </div>



        <div class="col-sm-2 col-sm-push-2">
          <?php print render($form['bankruptcy_date']); ?>
        </div>

      </div>

      <div class="row card-top-margin">
        <div class="col-sm-2">
          <?php print render($form['fico_score']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['loan_to_value']); ?>
        </div>

        <div class="col-sm-2">
          <div class="input-group">
            <?php print render($form['down_payment_percentage_wrapper']); ?>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="input-group">
            <?php print render($form['down_payment_amount_wrapper']); ?>
          </div>
        </div>

        <div class="col-sm-2 col-sm-push-2">
          <?php print render($form['foreclosure_date']); ?>
        </div>

      </div>

    </div>

</div>


<div id="card-2" class="card card-top-margin hidden">
    <div class="card-header">
        <h2><span class="label label-success lg-text"><strong>Step 2</strong></span> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> The Property</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row">



        <div class="col-sm-3">
          <?php print render($form['address']); ?>
        </div>

        <div class="col-sm-1">
          <?php print render($form['address_2']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['city']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['state']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['zipcode']); ?>
        </div>
      </div>

      <div class="row card-top-margin">

        <div class="col-sm-2">
            <?php print render($form['occupancy']); ?>
        </div>

        <div class="col-sm-2">
          <div class="input-group">
            <span class="input-group-addon" id="name-addon">$</span>
            <?php print render($form['property_income']); ?>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="input-group">
            <span class="input-group-addon" id="name-addon">$</span>
            <?php print render($form['other_income']); ?>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="input-group">
            <span class="input-group-addon" id="name-addon">$</span>
            <?php print render($form['expenses']); ?>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="input-group">
            <?php print render($form['available_units']); ?>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="input-group">
            <?php print render($form['vacancy_factor']); ?>
            <span class="input-group-addon" id="name-addon">%</span>
          </div>
        </div>

      </div>

    </div>

</div>


<div id="card-3" class="card card-top-margin hidden">
    <div class="card-header">
        <h2><span class="label label-success lg-text"><strong>Step 3</strong></span> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> Loan Specifications</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row">

        <div class="col-sm-2">
          <?php print render($form['program_type_id']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['terms']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['amortization']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['pre_payment_penalty']); ?>
        </div>

        <div class="col-sm-2">
          <?php print render($form['doc_id']); ?>
        </div>

        <div class="col-sm-2 text-center">
          <?php print render($form['actions']['submit']); ?>
        </div>

      </div>

    </div>

</div>


<div class="row">
  <div class="col-sm-12 text-center">
    <div class="alert alert-block alert-info m-20 disclaimer-lead">
      <?php print render($form['disclaimer']); ?>
    </div>
  </div>
</div>

        <?php print drupal_render_children($form); ?>



