<div class="card">
    <div class="card-header">
        <h2>Deal Conditions</h2>
    </div>

    <div class="card-body card-padding">

      <div class="row">

        <div class="col-sm-6">
          <?php print render($form['type']); ?>
        </div>

        <div class="col-sm-6">
          <?php print render($form['file']); ?>
        </div>

        <div class="col-sm-6 card-top-margin">
          <?php print render($form['comment']); ?>
        </div>

        <div class="col-sm-6 card-top-margin">
          <div class="well lead">
            <strong>Deal phase: </strong><?php print render($form['phase']); ?><br />
            <strong>Required conditions to move to the next phase</strong>
            <?php print render($form['conditions']); ?>
          </div>
        </div>

        <div class="col-sm-12 card-top-margin">
          <?php print str_replace('btn-default', 'btn-success', render($form['submit'])); ?>
        </div>

      </div>

    </div>

    <div class="row">

      <div class="col-sm-12">
        <?php print render($form['table']); ?>
      </div>

    </div>

</div>



<?php print drupal_render_children($form); ?>
