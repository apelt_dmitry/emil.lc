<div class="card">
    <div class="card-header">
        <h2>Your profile
            <small>Change your account details and contact information.</small>
        </h2>
    </div>

    <div class="card-body card-padding">

      <div class="row card-top-margin">
        <div class="col-md-6">
          <?php print render($form['first_name']); ?>
        </div>

        <div class="col-md-6">
          <?php print render($form['last_name']); ?>
        </div>
      </div>

      <div class="row card-top-margin">
        <div class="col-md-6">
          <?php print render($form['company_name']); ?>
        </div>

        <div class="col-md-6">
          <?php print render($form['type']); ?>
        </div>
      </div>

      <div class="row card-top-margin">
        <div class="col-md-6">
          <?php print render($form['telefone']); ?>
        </div>

        <div class="col-md-6">
          <?php print render($form['fax']); ?>
        </div>
      </div>

      <div class="row card-top-margin">
        <div class="col-md-8">
          <?php print render($form['address']); ?>
        </div>
        <div class="col-md-4">
          <?php print render($form['address_2']); ?>
        </div>
      </div>

      <div class="row card-top-margin">
        <div class="col-md-4">
          <?php print render($form['city']); ?>
        </div>
        <div class="col-md-4">
          <?php print render($form['state']); ?>
        </div>
        <div class="col-md-4">
          <?php print render($form['zipcode']); ?>
        </div>
      </div>

      <div class="row card-top-margin">
        <div class="col-md-6">
          <?php print render($form['timezone']); ?>
        </div>

        <div class="col-md-6">
          <?php print render($form['account']['status']); ?>
        </div>
      </div>

    </div>

</div>


<div class="card card-top-margin">
    <div class="card-header">
        <h2>Your avatar
            <small>Change your avatar image.</small>
        </h2>
    </div>

    <div class="row card-body card-padding">

        <div class="col-sm-12 card-top-margin">
          <?php print render($form['photo']); ?>
        </div>

    </div>

</div>


<div class="card card-top-margin">
    <div class="card-header">
        <h2>Your credentials
            <small>Change your login password or e-mail.</small>
        </h2>
    </div>

    <div class="row card-body card-padding">

        <div class="col-sm-6 card-top-margin">
          <?php print render($form['account']['mail']); ?>
        </div>

        <div class="col-sm-6 card-top-margin">
          <?php print render($form['account']['current_pass']); ?>
        </div>

        <div class="col-sm-6 card-top-margin">
          <?php print render($form['account']['pass']['pass1']); ?>
        </div>

        <div class="col-sm-6 card-top-margin">
          <?php print render($form['account']['pass']['pass2']); ?>
        </div>

        <div class="col-sm-12 card-top-margin">
          <div class="password-strength"></div>
        </div>

        <div class="col-sm-12 card-top-margin">
          <?php print drupal_render_children($form); ?>
        </div>

    </div>

</div>


