<div class="card">

  <div class="row">

    <div class="col-sm-5 col-sm-offset-1">

      <h2>Basic Information</h2>

      <div class="pmbb-view">

          <dl class="dl-horizontal">
              <dt>Company</dt>
              <dd><?php print render($user_profile['company_name']); ?></dd>
          </dl>

          <dl class="dl-horizontal">
              <dt>Full Name</dt>
              <dd><?php print render($user_profile['first_name']); ?> <?php print render($user_profile['last_name']); ?></dd>
          </dl>

          <dl class="dl-horizontal">
              <dt>Representative</dt>
              <dd><?php print render($user_profile['representative']); ?></dd>
          </dl>

          <dl class="dl-horizontal">
              <dt>Member for</dt>
              <dd><?php print render($user_profile['summary']['member_for']); ?></dd>
          </dl>

      </div>

    </div>

    <div class="col-sm-5">

      <h2>Contact Information</h2>

      <div class="pmbb-view">

          <dl class="dl-horizontal">
              <dt>E-mail</dt>
              <dd><?php print $user->mail; ?></dd>
          </dl>

          <dl class="dl-horizontal">
              <dt>Phone</dt>
              <dd><?php print render($user_profile['telefone']); ?></dd>
          </dl>

          <dl class="dl-horizontal">
              <dt>Fax</dt>
              <dd><?php print render($user_profile['fax']); ?></dd>
          </dl>

          <dl class="dl-horizontal">
              <dt>Address</dt>
              <dd>
                <?php print render($user_profile['address']); ?> <?php print render($user_profile['address_2']); ?> <br/ >
                <?php print render($user_profile['city']); ?>, <?php print render($user_profile['state']); ?> <?php print render($user_profile['zipcode']); ?>
              </dd>
          </dl>

      </div>





    </div>
  </div>
</div>

