<?php
/**
 * Pre-processes variables for the "page" theme hook.
 *
 * See template for list of available variables.
 *
 * @see page.tpl.php
 *
 * @ingroup theme_preprocess
 */
function p1_preprocess_page(&$variables) {
  // Add information about the number of sidebars.
  if (!empty($variables['page']['sidebar_first']) && !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-6"';
  }
  elseif (!empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second'])) {
    $variables['content_column_class'] = ' class="col-sm-9"';
  }
  else {
    $variables['content_column_class'] = ' class="col-sm-10 col-sm-offset-1"';
  }

  // Title
  if ($variables['is_front'] || arg(0) == 'deal' || arg(0) == 'user') {
    $variables['title'] = '';
  }

}

/**
* theme_menu_link()
*/
function p1_menu_link(array $variables) {
  // custom class
  $custom_class = str_replace(' ','-',strtolower(check_plain($variables['element']['#title'])));

  // add class for li
  $variables['element']['#attributes']['class'][] = 'menu-' . $variables['element']['#original_link']['mlid'] . ' ' .$custom_class;

  // add class for a
  $variables['element']['#localized_options']['attributes']['class'][] = 'menu-' . $variables['element']['#original_link']['mlid'] . ' ' . $custom_class;

  return theme_menu_link($variables);
}


function p1_theme($existing, $type, $theme, $path) {

  // Login form
  $items['user_login'] = array(
      'render element' => 'form',
      'template' => 'login',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Password reset form
  $items['user_pass'] = array(
      'render element' => 'form',
      'template' => 'reset',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // User Registration form
  $items['user_register_form'] = array(
      'render element' => 'form',
      'template' => 'register',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Dashboard form
  $items['p1_dashboard_form'] = array(
      'render element' => 'form',
      'template' => 'dashboard',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Add deal form
  $items['p1_deal_form'] = array(
      'render element' => 'form',
      'template' => 'deal',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // View deal form
  $items['p1_deal_view_form'] = array(
      'render element' => 'form',
      'template' => 'view_deals',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Client form
  $items['p1_client_form'] = array(
      'render element' => 'form',
      'template' => 'client',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Deal Documents
  $items['p1_deal_upload_form'] = array(
      'render element' => 'form',
      'template' => 'deal_upload',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Deal Documents
  $items['p1_access_form'] = array(
      'render element' => 'form',
      'template' => 'deal_access',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // User profile form
  $items['user_profile_form'] = array(
      'render element' => 'form',
      'template' => 'profile',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Deal status form
  $items['px_status_form'] = array(
      'render element' => 'form',
      'template' => 'deal_status',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Deal status filter form
  $items['p1_deal_status_filter_form'] = array(
      'render element' => 'form',
      'template' => 'deal_filter',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Deal status comment form
  $items['p1_deal_status_message_form'] = array(
      'render element' => 'form',
      'template' => 'deal_comment',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Deal term sheet form
  $items['p1_term_sheet_form'] = array(
      'render element' => 'form',
      'template' => 'deal_term_sheet',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Deal term sheet form
  $items['p1_deal_status_review_form'] = array(
      'render element' => 'form',
      'template' => 'deal_status_review',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  // Deal term sheet form
  $items['p1_deal_status_email_entity_form'] = array(
      'render element' => 'form',
      'template' => 'deal_status_email',
      'path' => drupal_get_path('theme', 'p1') . '/templates/forms',
  );

  return $items;
}
